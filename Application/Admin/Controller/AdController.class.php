<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class adController extends AdminController {

    public function _initialize(){
        parent::_initialize();
    }

    /*
     * 广告列表
     */
    public function index(){
        $Model = new \Home\Model\MemberModel();
        $type = array(1=>'链接',2=>'拨号',3=>'QQ');
        $map['a.status'] = 1;
        $map['a.source'] = 0;
        $map['a.image'] = array('not in',array('','/Public/images/mobile/ad_add.png'));
        $count = M("AdDetail")->join('as a left join yx_member as m on a.uid=m.uid')->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("AdDetail")->join('as a left join yx_member as m on a.uid=m.uid')
                ->field('a.*, m.name,m.parent_uid,m.phone')
                ->where($map)
                ->limit($p->firstRow.','.$p->listRows)
                ->order('a.id desc')
                ->select();
        foreach($list as $key=>$vo){
            $list[$key]['source'] = $Model->source($vo['parent_uid']);
            $list[$key]['type'] = $type[$vo['type']];

        }
        $page = $p->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }

    public function add(){
        $this->display('edit');
    }

    /*
     * 模板
     */
    public function muban(){
        $sort = M("AdTemplateType")->where( array('status'=>1) )->select();
        foreach($sort as $vo){
            $array[$vo['id']] = $vo['name'];
        }
        if($_GET['type']){
            $map['type'] = intval($_GET['type']);
        }
        $map['status'] = 1;
        $count = M("AdTemplate")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("AdTemplate")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        foreach($list as $key=>$vo){
            $list[$key]['type'] = $array[$vo['type']];
        }
        $page = $p->show();

        $sort = M("AdTemplateType")->where( array('status'=>1) )->select();
        $this->assign('sort',$sort);
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }


    /*
     * 添加模板
     */
    public function addtype(){
        $template = M("AdTemplate")->where( array('id'=>$_GET['id']) )->find();
        $sort = M("AdTemplateType")->where( array('status'=>1) )->select();
        $this->assign('template',$template);
        $this->assign('sort',$sort);
        $this->display();
    }

    /*
     * 模板分类
     */
    public function type(){
        $list = M("AdTemplateType")->where( array('status'=>1) )->select();
        $this->assign('list',$list);
        $this->display();
    }

    /*
     * 添加模板分类
     */
    public function addAdType(){
        $name = $_POST['name'];
        if( empty($name) ){
            $this->ajaxReturn( array('data'=>0,'info'=>'分类名称不能为空','status'=>0) );
        }else{
            $result = M("AdTemplateType")->add( array('name'=>$name,'status'=>1) );
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

    /*
     * 更新模板分类名称
     */
    public function updateAdType(){
        if( empty($_POST['id']) || empty($_POST['name']) ){
            dialog('参数错误');
        }else{
            $result = M("AdTemplateType")->where( array('id'=>$_POST['id']) )->save( array('name'=>$_POST['name']) );
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

    /*
    * 分类操作
    * id：广告分类ID
    */
    public function deleteAdType(){
        $id = intval($_POST['id']);
        if( empty($id) ){
            $this->ajaxReturn( array('data'=>0,'info'=>'缺少参数','status'=>0) );
        }
        $result = M("AdTemplateType")->where( array('id'=>$id) )->save( array('status'=>0) );
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'删除成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }


    /*
     * 添加/编辑模板图片
     */
    public function addTemplate(){
        $id = intval($_POST['id']);
        if($id){
            $result = M("AdTemplate")->where( array('id'=>$id) )->save(array(
                'type' => $_POST['type'],
                'image' => $_POST['image']
            ));
        }else{
            $result = M("AdTemplate")->add(array(
                'type' => $_POST['type'],
                'image' => $_POST['image'],
                'status' => 1
            ));
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'保存成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'保存失败','status'=>0) );
        }
    }

    /*
     * 上传图片插件
     * type：图片类型，1名片，2广告
     * base64_string：压缩图片
     */
    public function interface_upload(){
        $Image = new \Home\Model\ImageModel();
        $msg = $Image->upload(array(
            'type' => 3,
            'base64_string' => $_POST['base64_string'],
            'uid' => $this->uid
        ));
        $this->ajaxReturn($msg);
    }




    /*
     * 删除广告类别
     * ids：ID
     * type：1广告，2模板
     */
    public function operateAds(){
        $ids = explode(',',$_POST['ids']);
        $type = $_POST['type'];
        $operate = $_POST['operate'];
        if($type==1){
            $Model = M("AdDetail");
        }elseif($type==2){
            $Model = M("AdTemplate");
        }
        if( count($ids)==1 ){
            $result = $Model->where( array('id'=>$ids[0]) )->delete();
        }else{
            if($operate==3 && $type==1){
                foreach($ids as $vo){
                    $result = $Model->where( array('id'=>$vo) )->save( array('check'=>1) );
                }
            }elseif($operate==0){
                foreach($ids as $vo){
                    $result = $Model->where( array('id'=>$vo) )->delete();
                }
            }

        }
        if($result){
            $this->ajaxReturn( array('data'=>0,'info'=>'删除成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'删除失败','status'=>0) );
        }
    }


}