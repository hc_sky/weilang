<?php
namespace Admin\Controller;
use Think\BaseController;

class AdminController extends BaseController {


    public function _initialize(){
        parent::_initialize();
        $this->check_member();
    }

    public function check_member(){
        if( cookie("Admin") ){
            $this->uid = cookie("Admin")['id'];
            $this->role = cookie("Admin")['role'];
            $role = cookie("Admin")['role'];
            $this->assign("Admin", cookie("Admin"));
            //dump(cookie("Admin"));
            //echo MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;
            if( $role==4 ){
                if( !in_array(CONTROLLER_NAME, array('Index','Member','Sale','Statistics')) ){
                    $this->redirect("/admin/index");
                }
            }elseif( in_array($role,array(3,5)) ){
                if( !in_array(CONTROLLER_NAME, array('Index','Member','Sale','Statistics')) ){
                    $this->redirect("/admin/index");
                    //echo '不允许访问';
                }
            }
            self::checkLock($this->uid);
        }else{
            $this->redirect("Public/login");
        }
    }

    /*
     * 冻结
     */
    protected function checkLock($id){
        $status = M("Admin")->where( array('id'=>$id) )->getField('status');
        if($status==2){
            cookie("Admin",null);
            $this->redirect("Public/lock");
        }
    }

}