<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class CheckController extends AdminController {

    public function _initialize(){
        parent::_initialize();
    }

    /*
     * 广告列表
     */
    public function ad(){
        $Model = new \Home\Model\MemberModel();
        $type = array(1=>'链接',2=>'拨号',3=>'QQ');
        $map['a.status'] = 1;
        $map['a.source'] = 0;
        $map['a.image'] = array('not in',array('','/Public/images/mobile/ad_add.png'));
        $map['m.uid'] = array('gt',0);
        $count = M("AdDetail")->join('as a left join yx_member as m on a.uid=m.uid')->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("AdDetail")->join('as a left join yx_member as m on a.uid=m.uid')
                ->field('a.*, m.name,m.parent_uid,m.phone,m.relation_id')
                ->where($map)
                ->limit($p->firstRow.','.$p->listRows)
                ->order('a.id desc')
                ->select();
        foreach($list as $key=>$vo){
            $list[$key]['source'] = $Model->source($vo['parent_uid']);
            $list[$key]['type'] = $type[$vo['type']];

        }
        $page = $p->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }


    /*
     * 删除广告类别
     * ids：ID
     * type：1广告，2模板
     */
    public function operateAds(){
        $ids = explode(',',$_POST['ids']);
        $type = $_POST['type'];
        $operate = $_POST['operate'];
        if($type==1){
            $Model = M("AdDetail");
        }elseif($type==2){
            $Model = M("AdTemplate");
        }
        $Notics = new \Admin\Model\NoticsModel();
        if($operate==3 && $type==1){
            foreach($ids as $vo){
                $result = $Model->where( array('id'=>$vo) )->save( array('check'=>1) );
            }
        }elseif($operate==0){
            foreach($ids as $vo){
                $uid = $Model->where( array('id'=>$vo) )->getField('uid');
                $result = $Model->where( array('id'=>$vo) )->delete();
                if($type==1 && $result){
                    $Notics->create($vo,2,$uid);   //通知入库
                }
            }
        }
        if($result != false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }


    /*
     * 文章列表
     */
    public function article(){
        $Model = new \Home\Model\MemberModel();
        if( in_array($this->role,array(3,4,5)) ){
            $map['a.uid'] = array('in',$this->uids);
        }
        $map['a.status'] = 1;
        $map['m.uid'] = array('gt',0);
        $count = M("Article")->join('as a left join yx_member as m on a.uid=m.uid')->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("Article")->join('as a left join yx_member as m on a.uid=m.uid')
            ->field('a.*, m.parent_uid,m.phone,m.relation_id')
            ->where($map)
            ->limit($p->firstRow.','.$p->listRows)
            ->order('a.id desc')
            ->select();
        foreach($list as $key=>$vo){
            if( in_array($this->role,array(3,5)) ){
                $list[$key]['phone'] = substr($vo['phone'],0,3). "****" . substr($vo['phone'],7,11);
            }
//            $list[$key]['source'] = $Model->source($vo['parent_uid']);
            $list[$key]['source2'] = D("Admin")->source2($vo['parent_uid'],$this->role);
            $list[$key]['link'] = 'http://'.$_SERVER['SERVER_NAME'].'/Home/article/item?id='.$vo['uid'].'&aid='.$vo['id'];

        }
        $page = $p->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }

    /*
    * 删除文章
    * ids：ID
    * operate：0删除，3已阅
    */
    public function operateArtice(){
        $Notics = new \Admin\Model\NoticsModel();
        $ids = explode(',',$_POST['ids']);
        $operate = $_POST['operate'];
        if($operate==3){
            foreach($ids as $vo){
                $result = M("Article")->where( array('id'=>$vo) )->save( array('check'=>1) );
            }
        }elseif($operate==0){
            foreach($ids as $vo){
                $uid = M("Article")->where( array('id'=>$vo) )->getField('uid');
                $result = M("Article")->where( array('id'=>$vo) )->delete();
                $Notics->create($vo,1,$uid);   //通知入库
            }
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }


}