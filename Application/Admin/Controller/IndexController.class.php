<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;



class IndexController extends AdminController {

    public function _initialize(){
        parent::_initialize();
        $admin = M("Admin")->where( array('id'=>$this->uid) )->find();
        if($admin['role']==3){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $this->uids = M("Member")->where( array('parent_uid'=>array('in',$uids)) )->getField('uid',true);
        }elseif($admin['role']==5){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid,'role'=>3) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $uids = M("Admin")->where( array('parent_uid'=>array('in',$uids),'role'=>4) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $this->uids = M("Member")->where( array('parent_uid'=>array('in',$uids)) )->getField('uid',true);
        }elseif($admin['role']==4){
            $this->uids = M("Member")->where( array('parent_uid'=>$this->uid) )->getField('uid',true);
        }
        if( empty($this->uids) ){ $this->uids = array(-1); }

        $this->role = $admin['role'];
    }

    public function index(){


        timediff(1492185600);   //20170415
        timediff(1492444800);   //20170418
        timediff(1492617600);   //20170420

        $yesterday = date('Y-m-d',strtotime('-1 day'));
        $today = date('Y-m-d',time());
        $tomorrow = date('Y-m-d',strtotime('+1 day'));
        $yesterday = strtotime($yesterday);
        $today= strtotime($today);
        $tomorrow= strtotime($tomorrow);;
        $data1 = array($yesterday,$today);
        $data2 = array($today,$tomorrow);

        if( in_array($this->role,array(3,4,5)) ){
            $map_1['uid'] = $this->uid;
        }
        $map_1['status'] = 1;
        $map_1['ctime'] = array('between',$data1);
        $list['money']['yesterday'] = (float) M("MoneyLogs")->where($map_1)->sum('money');
        $map_1['ctime'] = array('between',$data2);
        $list['money']['today'] = (float) M("MoneyLogs")->where($map_1)->sum('money');
        unset($map_1['ctime']);
        $list['money']['all'] = (float) M("MoneyLogs")->where($map_1)->sum('money');
        //官网业绩
        unset($map_1);
        $map_1['sale_id'] = 0;
        $map_1['status'] = 1;
        $map_1['ctime'] = array('between',$data1);
        $list['deal_web']['yesterday'] = (int) M("PayLogs")->where($map_1)->sum('price');
        $map_1['ctime'] = array('between',$data2);
        $list['deal_web']['today'] = (int) M("PayLogs")->where($map_1)->sum('price');
        unset($map_1['ctime']);
        $list['deal_web']['all'] = (int) M("PayLogs")->where($map_1)->sum('price');
        //推荐码业绩
        unset($map_1);
        $map_1['sale_id'] = array('gt',0);
        $map_1['status'] = 1;
        $map_1['ctime'] = array('between',$data1);
        $list['deal_invite']['yesterday'] = (int) M("PayLogs")->where($map_1)->sum('price');
        $map_1['ctime'] = array('between',$data2);
        $list['deal_invite']['today'] = (int) M("PayLogs")->where($map_1)->sum('price');
        unset($map_1['ctime']);
        $list['deal_invite']['all'] = (int) M("PayLogs")->where($map_1)->sum('price');
        //成交数据
        if($this->role==3){
            $map_2['manage_id'] = $this->uid;
            $ratio = M("SystemConfig")->where( array('name'=>'cfg_ratio_manager') )->getField('value');
        }elseif($this->role==4){
            $map_2['sale_id'] = $this->uid;
            $ratio = M("SystemConfig")->where( array('name'=>'cfg_ratio_sale') )->getField('value');
        }elseif($this->role==5){
            $map_2['leader_id'] = $this->uid;
            $ratio = M("SystemConfig")->where( array('name'=>'cfg_ratio_director') )->getField('value');
        }
        $map_2['status'] = 1;
        $map_2['ctime'] = array('between',$data1);
        $list['deal']['yesterday'] = (int) M("PayLogs")->where($map_2)->sum('price');
        $map_2['ctime'] = array('between',$data2);
        $list['deal']['today'] = (int) M("PayLogs")->where($map_2)->sum('price');
        unset($map_2['ctime']);
        $list['deal']['all'] = (int) M("PayLogs")->where($map_2)->sum('price');
        //毛利
        $list['profit']['yesterday'] = $list['deal']['yesterday'] - $list['money']['yesterday'];
        $list['profit']['today'] = $list['deal']['today'] - $list['money']['today'];
        $list['profit']['all'] = $list['deal']['all'] - $list['money']['all'];
        //非邀请注册
        $map_3['parent_uid'] = 0;
        $map_3['pay'] = array('egt',0);
        $map_3['regtime'] = array('between',$data1);
        $map_3['isaudit'] = 1;
        $list['offweb']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['offweb']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['offweb']['all'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['pay']);
        //邀请注册
        $map_3['parent_uid'] = array('gt',0);
        $map_3['regtime'] = array('between',$data1);
        $list['invite']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['invite']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['invite']['all'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['parent_uid']);
        unset($map_3['isaudit']);
        //体验用户
        if( in_array($this->role,array(3,4,5)) ){
            $map_3['uid'] = array('in',$this->uids);
        }
        $map_3['pay'] = 0;
        $map_3['regtime'] = array('between',$data1);
        $map_3['isaudit'] = 0;
        $list['try']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['try']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['try']['all'] = (int) M("Member")->where($map_3)->count();
        //正式用户
        $map_3['pay'] = array('gt',0);
        $map_3['regtime'] = array('between',$data1);
        $map_3['isaudit'] = 1;
        $list['pay']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['pay']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['pay']['all'] = (int) M("Member")->where($map_3)->count();
        //天用户
        $map_3['pay'] = 5;
        $map_3['regtime'] = array('between',$data1);
        $list['role5']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['role5']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['role5']['all'] = (int) M("Member")->where($map_3)->count();
        //月用户
        $map_3['pay'] = 4;
        $map_3['regtime'] = array('between',$data1);
        $list['role4']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['role4']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['role4']['all'] = (int) M("Member")->where($map_3)->count();
        //季度用户
        $map_3['pay'] = 1;
        $map_3['regtime'] = array('between',$data1);
        $list['role1']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['role1']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['role1']['all'] = (int) M("Member")->where($map_3)->count();
        //半年用户
        $map_3['pay'] = 2;
        $map_3['regtime'] = array('between',$data1);
        $list['role2']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['role2']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['role2']['all'] = (int) M("Member")->where($map_3)->count();
        //一年用户
        $map_3['pay'] = 3;
        $map_3['regtime'] = array('between',$data1);
        $list['role3']['yesterday'] = (int) M("Member")->where($map_3)->count();
        $map_3['regtime'] = array('between',$data2);
        $list['role3']['today'] = (int) M("Member")->where($map_3)->count();
        unset($map_3['regtime']);
        $list['role3']['all'] = (int) M("Member")->where($map_3)->count();
        //网站流量统计
        $list['web']['yesterday'] = (int) M("WebClick")->where(array(
                                        'page' => 1,
                                        'date' => date('Y-m-d',strtotime('-1 day'))
                                    ))->getField('ip');
        $list['web']['today'] = (int) M("WebClick")->where(array(
                                    'page' => 1,
                                    'date' => date('Y-m-d',time())
                                ))->getField('ip');
        $list['web']['all'] = (int) M("WebClick")->where(array(
                                    'page' => 1,
                                    'date' => array('like', date('Y-m',time()).'%')
                                ))->sum('ip');
        $list['pv']['yesterday'] = (int) M("WebClick")->where(array(
                                    'page' => 1,
                                    'date' => date('Y-m-d',strtotime('-1 day'))
                                ))->getField('pv');
        $list['pv']['today'] = (int) M("WebClick")->where(array(
                                    'page' => 1,
                                    'date' => date('Y-m-d',time())
                                ))->getField('pv');
        $list['pv']['all'] = (int) M("WebClick")->where(array(
                                    'page' => 1,
                                    'date' => array('like', date('Y-m',time()).'%')
                                ))->sum('pv');
        //登录页流量统计
        $list['login']['yesterday'] = (int) M("WebClick")->where(array(
                                        'page' => 2,
                                        'date' => date('Y-m-d',strtotime('-1 day'))
                                    ))->getField('ip');
        $list['login']['today'] = (int) M("WebClick")->where(array(
                                    'page' => 2,
                                    'date' => date('Y-m-d',time())
                                ))->getField('ip');
        $list['login']['all'] = (int) M("WebClick")->where(array(
                                    'page' => 2,
                                    'date' => array('like', date('Y-m',time()).'%')
                                ))->sum('ip');
        $list['login_pv']['yesterday'] = (int) M("WebClick")->where(array(
                                            'page' => 2,
                                            'date' => date('Y-m-d',strtotime('-1 day'))
                                        ))->getField('pv');
        $list['login_pv']['today'] = (int) M("WebClick")->where(array(
                                        'page' => 2,
                                        'date' => date('Y-m-d',time())
                                    ))->getField('pv');
        $list['login_pv']['all'] = (int) M("WebClick")->where(array(
                                        'page' => 2,
                                        'date' => array('like', date('Y-m',time()).'%')
                                    ))->sum('pv');
        //业务员统计
        if( in_array($this->role,array(3)) ){
            $map_4['parent_uid'] = $this->uid;
        }
        $map_4['role'] = 4;
        $map_4['status'] = 1;
        $map_4['ctime'] = array('between',$data1);
        $list['sale']['yesterday'] = (int) M("Admin")->where($map_4)->count();
        $map_4['ctime'] = array('between',$data2);
        $list['sale']['today'] = (int) M("Admin")->where($map_4)->count();
        unset($map_4['ctime']);
        $list['sale']['all'] = (int) M("Admin")->where($map_4)->count();
        //经理统计
        if( in_array($this->role,array(5)) ){
            $map_4['parent_uid'] = $this->uid;
        }
        $map_4['role'] = 3;
        $map_4['ctime'] = array('between',$data1);
        $list['manage']['yesterday'] = (int) M("Admin")->where($map_4)->count();
        $map_4['ctime'] = array('between',$data2);
        $list['manage']['today'] = (int) M("Admin")->where($map_4)->count();
        unset($map_4['ctime']);
        $list['manage']['all'] = (int) M("Admin")->where($map_4)->count();

        if($this->role==4){
            $code = M("AdminDetail")->where( array('uid'=>$this->uid) )->field('code_invite as invite,code_product as product')->find();
            if( empty($code['invite']) || empty($code['product']) ){
                $QRcode = new \Home\Model\QRcodeModel();
                $code['invite'] = $QRcode->createQRCode($this->uid);
                $code['product'] = $QRcode->createQRCode($this->uid,2);
            }
        }
        switch($this->role){
            case 3: $role = '经理';   break;
            case 4: $role = '业务员';   break;
            case 5: $role = '总监';   break;
        }
        if( in_array($this->role,array(3,4)) ){
            $admin = M("Admin")->where( array('id'=>$this->uid) )->find();
            $phone = M("AdminDetail")->where( array('uid'=>$this->uid) )->getField('phone');
            $parent = M("AdminDetail")->where( array('uid'=>$admin['parent_uid']) )->field('realname,phone,weixin,qq')->find();
        }
        $free = M("SystemConfig")->where( array('name'=>'cfg_free') )->getField('value');


        $this->assign('list',$list);
        $this->assign('code',$code);
        $this->assign('ratio',$ratio);
        $this->assign('role',$role);
        $this->assign('parent',$parent);
        $this->assign('phone',$phone);
        $this->assign('free',$free);
        $this->display();
    }


    /*
     * 修改密码
     */
    public function changepass(){
        if(IS_POST){
            $old_pass = md5($_POST['old_pass']);
            $new_pass = md5($_POST['new_pass']);
            $user = M("Admin")->where(array('id'=>$this->uid))->find();
            if($user['pass'] != $old_pass){
                $this->ajaxReturn( array('data'=>0,'info'=>'旧密码不正确','status'=>0) );
            }else{
                $result = M("Admin")->where(array('id'=>$this->uid))->save( array('pass'=>$new_pass) );
                M("AdminDetail")->where(array('uid'=>$this->uid))->save( array('pass_code'=>$_POST['new_pass']) );
            }
            if($result !== false){
                cookie("Admin",null);
                $this->ajaxReturn( array('url'=>'/','data'=>0,'info'=>'修改成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'修改失败','status'=>0) );
            }
        }else{
            $this->assign('username',cookie("Admin")['username']);
        }
        //$this->assign();
        $this->display();
    }

    /*
     * 重置业务员推荐码
     */
    public function updateCode(){
        $Admin = new \Admin\Model\AdminModel();
        $sale = M("Admin")->where( array('role'=>4) )->getField('id',true);
        foreach($sale as $key=>$vo){
            $code = $Admin->createCode();
            M("AdminDetail")->where(array('uid'=>$vo) )->save( array('code'=>$code) );
        }
    }



}