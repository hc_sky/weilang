<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class MemberController extends AdminController {

    public function _initialize(){
        parent::_initialize();
        $this->model = new \Home\Model\MemberModel();
        $admin = M("Admin")->where( array('id'=>$this->uid) )->find();
        if($admin['role']==3){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $this->uids = M("Member")->where( array('parent_uid'=>array('in',$uids)) )->getField('uid',true);
        }elseif($admin['role']==5){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid,'role'=>3) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $uids = M("Admin")->where( array('parent_uid'=>array('in',$uids),'role'=>4) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $this->uids = M("Member")->where( array('parent_uid'=>array('in',$uids)) )->getField('uid',true);
        }elseif($admin['role']==4){
            $this->uids = M("Member")->where( array('parent_uid'=>$this->uid) )->getField('uid',true);
        }
        if( empty($this->uids) ){ $this->uids = array(-1); }
        $this->role = $admin['role'];
        //是否显示体验用户
        $free = M("SystemConfig")->where( array('name'=>'cfg_free') )->getField('value');
        $this->assign('free',$free);
    }

    /*
     * 用户列表
     */
    public function index(){
        //到期时间排序
        if($_GET['date']==1){
            $order = 'outtime desc';
        }elseif($_GET['date']==2){
            $order = 'outtime asc';
        }else{
            $order = 'regtime desc';
        }
        if($_POST['keyword']){
            $map['phone'] = array('like','%'.$_POST['keyword'].'%');
        }
        if($_GET['type']==1){
            $map['pay'] = array('gt',0);
        }elseif($_GET['type']==2){
            $map['isaudit'] = 5;
            $order = 'uid desc';
        }else{
            $map['pay'] = 0;
            $map['isaudit'] = 0;
        }
        //购买套餐排序
        if($_GET['price']>0){
            $map['pay'] = intval($_GET['price']);
        }
        if( in_array($this->role,array(3,4,5)) ){
            $map['uid'] = array('in',$this->uids);
        }
        if( $_GET['star'] && $_GET['end'] ){
            $star = strtotime($_GET['star']);
            $end = strtotime($_GET['end']) + 3600 * 24;
            $map['regtime'] = array('between',array($star,$end) );
        }
        $map['status'] = array('in',array(1,2));
        $count = $this->model->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = $this->model->where($map)->limit($p->firstRow.','.$p->listRows)->order($order)->select();
        foreach($list as $key=>$vo){
            if( !in_array($this->role,array(0,1,4)) ){
                $list[$key]['phone'] = substr($vo['phone'],0,3). "****" . substr($vo['phone'],7,11);
            }
            $list[$key]['article_num'] = M("Article")->where( array('uid'=>$vo['uid']) )->count();
            $list[$key]['article_delete_num'] = M("Article")->where( array('uid'=>$vo['uid'],'status'=>0) )->count();
            $list[$key]['article_click'] = M("Article")->where( array('uid'=>$vo['uid']) )->sum('click');
            $list[$key]['ad_click'] = M("AdDetail")->where( array('uid'=>$vo['uid']) )->sum('click');
            $list[$key]['out_day'] = timediff($vo['outtime']);
            $list[$key]['source'] = $this->model->source($vo['parent_uid']);
            $list[$key]['source2'] = D("Admin")->source2($vo['parent_uid'],$this->role);
            $list[$key]['try'] = M("PayLogs")->where( array('uid'=>$vo['uid'],'choose'=>0,'status'=>1) )->getField('id');
            $list[$key]['buy_num'] = M("PayLogs")->where( array('uid'=>$vo['uid'],'choose'=>array('gt',0),'status'=>1) )->count();
            $list[$key]['pay_all_money'] = M("PayLogs")->where( array('uid'=>$vo['uid'],'choose'=>array('gt',0),'status'=>1) )->sum('price');
            $log = M("PayLogs")->where( array('uid'=>$vo['uid'],'price'=>array('gt',0),'status'=>1) )
                    ->order('id desc')->field('choose,pay_way,price')->find();
            if( empty($log['choose']) ){
                $list[$key]['choose'] = '无记录';
            }else{
                $list[$key]['choose_price'] = $log['price'];
                $list[$key]['choose'] = chooseWay( intval($log['choose']) );
            }
            $list[$key]['pay_way'] = payWay( intval($log['pay_way']) );
            if($_GET['type'] == 2){
                $tosale = M("Admin")->where( array('username'=>$vo['phone']) )->field('realname,role,ctime')->find();
                $list[$key]['realname'] = $tosale['realname'];
                $list[$key]['role'] = $tosale['role'];
                $list[$key]['regtime'] = $tosale['ctime'];
            }
        }
        $page = $p->show();

        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->assign("count",$count);
        if($_GET['type'] == 0){
           $tpl = 'try';
        }elseif($_GET['type'] == 2){
            $tpl = 'team';
        }else{
            $tpl = 'index';
        }
        $this->display($tpl);
    }

    /*
     * 潜在列表
     */
    public function outtime(){
        if($_POST['keyword']){
            $map['phone'] = array('like','%'.$_POST['keyword'].'%');
        }
        if($this->role==3){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $map['parent_uid'] = array('in',$uids);
        }elseif($this->role==4){
            $map['parent_uid'] = $this->uid;
        }elseif($this->role==5){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $uids = M("Admin")->where( array('parent_uid'=>array('in',$uids),'role'=>4) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $map['parent_uid'] = array('in',$uids);
        }

        $map['status'] = 0;
        $count = M("RegisterLogs")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("RegisterLogs")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        foreach($list as $key=>$vo){
            if( !in_array($this->role,array(0,1,4)) ){
                $list[$key]['phone'] = substr($vo['phone'],0,3). "****" . substr($vo['phone'],7,11);
            }
            $list[$key]['article_num'] = M("Article")->where( array('uid'=>$vo['uid']) )->count();
            $list[$key]['article_click'] = M("Article")->where( array('uid'=>$vo['uid']) )->sum('click');
            $list[$key]['ad_click'] = M("AdDetail")->where( array('uid'=>$vo['uid']) )->sum('click');
            $list[$key]['out_day'] = ceil( ($vo['outtime'] - $vo['startime']) / 86400 );
            $list[$key]['try'] = M("PayLogs")->where( array('uid'=>$vo['uid'],'choose'=>0,'status'=>1) )->getField('id');
            $list[$key]['buy_num'] = M("PayLogs")->where( array('uid'=>$vo['uid'],'price'=>array('gt',0),'status'=>1) )->count();
            $log = M("PayLogs")->where( array('uid'=>$vo['uid'],'price'=>array('gt',0),'status'=>1) )
                    ->order('id desc')->field('choose,pay_way')->find();
            $list[$key]['choose'] = chooseWay( intval($log['choose']) );
            $list[$key]['pay_way'] = payWay( intval($log['pay_way']) );
            $list[$key]['source'] = D("Admin")->source2($vo['parent_uid'],$this->role);
        }
        $page = $p->show();

        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->assign("count",$count);
        $this->display();
    }

    /*
     * 删除用户
     * ids：用户ID
     */
    public function deleteUser($ids){
        if( empty($ids) ){
            $ids = explode(',',$_POST['ids']);
        }
        if( count($ids)==1 ){
            $result = $this->model->where( array('uid'=>$ids[0]) )->delete();
        }else{
            foreach($ids as $vo){
                $result = $this->model->where( array('uid'=>$vo) )->delete();
            }
        }
        //echo M()->getLastSql();
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

    /*
     * 操作用户
     * ids：用户ID
     * stauts：操作，0逻辑删除，2冻结
     */
    public function operateUser(){
        $ids = explode(',',$_POST['ids']);
        $operate = intval($_POST['operate']);
        if($operate==0){
            self::deleteUser($ids);
        }
        if( count($ids)==1 ){
            $result = M("Member")->where( array('uid'=>$ids[0]) )->save( array('status'=>$operate) );
        }else{
            foreach($ids as $vo){
                $result = M("Member")->where( array('uid'=>$vo) )->save( array('status'=>$operate) );
            }
        }
        //echo M()->getLastSql();
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

    /*
    * 删除潜在用户
    * ids：用户ID
    */
    public function deleteOutUser(){
        $ids = explode(',',$_POST['ids']);
        if( count($ids)==1 ){
            $result = M("RegisterLogs")->where( array('id'=>$ids[0]) )->delete();
        }else{
            foreach($ids as $vo){
                $result = M("RegisterLogs")->where( array('id'=>$vo) )->delete();
            }
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }



}