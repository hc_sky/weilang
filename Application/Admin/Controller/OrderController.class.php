<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class OrderController extends AdminController {

    public function _initialize(){
        parent::_initialize();
        $this->model = new \Admin\Model\AdminModel();
    }

    public function index(){
        $map['a.status'] = array('neq',0);
        $map['a.role'] = intval($_GET['role']);
        $count = $this->model->where($map)->join('as a left join yx_admin_detail as d on a.id = d.uid')->count();
        $p = new \Think\Page($count,20);
        $page = $p->show();
        $list = $this->model->where($map)
            ->join('as a left join yx_admin_detail as d on a.id = d.uid')
            ->join('left join yx_admin_detail as d2 on a.parent_uid = d2.uid')
            ->join('LEFT JOIN ( select uid,SUM(money) as money FROM yx_money_logs GROUP BY uid) as l on a.id = l.uid')
            ->join('LEFT JOIN ( select uid,SUM(money) as yes FROM yx_case_logs GROUP BY uid) as l2 on a.id = l2.uid')
            ->field('a.id as userid,a.username,a.role,a.realname, d2.realname as manage,d2.phone as phone_manage, l.money, l2.yes, d.*')
            ->limit($p->firstRow.','.$p->listRows)
            ->select();
        foreach($list as $key=>$vo){
            if($vo['role']==4){
                $list[$key]['worktype'] = (int) M("PayLogs")->where( array('sale_id'=>$vo['userid'],'status'=>1) )->sum('price');
            }elseif($vo['role']==3){
                $list[$key]['worktype'] = (int) M("PayLogs")->where( array('manage_id'=>$vo['userid'],'status'=>1) )->sum('price');
            }elseif($vo['role']==5){
                $list[$key]['worktype'] = (int) M("PayLogs")->where( array('leader_id'=>$vo['userid'],'status'=>1) )->sum('price');
            }
        }

        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->assign("count",$count);
        $this->display();
    }

    /*
     * 结算
     * uid：业务员、主管ID
     * pay_way：转账账户类型，2微信，3支付宝
     * money：结算金额
     */
    public function tocase(){
        $uid = intval($_POST['uid']);
        $money = M("MoneyLogs")->where( array('uid'=>$uid) )->sum('money');
        $yes = M("CaseLogs")->where( array('uid'=>$uid) )->sum('money');

        $no = $money - $yes;
        if($_POST['money'] > $no){
            $this->ajaxReturn( array('data'=>0,'info'=>'结算金额超过未结算金额','status'=>0) );
        }else{
            $detail = M("AdminDetail")->where( array('uid'=>$uid) )->field('alipay,wechat')->find();
            if($_POST['pay_way']==2){
                $pay_account = $detail['wechat'];
            }elseif($_POST['pay_way']==3){
                $pay_account = $detail['alipay'];
            }
            $result = M("CaseLogs")->add(array(
                'uid' => $uid,
                'role' => M("Admin")->where( array('id'=>$uid) )->getField('role'),
                'money' => $_POST['money'],
                'pay_way' => $_POST['pay_way'],
                'pay_account' => $pay_account,
                'ctime' => time(),
                'status' => 1,
                'operate_id' => $this->uid
            ));
        }
        if($result !== false){
            self::uploadLicense($result);  //上传凭证
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

    /*
     * 修改凭证
     * pid：结账ID
     */
    public function license(){
        $pid = intval($_GET['pid']);
        $license = M("CaseLogsLicense")->where( array('pid'=>$pid,'status'=>1) )->order('id desc')->getField('license');
        $this->assign("license",$license);
        $this->display();
    }

    /*
     * 结算记录
     * uid：对象ID
     */
    public function logs(){
        $map['uid'] = intval($_GET['uid']);
        $count = M("CaseLogs")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("CaseLogs")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        foreach($list as $key=>$vo){
            $list[$key]['operate'] = M("Admin")->where( array('id'=>$vo['operate_id']) )->getField('username');
            $images = M("CaseLogsLicense")->where( array('pid'=>$vo['id'],'status'=>1) )
                        ->field('id,license')
                        ->order('id desc')
                        ->find();
            $list[$key]['lid']  = $images['id'];
            $list[$key]['license']  = $images['license'];
        }
        $page = $p->show();

        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->display();
    }


    /*
     * 结算凭证上传
     * pid：结算单ID
     * license_id：凭证ID，更新凭证时使用
     */
    public function uploadLicense($pid){
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     3145728 ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =     './Upload/'; // 设置附件上传根目录
        $upload->savePath  =     'license/'; // 设置附件上传（子）目录
        $upload->autoSub  = true;
        // 上传文件
        $info   =   $upload->upload();
        if(!$info) {
            $this->error($upload->getError());
        }else{
            $file = '/Upload/'.$info['file']['savepath'].$info['file']['savename'];
            if($file){
                if($pid){
                    $result = M("CaseLogsLicense")->add(array(
                        'pid' => $pid,
                        'license' => $file,
                        'status' => 1
                    ));
                }
            }
        }
        if($result){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

}