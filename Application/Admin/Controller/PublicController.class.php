<?php
namespace Admin\Controller;
use Think\BaseController;



class PublicController extends BaseController {

    public function login(){
        if(IS_POST){
            $username = $_POST['username'];
            $pass = md5($_POST['pass']);
            $user = M("Admin")->where( array('username'=>$username) )
                    ->field('id,username,realname,pass,role,status')
                    ->find();
            if( empty($user) ){
                $this->ajaxReturn( array('data'=>0,'info'=>'账户不存在','status'=>0) );
            }elseif($user['status']==2){
                $this->ajaxReturn( array('data'=>0,'info'=>'账户被禁用','status'=>2) );
            }elseif($pass != $user['pass']){
                $this->ajaxReturn( array('data'=>0,'info'=>'密码不正确','status'=>0) );
            }elseif($user['status']==1){
                unset($user['pass']);
                cookie("Admin", $user);
                self::loginLogs($user['id']);
                $this->ajaxReturn( array('data'=>0,'info'=>'登录成功','status'=>1) );
            }
        }else{
            $user = cookie("Admin");
            $role = M("Admin")->where( array('id'=>$user['id'],'status'=>1) )->find();
            if($role){
                $this->redirect("/Admin/index");
            }
            $this->display();
        }
    }

    public function verify(){
        $Verify = new \Think\Verify();
        $Verify->entry(1);
    }

    /*
    * 退出登录
    */
    public function layout(){
        cookie("Admin",null);
        $this->ajaxReturn( array('data'=>0,'info'=>'退出登录','status'=>1,'url'=>U('/web') ) );
    }


    /*
     * 省份、城市、区域选择
     * id：省份、城市ID
     * level：等级，1：省份，2：城市，3：区域
     */
    public function choose_city(){
        $id = intval($_POST['id']);
        $level = intval($_POST['level']);
        if($level==2){
            $data = M("City")->where( array('province_id'=>$id) )
                    ->field("city_id as id,city_name as name")->select();
        }elseif($level==3){
            $data = M("District")->where( array('city_id'=>$id) )
                    ->field("district_id as id,district_name as name")->select();
        }else{
            $data = M("Province")->field("province_id as id,province_name as name")->select();
        }
        $this->ajaxReturn( array('data'=>$data,'info'=>'返回信息','status'=>1) );
    }

    /*
     * 登录日记
     */
    public function loginLogs($uid){
        M("AdminLoginLogs")->add(array(
            'uid' => $uid,
            'ctime' => time(),
            'ip' => $this->getRealIP()
        ));
    }

    /*
     * 忘记密码
     * phone：手机号码
     * pass：新密码
     * code：短信验证码
     */
    public function forget(){
        if(IS_POST){
            $SMS = new \Home\Model\SmsModel();
            $msg = $SMS->checkPhoneCode($_POST['phone'],$_POST['code']);
            if($msg['status']==1){
                $uid = M("AdminDetail")->where( array('phone'=>$_POST['phone']) )->getField('uid');
                if($uid){
                    $result = M("Admin")->where( array('id'=>$uid) )->save( array('pass'=>md5($_POST['pass'])) );
                }
            }
            if($result !== false){
                $this->ajaxReturn( array('data'=>0,'info'=>'修改成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'修改失败','status'=>0) );
            }
        }else{
            $this->display();
        }
    }
}