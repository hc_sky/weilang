<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class SaleController extends AdminController {

    public function _initialize(){
        parent::_initialize();
        $this->model = new \Admin\Model\AdminModel();
        $admin = M("Admin")->where( array('id'=>$this->uid) )->find();
        if($admin['role']==3){
            $this->uids = M("Admin")->where( array('parent_uid'=>$this->uid) )->getField('id',true);
            if( empty($this->uids) ){
                $this->uids = array(-1);
            }
        }
        $this->role = $admin['role'];
        $this->assign('thisrole',$this->role);
    }

    public function index(){
        if($_GET['money']==1){
            $order = 'l.money_all desc';
        }elseif($_GET['money']==2){
            $order = 'l.money_all asc';
        }else{
            $order = 'id desc';
        }
        if( in_array($this->role,array(3,4,5)) ){
            $map['a.parent_uid'] = $this->uid;
        }
        if( $_POST['keyword'] ){
            $map['d.phone'] = $_POST['keyword'];
        }
        $map['a.status'] = array('neq',0);
        $map['a.role'] = intval($_GET['role']);
        $count = $this->model->where($map)->join('as a left join yx_admin_detail as d on a.id = d.uid')->count();
        $p = new \Think\Page($count,20);
        $list = $this->model->where($map)
                ->join('as a left join yx_admin_detail as d on a.id = d.uid')
                ->join('LEFT JOIN ( select uid,SUM(money) as money_all from yx_money_logs GROUP BY uid ) as l on a.id = l.uid')
                ->field('a.id as userid,a.username,a.role,a.realname,a.parent_uid,a.status, d.*, l.money_all')
                ->limit($p->firstRow.','.$p->listRows)
                ->order($order)
                ->select();
        $page = $p->show();
        //echo M()->getLastSql();
        $Member = new \Home\Model\MemberModel();
        foreach($list as $key=>$vo){
            if($vo['role']==5){
                $list[$key]['num_manage'] = (int) M("Admin")->where( array('parent_uid'=>$vo['userid'],'role'=>3,'status'=>1) )->count();
                $uids = M("Admin")->where( array('parent_uid'=>$vo['userid'],'role'=>3,'status'=>1) )->getField('id',true);
                if( empty($uids) ){ $uids[0] = -1; }
                $list[$key]['num_sale'] = (int) M("Admin")->where( array('parent_uid'=>array('in',$uids),'role'=>4,'status'=>1) )->count();
            }elseif($vo['role']==3){
                $list[$key]['num_sale'] = (int) M("Admin")->where( array('parent_uid'=>$vo['userid'],'role'=>4,'status'=>1) )->count();
            }

            $list[$key]['source'] = $this->model->source($vo['userid'],$_GET['role']);
            $parent = M("Admin")->where( array('id'=>$vo['parent_uid']) )->field('realname,parent_uid')->find();
            $list[$key]['parent_name'] = $parent['realname'];
            $parent = M("Admin")->where( array('id'=>$parent['parent_uid']) )->field('realname,parent_uid')->find();
            $list[$key]['leader_name'] = $parent['realname'];
            $list[$key]['all_yesterday'] = $Member->compute($vo['userid'],10,1);
            $list[$key]['try_yesterday'] = $Member->compute($vo['userid'],0,1);
            $list[$key]['pay1_yesterday'] = $Member->compute($vo['userid'],1,1);
            $list[$key]['pay2_yesterday'] = $Member->compute($vo['userid'],2,1);
            $list[$key]['pay3_yesterday'] = $Member->compute($vo['userid'],3,1);
            $list[$key]['pay4_yesterday'] = $Member->compute($vo['userid'],4,1);
            //$list[$key]['pay5_yesterday'] = $Member->compute($vo['userid'],5,1);
            $list[$key]['all'] = $Member->compute($vo['userid'],10);
            $list[$key]['try'] = $Member->compute($vo['userid'],0);
            $list[$key]['pay1'] = $Member->compute($vo['userid'],1);
            $list[$key]['pay2'] = $Member->compute($vo['userid'],2);
            $list[$key]['pay3'] = $Member->compute($vo['userid'],3);
            $list[$key]['pay4'] = $Member->compute($vo['userid'],4);
            //$list[$key]['pay5'] = $Member->compute($vo['userid'],5);
            $list[$key]['money_yesterday'] =  $Member->computeMoney($vo['userid'],1);
            $list[$key]['money_today'] =  $Member->computeMoney($vo['userid'],2);
            $list[$key]['money_all'] =  $Member->computeMoney($vo['userid']);
            $list[$key]['work_yesterday'] =  $Member->workMoney($vo['userid'],1);
            $list[$key]['work_today'] =  $Member->workMoney($vo['userid'],2);
            $list[$key]['work_all'] =  $Member->workMoney($vo['userid']);
            $list[$key]['last_login'] = M("AdminLoginLogs")->where( array('uid'=>$vo['userid']) )->order('id desc')->getField('ctime');
            $list[$key]['login_num'] = M("AdminLoginLogs")->where( array('uid'=>$vo['userid']) )->count();
        }


        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->assign("count",$count);
        $this->display();
    }


    /*
     * 添加业务员/主管
     * type：结佣方式，2主管，3业务员
     */
    public function add(){
        $province = M("Province")->field('province_id as id, province_name as name')->select();
        $this->assign('province',$province);
        $this->display('edit');
    }

    /*
     * 编辑添加业务员，主管
     */
    public function edit(){
        if(IS_POST){
            $Admin = new \Admin\Model\AdminModel();
            $msg = $Admin->baseAccount($_POST);
            if($msg['status']==1){
                $msg['url'] = U('index',array('role'=>$_POST['role']));
            }
            $this->ajaxReturn($msg);
        }else{
            $map['id'] = intval($_GET['id']);
            $map['role'] = intval($_GET['role']);
            $admin = M("Admin")->where($map)->field('id,username,parent_uid')->find();
            $detail = M("AdminDetail")->where(array('uid'=>$map['id']))->find();
            $admin['parent_name'] = M("AdminDetail")->where(array('uid'=>$admin['parent_uid']))->getField('realname');
            unset($detail['id']);
            $user = array_merge( (array) $admin , (array) $detail);
            $province = M("Province")->field('province_id as id, province_name as name')->select();
            $this->assign('user',$user);
            $this->assign('province',$province);
            $this->display();
        }
    }

    /*
     * 操作业务员，主管
     * ids：用户ID
     * stauts：操作，0删除，2冻结
     */
    public function operateUser(){
        $ids = explode(',',$_POST['ids']);
        $operate = intval($_POST['operate']);
        if( count($ids)==1 ){
            $result = $this->model->where( array('id'=>$ids[0]) )->save( array('status'=>$operate) );
            if($ids[0]){
                M("Member")->where( array('relation_id'=>$ids[0]) )->save( array('pay'=>-1) );
            }
        }else{
            foreach($ids as $vo){
                $result = $this->model->where( array('id'=>$vo) )->save( array('status'=>$operate) );
                if($vo){
                    M("Member")->where( array('relation_id'=>$vo) )->save( array('pay'=>-1) );
                }
            }
        }
        //echo M()->getLastSql();
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }


    /*
     * 主管搜索
     */
    public function search(){
        if($_POST['keyword']){
            if( is_numeric($_POST['keyword']) ){
                $map['d.phone'] = array('like','%'.$_POST['keyword'].'%');
            }else{
                $map['d.realname'] = array('like','%'.$_POST['keyword'].'%');
            }
        }
        if($_POST['province']){
            $map['d.province'] = $_POST['province'];
        }
        if($_GET['role']==4){
            $map['m.role'] = 3;
        }elseif($_GET['role']==3){
            $map['m.role'] = 5;
        }
        $map['m.status'] = 1;
        $list = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where($map)
                ->field('d.uid, d.realname, d.phone')
                ->select();

        $province = M("Province")->field('province_id as id, province_name as name')->select();
        $this->assign('province',$province);
        $this->assign('list',$list);
        $this->display();
    }

    /*
     * ajax请求主管
     */
    public function ajaxSearch(){
        if($_POST['province']){
            $map['d.province'] = $_POST['province'];
        }
        if($_POST['city']){
            $map['d.city'] = $_POST['city'];
        }
        if($_POST['keyword']){
            if( is_numeric($_POST['keyword']) ){
                $map['d.phone'] = array('like','%'.$_POST['keyword'].'%');
            }else{
                $map['d.realname'] = array('like','%'.$_POST['keyword'].'%');
            }
            unset( $map['d.province'] );
        }
        if($_GET['role']==4){
            $map['m.role'] = 3;
        }elseif($_GET['role']==3){
            $map['m.role'] = 5;
        }
        $map['m.status'] = 1;
        $list = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where($map)
                ->field('d.uid, d.realname, d.phone')
                ->select();

        if($list){
            $this->ajaxReturn( array('data'=>$list,'info'=>'返回列表','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'暂无数据','status'=>0) );
        }
    }


    /*
     * 查看业务员、主管信息
     */
    public function viewSale(){
        $map['m.id'] = intval($_GET['id']);
        $sale = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where($map)
                ->field('m.id, m.username, m.pass, m.role, d.realname, d.code, d.pass_code')->find();
        if( in_array($this->role,array(3,5)) ){
            $sale['pass_code'] = M("AdminPass")->where( array('uid'=>$sale['id']) )->getField('pass');
        }
        $this->assign("sale",$sale);
        $this->display();
    }


    /*
     * 提交记录
     */
    public function money(){
        $map['l.uid'] = $this->uid;
        if( $_GET['star'] && $_GET['end'] ){
            $star = strtotime($_GET['star']);
            $end = strtotime($_GET['end']) + 3600 * 24;
            $map['l.ctime'] = array('between',array($star,$end) );
        }
        $count = M("MoneyLogs")->where($map)->join("as l left join yx_pay_logs as p on l.pid=p.id")
                ->join("left join yx_member as m on p.uid=m.uid")->count();
        $p = new \Think\Page($count,20);
        $list = M("MoneyLogs")->where($map)->join("as l left join yx_pay_logs as p on l.pid=p.id")
                ->join("left join yx_member as m on p.uid=m.uid")
                ->field('l.*, p.uid as pay_uid,p.phone,p.choose,p.price,p.pay_way, m.parent_uid')
                ->limit($p->firstRow.','.$p->listRows)
                ->order('l.id desc')
                ->select();
        //dump($list);
        //echo M()->getLastSql();
        foreach($list as $key =>$vo){
            if( !in_array($this->role,array(0,4)) ){
                $list[$key]['phone'] = substr($vo['phone'],0,3).'****'.substr($vo['phone'],7,11);
            }
            $list[$key]['buy_num'] = M("PayLogs")->where( array('uid'=>$vo['pay_uid'],'price'=>array('gt',0),'status'=>1) )->count();

            $sale = M("Admin")->where( array('id'=>$vo['parent_uid']) )->field('id,role')->find();
            $list[$key]['source'] = $this->model->source2($sale['id'],$this->role);
        }
        $page = $p->show();

        $yesterday = date('Y-m-d',strtotime('-1 day'));
        $today = date('Y-m-d',time());
        $tomorrow = date('Y-m-d',strtotime('+1 day'));
        $yesterday = strtotime($yesterday);
        $today = strtotime($today);
        $tomorrow = strtotime($tomorrow);;
        $data1 = array($yesterday,$today);
        $data2 = array($today,$tomorrow);
        $money['yesterday'] = (float) M("MoneyLogs")->where( array('uid'=>$this->uid,'ctime'=>array('between',$data1)) )->sum('money');
        $money['today'] = (float) M("MoneyLogs")->where( array('uid'=>$this->uid,'ctime'=>array('between',$data2)) )->sum('money');
        $money['all'] = (float) M("MoneyLogs")->where( array('uid'=>$this->uid) )->sum('money');

        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->assign("money",$money);
        $this->display();
    }


    /*
     * 结算记录
     */
    public function moneylogs(){
        $logs['all'] = (float) M("moneyLogs")->where( array('uid'=>$this->uid) )->sum('money');
        $logs['yes'] = (float) M("CaseLogs")->where( array('uid'=>$this->uid) )->sum('money');
        $logs['no'] = (float) $logs['all'] - $logs['yes'];

        $map['uid'] = $this->uid;
        $count = M("CaseLogs")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("CaseLogs")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        foreach($list as $key=>$vo){
            $list[$key]['operate'] = M("Admin")->where( array('id'=>$vo['operate_id']) )->getField('username');
            $list[$key]['license'] = M("CaseLogsLicense")->where( array('pid'=>$vo['id']) )
                                        ->order('id desc')->getField('license');
        }
        $page = $p->show();

        $this->assign("list",$list);
        $this->assign("page",$page);
        $this->assign("logs",$logs);
        $this->display();
    }

}