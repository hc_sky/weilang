<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class SetController extends AdminController {

    public function _initialize(){
        parent::_initialize();
        $this->model = new \Admin\Model\AdminModel();
    }

    /*
     * 设置
     */
    public function index(){
        if(IS_POST){
            if($_POST['content']){
                self::updateAgreement(1,$_POST['content']);
                unset($_POST['content']);
            }
            foreach($_POST as $key=>$vo){
                $result = M("SystemConfig")->where(array('name'=>$key))->save(array('value'=>$vo));
                //echo M()->getLastSql().'<br>';
            }
            if($result !== false){
                $this->ajaxReturn( array('data'=>0,'info'=>'保存成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'保存失败','status'=>0) );
            }
        }else{
            $config = M("SystemConfig")->order('FIELD(`id`,1,2,12,3,4,5,6,7,8,9,10)')->select();
            $content = M("SystemArticle")->where( array('id'=>1) )->getField('content');
            $this->assign("config",$config);
            $this->assign("content",$content);
            $this->display();
        }
    }


    /*
     * 管理员列表
     */
    public function manage(){
        $role = array('超级管理员','管理员','编辑者');
        $role_status = array('待审核','正常','禁用','删除');
        if($_POST['keyword']){
            //$map['realname'] = array('like','%'.$_POST['keyword'].'%');
            $map['_string'] = "realname like '%".$_POST['keyword']."%' or username like '%".$_POST['keyword']."%'";
        }
        $map['id'] = array('neq',1);
        $map['status'] = array('neq',3);
        $map['role'] = array('in',array(0,1,2));
        $count = M("Admin")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("Admin")->where($map)->limit($p->firstRow.','.$p->listRows)->select();
        foreach($list as $key=>$vo){
            $list[$key]['role'] = $role[$vo['role_id']];
            $list[$key]['status'] = $role_status[$vo['status']];
            $list[$key]['lasttime'] = M("AdminLoginLogs")->where( array('uid'=>$vo['id']) )->getField('ctime');
        }
        $page = $p->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }


    /*
     * 保存管理员信息
     */
    public function edit_manage(){
        if(IS_POST){
            if($_POST['password']){
                $_POST['pass'] = md5($_POST['password']);
            }
//            $_POST['last_login_ip'] = get_client_ip();
//            $_POST['last_login_time'] = time();
            if($_POST['id']==1){
                $this->ajaxReturn( array('data'=>0,'info'=>'参数错误','status'=>0) );
            }
            if($_POST['id']){
                $result = M("Admin")->where(array('id'=>$_POST['id']))->save($_POST);
            }else{
                $result = M("Admin")->add(array(
                    'username' => $_POST['username'],
                    'pass' => $_POST['pass'],
                    'role' => 0,
                    'parent_uid' => 0,
                    'realname' => $_POST['realname'],
                    'money' => 0,
                    'status' => 1,
                    'ctime' => time()
                ));
            }
            if($result !== false){
                $this->ajaxReturn( array('data'=>$result,'info'=>'账号保存成功','status'=>1,'url'=>U('set/manage')) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'账号保存失败','status'=>0) );
            }
        }else{
            $uid = intval($_GET['uid']);
            $user = M("Admin")->where(array('id'=>$uid))->find();
            $this->assign('user',$user);
            $this->display();
        }
    }

    /*
     * 管理员操作
     * ids：账号ID
     * operate：1恢复，2禁用，3删除
     */
    public function operateManage(){
        $ids = explode(',',$_POST['ids']);
        $operate = $_POST['operate'];
        if( !in_array($operate,array(0,1,2,3)) ){
            $this->ajaxReturn( array('data'=>0,'info'=>'不允许操作','status'=>0) );
        }
        if( count($ids)==1 ){
            $result = M("Admin")->where( array('id'=>$ids[0]) )->save( array('status'=>$operate) );
        }else{
            foreach($ids as $vo){
                $result = M("Admin")->where( array('id'=>$vo) )->save( array('status'=>$operate) );
            }
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

    /*
     * 更新用户协议
     * content：协议内容
     */
    public function updateAgreement($aid,$content){
        $id = M("SystemArticle")->where( array('id'=>$aid) )->find();
        if( empty($id) ){
            M("SystemArticle")->add(array(
                'typeid' => 1,
                'content' => $_POST['content'],
                'ctime' => time(),
            ));
        }else{
            M("SystemArticle")->where( array('id'=>$aid) )->save( array('content'=>$_POST['content']) );
        }
    }


}