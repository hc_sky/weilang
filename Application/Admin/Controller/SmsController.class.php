<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;
//use Admin\ORG\OLERead;
//use Admin\ORG\Spreadsheet_Excel_Reader;
use Home\ORG\String;

class SmsController extends AdminController {

    public function _initialize(){
        parent::_initialize();

    }

    /*
     *
     */
    public function index(){
        if( !empty($_GET['role']) ){
            $map['role'] = $_GET['role'];
        }
        if( !empty($_GET['province2']) ){
            $map['province'] = $_GET['province2'];
        }
        if( !empty($_GET['city2']) ){
            $map['city'] = $_GET['city2'];
        }
        if( !empty($_GET['area2']) ){
            $map['area'] = $_GET['area2'];
        }

        $count = M("SmsUser")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("SmsUser")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        $page = $p->show();
        $role = M("SmsUser")->group('role')->getField('role',true);
        $province = M("SmsUser")->group('province')->getField('province',true);
        $city = M("SmsUser")->group('city')->getField('city',true);
        $area = M("SmsUser")->group('area')->getField('area',true);

        $this->assign('count',$count);
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->assign('role',$role);
        $this->assign('province',$province);
        $this->assign('city',$city);
        $this->assign('area',$area);
        $this->display();
    }

    public function load(){
        import("Admin.ORG.Excel",'','.php');
        $excel = new \Spreadsheet_Excel_Reader($_FILES['people']['tmp_name']);
        $file = $excel->dump2();

        foreach($file as $key=>$vo){
            switch( $key%6 ){
                case 0: $data['name'] = $vo;    break;
                case 1: $data['phone'] = $vo;    break;
                case 2: $data['province'] = $vo;    break;
                case 3: $data['city'] = $vo;    break;
                case 4: $data['area'] = $vo;    break;
                case 5: $data['role'] = $vo;    break;
            }
            if( $key%6 == 5){
                $data['ctime'] = time();
                $data['status'] = 1;
                $data['code'] = String::rand_string(10);
                //dump($data);
                $result = M("SmsUser")->add($data);
                unset($data);
            }
        }
        if($result){
            $this->ajaxReturn( array('data'=>0,'info'=>'导入成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'导入失败','status'=>0) );
        }
    }

    /*
     * 短信发布记录
     */
    public function logs(){
        if($_POST['keyword']){
            $map['phone'] = array('like','%'.$_POST['keyword'].'%');
        }
        if($_GET['status_read']==1){
            $map['click'] = array('gt',0);
        }elseif($_GET['status_read']==2){
            $map['click'] = 0;
        }
        if($_GET['status_register']==1){
            $phones = M("Member")->where( array('pay'=>array('gt',0)) )->getField('phone',true);
            $map['phone'] = array('in',$phones);
        }
        $count = M("SmsUserLogs")->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("SmsUserLogs")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        if($_GET['status_register']==1){
            foreach($list as $key=>$vo){
                $list[$key]['register_status'] = 1;
            }
        }
        $page = $p->show();
        $this->assign('count',$count);
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }

    /*
     * 设置短信内容
     */
    public function set(){
        if(IS_POST){
            if($_POST['id']){
                $result = M("SmsTemplete")->where( array('id'=>1) )->save( array('content'=>$_POST['content']) );
            }else{
                M("SmsTemplete")->add( array('content'=>$_POST['content']) );
            }
            if($result !== false){
                $this->ajaxReturn( array('data'=>0,'info'=>'保存成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'保存失败','status'=>0) );
            }
        }else{
            $templete = M("SmsTemplete")->where( array('id'=>1) )->find();
            $this->assign('templete',$templete);
            $this->display();
        }
    }




    /*
     * 发送行业短信
     */
    public function smsLogs($log){
        if($log['phone']){
            $code = String::rand_string(10);
            M("SmsUserLogs")->add(array(
                'name' => $log['name'],
                'phone' => $log['phone'],
                'ctime' => time(),
                'code' => $code,
            ));
            $content = M("SmsTemplete")->where( array('id'=>1) )->getField('content');
            $content = $content . "了解详情：http://www.weilangcn.com/?token={$code}";
        }
        return $content;
    }


    /*
     * 操作
     * ids：用户ID
     * stauts：操作，0删除，2冻结
     */
    public function operateSms(){
        $ids = explode(',',$_POST['ids']);
        $operate = intval($_POST['operate']);
        if($operate==1){
            //选择发送
            foreach($ids as $vo){
                $log = M("SmsUser")->where( array('id'=>$vo) )->field('name,phone')->find();
                $content = self::smsLogs($log);
                if( $log['phone'] && $content ){
                    $msg = D("Aliyun")->sms($log['phone'], $content);
                }
            }
            $msg = json_decode($msg,true);
            if($msg['resid']==0){
                M("SmsUserLogs")->where( array('phone'=>$log['phone'],'status'=>0) )->save( array('status'=>1) );
                $result = true;
            }else{
                $result = false;
            }
        }elseif($operate==2){
            //一键发送
            $map['id'] = array('gt',329);
            $list = M("SmsUser")->where( array('sms_num'=>0) )->select();
            foreach($list as $vo){
                $content = self::smsLogs($vo);
                if( $vo['phone'] && $content ){
                    $msg = D("Aliyun")->sms($vo['phone'], $content);
                }
            }
            $msg = json_decode($msg,true);
            if($msg['resid']==0){
                M("SmsUserLogs")->where( array('phone'=>$vo['phone'],'status'=>0) )->save( array('status'=>1) );
                $result = true;
            }else{
                $result = false;
            }
        }elseif($operate==0){
            foreach($ids as $vo){
                if($vo){
                    $result = M("SmsUser")->where( array('id'=>$vo) )->delete();
                }
            }
        }
        //echo M()->getLastSql();
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }

}