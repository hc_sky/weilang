<?php
namespace Admin\Controller;
use Think\BaseController;
use Admin\Controller\AdminController;

class StatisticsController extends AdminController {

    public function _initialize(){
        parent::_initialize();
        $this->model = new \Admin\Model\AdminModel();
        $admin = M("Admin")->where( array('id'=>$this->uid) )->find();
        if($admin['role']==3){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $this->uids = M("Member")->where( array('parent_uid'=>array('in',$uids)) )->getField('uid',true);
        }elseif($admin['role']==5){
            $uids = M("Admin")->where( array('parent_uid'=>$this->uid,'role'=>3) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $uids = M("Admin")->where( array('parent_uid'=>array('in',$uids),'role'=>4) )->getField('id',true);
            if( empty($uids) ){ $uids[0] = -1; }
            $this->uids = M("Member")->where( array('parent_uid'=>array('in',$uids)) )->getField('uid',true);
        }elseif($admin['role']==4){
            $this->uids = M("Member")->where( array('parent_uid'=>$this->uid) )->getField('uid',true);
        }
        if( empty($this->uids) ){ $this->uids = array(-1); }
        $this->role = $admin['role'];
        $this->assign('thisrole',$this->role);
    }

    /*
     * 文章列表
     */
    public function article(){
        $Model = new \Home\Model\MemberModel();
        if( in_array($this->role,array(3,4,5)) ){
            $map['a.uid'] = array('in',$this->uids);
        }
        if($_GET['uid']){
            $map['m.uid'] = intval($_GET['uid']);
        }else{
            $map['m.uid'] = array('gt',0);
        }
        if($_GET['type']){
            $map['a.status'] = 0;
        }else{
            $map['a.status'] = 1;
        }
        $count = M("Article")->join('as a left join yx_member as m on a.uid=m.uid')->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("Article")->join('as a left join yx_member as m on a.uid=m.uid')
                ->field('a.*, m.parent_uid,m.phone,m.relation_id')
                ->where($map)
                ->limit($p->firstRow.','.$p->listRows)
                ->order('a.id desc')
                ->select();
        foreach($list as $key=>$vo){
            if( in_array($this->role,array(3,5)) ){
                $list[$key]['phone'] = substr($vo['phone'],0,3). "****" . substr($vo['phone'],7,11);
            }
//            $list[$key]['source'] = $Model->source($vo['parent_uid']);
            $list[$key]['source2'] = D("Admin")->source2($vo['parent_uid'],$this->role);
            $list[$key]['link'] = 'http://'.$_SERVER['SERVER_NAME'].'/Home/article/item?id='.$vo['uid'].'&aid='.$vo['id'];

        }
        $page = $p->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }

    public function ad(){
        $Model = new \Home\Model\MemberModel();
        $type = array(1=>'链接',2=>'拨号',3=>'QQ');
        //$map['a.status'] = 1;
        if( in_array($this->role,array(3,4,5)) ){
            $map['a.uid'] = array('in',$this->uids);
        }
        $map['a.image'] = array('not in', array('','/Public/images/mobile/ad_add.png') );
        if($_GET['uid']){
            $map['m.uid'] = intval($_GET['uid']);
        }else{
            $map['m.uid'] = array('gt',0);
        }
        $count = M("AdDetail")->join('as a left join yx_member as m on a.uid=m.uid')->where($map)->count();
        $p = new \Think\Page($count,20);
        $list = M("AdDetail")->join('as a left join yx_member as m on a.uid=m.uid')
                ->field('a.*, m.parent_uid,m.phone,m.relation_id')
                ->where($map)
                ->limit($p->firstRow.','.$p->listRows)
                ->order('a.aid desc, a.id asc ')
                ->select();
        foreach($list as $key=>$vo){
            if( in_array($this->role,array(3,5)) ){
                $list[$key]['phone'] = substr($vo['phone'],0,3). "****" . substr($vo['phone'],7,11);
            }
            //$list[$key]['source'] = $Model->source($vo['parent_uid']);
            $list[$key]['source2'] = D("Admin")->source2($vo['parent_uid'],$this->role);
            $list[$key]['type'] = $type[$vo['type']];
            if($list[$key]['aid'] != $list[$key-1]['aid']){
                $num[$vo['aid']]['num'] = 1;
            }else{
                $num[$vo['aid']]['num'] = $num[$vo['aid']]['num'] + 1;
            }
            $list[$key]['ad_status'] = M("Ad")->where( array('aid'=>$vo['aid']) )->getField('first');
        }

        $page = $p->show();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->assign('num',$num);
        $this->display();
    }




}