<?php
namespace Admin\Model;
use Think\Model;

class AdminModel extends Model{

	//protected $trueTableName = '';


	/*
	 * 更新账户信息
	 */
	public function baseAccount($field){
		if( in_array($field['role'],array(3,4)) && $field['parent_uid']<1 ){
			return array('data'=>0,'info'=>'请选择所属上级','status'=>0);
		}else{
			$check = M("AdminDetail")->where( array('phone'=>$field['phone'],'uid'=>array('neq',$field['id'])) )->count();
			if($check){
				return array('data'=>0,'info'=>'手机号码已存在','status'=>0);
			}
		}
		if($field['id']){
			$id = $field['id'];
			unset($field['id']);
			if(empty($field['pass'])){
				unset($field['pass']);
			}else{
				$field['pass_code'] = $field['pass'];
				$field['pass'] = md5($field['pass']);
			}
			unset($field['role']);
			M("Admin")->where( array('id'=>$id) )->save($field);
			$result = M("AdminDetail")->where( array('uid'=>$id) )->save($field);
		}else{
			if( empty($field['username']) ){
				$field['username'] = $field['phone'];
			}
			$field['ctime'] = time();
			$field['pass_code'] = $field['pass'];
			$field['pass'] = md5($field['pass']);
			$field['uid'] = $uid = M("Admin")->add($field);
			if($field['role']==4){
				$field['code'] = self::createCode();
				$QRcode = new \Home\Model\QRcodeModel();
				$field['invite'] = $QRcode->createQRCode($uid);
				$field['product'] = $QRcode->createQRCode($uid,2);
			}
			$result = M("AdminDetail")->add($field);
			self::updatePass($uid,$field['pass_code']);
			$Model = new \Home\Model\MemberModel();
			$msg = $Model->register( array('phone'=>$field['phone'],'pass'=>$field['pass_code']) );
			M("Member")->where( array('uid'=>$msg['data']) )->save(array(
				'pay' => 0,
				'regtime' => 0,
				'outtime' => strtotime('+3 year'),
				'isaudit' => 5,
				'relation_id' => $field['uid']
			));
		}
		if($result !== false){
			return array('data'=>$result,'info'=>'保存成功','status'=>1);
		}else{
			return array('data'=>$result,'info'=>'保存失败','status'=>0) ;
		}
	}

	public function createCode(){
		$x32 = "123456789";
		$code = null;
		$max = strlen($x32) - 1;
		for($i=0;$i<4;$i++){
			$code .= $x32[rand(0,$max)];		//rand($min,$max)生成介于min和max两个数之间的一个随机整数
		}
		//dump($code);
		return $code;
	}

	/*
	 * 所属上司
	 * parent_uid：上司ID
	 */
	public function source($uid,$role=0){
		if($uid){
			//$Admin = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid');
			$sale = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$uid) )->field('d.realname,m.parent_uid')->find();
			$manage = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$sale['parent_uid']) )->field('d.realname,m.parent_uid')->find();
			$leader = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$manage['parent_uid']) )->field('d.realname,m.parent_uid')->find();
			if($role==4){
				$parents[] = $manage['realname'].'<font class="gray">（经理）</font>';
				$parents[] = $leader['realname'].'<font class="gray">（总监）</font>';
			}else{
				$parents[] = $manage['realname'];
				$parents[] = $leader['realname'];
			}
			return $parents;
		}else{
			return false;
		}
	}

	/*
	 * 来源下级
	 * parent_uid：业务员ID
	 * role：当前登录用户角色
	 */
	public function source2($uid,$role=0){
		if($uid){
			$sale = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$uid) )->field('d.realname,m.parent_uid')->find();
			$manage = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$sale['parent_uid']) )->field('d.realname,m.parent_uid')->find();
			$leader = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$manage['parent_uid']) )->field('d.realname,m.parent_uid')->find();
			if($role==3){
				$parents[] = $sale['realname'].'<font class="gray">（业务员）</font>';
			}elseif($role==5){
				$parents[] = $sale['realname'].'<font class="gray">（业务员）</font>';
				$parents[] = $manage['realname'].'<font class="gray">（经理）</font>';
			}else{
				$parents[] = $sale['realname'].'<font class="gray">（业务员）</font>';
				$parents[] = $manage['realname'].'<font class="gray">（经理）</font>';
				$parents[] = $leader['realname'].'<font class="gray">（总监）</font>';
			}
			return $parents;
		}else{
			return false;
		}
	}

	/*
	 * 修改密码记录
	 */
	protected function updatePass($uid,$pass){
		M("AdminPass")->add(array(
			'uid' => $uid,
			'pass' => $pass,
			'ctime' => time()
		));
	}

}