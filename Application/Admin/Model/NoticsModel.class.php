<?php
namespace Admin\Model;
use Think\Model;
use Home\ORG\String;

class NoticsModel extends Model{



	/*
     * 通知入库
	 * Model: 模型
	 * id：删除项ID
	 * type：删除项类型，1文章，2广告图
     */
	public function create($id, $type, $uid)
	{
		M("Notics")->add(array(
			'uid' => $uid,
			'delete_id' => $id,
			'type' => $type,
			'created_time' => time(),
			'read' => 0
		));
	}

    /*
     * 检测提示通知状态
     */
	public function check($uid){
		$types = M("Notics")->where( array('uid'=>$uid,'read'=>0) )->getField('type',true);
		return $types;
	}


}