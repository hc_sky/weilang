<?php
namespace Agent\Controller;
use Think\BaseController;

class AgentBaseController extends BaseController {

    function _initialize(){
        parent::_initialize();
        $this->check_member();
    }

    public function check_member(){
        if( cookie("Agent") ){
            $this->uid = cookie("Agent")['uid'];
            $uid = cookie("Agent")['uid'];
            $this->assign("Agent", cookie("Agent"));
        }else{
            $this->redirect("Public/login");
        }
    }



}