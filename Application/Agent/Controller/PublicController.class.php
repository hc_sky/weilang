<?php
namespace Agent\Controller;
use Think\BaseController;

class PublicController extends BaseController {

    public function login(){
        if( cookie("Agent") ){
            //$this->redirect("/Agent/index");
        }
        self::wehcat_js();
        $agent_num = (int) M("AgentsNum")->where( array('id'=>1) )->getField('value');
        $this->assign('agent_num',$agent_num + 30000);
        $this->display();
    }


    /*
     * 登录
     */
    public function interface_login(){
        $phone = $_POST['phone'];
        $pass = $_POST['pass'];
        $Agent = new \Agent\Model\AgentModel();
        $msg = $Agent->login($phone,$pass);
        $this->ajaxReturn($msg);
    }

    /*
     * 注册会员
     */
    public function register(){
        if(IS_POST){
            $log_id = intval($_POST['id']);
            if($log_id<1){
                $this->ajaxReturn( array('info'=>'参数错误','status'=>0) );
            }
            $log = M("RegisterLogs")->where( array('id'=>$log_id) )->find();
            $uid = M("Admin")->add(array(
                'username' => $log['phone'],
                'pass' => md5($log['pass']),
                'role' => 4,
                'realname' => $_POST['realname'],
                'status' => 1,
                'ctime' => time()
            ));
            $Model = new \Admin\Model\AdminModel();
            $QRcode = new \Home\Model\QRcodeModel();
            $result = M("AdminDetail")->add(array(
                'uid' => $uid,
                'realname' => $_POST['realname'],
                'phone' => $log['phone'],
                'code' => $Model->createCode(),
                'sex' => intval($_POST['sex']),
                'weixin' => $_POST['weixin'],
                'province' => $_POST['province'],
                'city' => $_POST['city'],
                'pass_code' => $log['pass'],
                'code_invite' => $QRcode->createQRCode($uid),
                'code_product' => $QRcode->createQRCode($uid,2),
            ));
            if($result){
                $this->ajaxReturn( array('data'=>$uid,'info'=>'注册成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'注册失败','status'=>0) );
            }
        }else{
            self::wehcat_js();
            $code = M("AdminDetail")->where( array('uid'=>$_GET['uid']) )->getField('code');
            $this->assign('code',$code);
            $this->display();
        }
    }

    /*
     * 注册第二步
     */
    public function step2(){
        $province = M("Province")->select();
        $this->assign('province',$province);
        $this->display();
    }



    /*
     * 忘记密码
     */
    public function forget(){
        if(IS_POST){
            $SMS = new \Home\Model\SmsModel();
            $msg = $SMS->checkPhoneCode($_POST['phone'],$_POST['code']);
            if($msg['status']==1){
                $uid = M("AdminDetail")->where( array('phone'=>$_POST['phone']) )->getField('uid');
                if($uid){
                    $result = M("Admin")->where( array('id'=>$uid) )->save( array('pass'=>md5($_POST['pass'])) );
                }
            }
            if($result !== false){
                $this->ajaxReturn( array('data'=>0,'info'=>'修改成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'修改失败','status'=>0) );
            }
        }else{
            $this->display();
        }
    }

    /*
     * 退出登录
     */
    public function layout(){
        $Agent = new \Agent\Model\AgentModel();
        $Agent->exitLogin();
        $this->ajaxReturn( array('data'=>0,'info'=>'退出登录','status'=>1) );
    }


    public function wehcat_js(){
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();
        $this->assign('signPackage',$signPackage);
    }









}