<?php
namespace Agent\Model;
use Think\Model;

class AgentModel extends Model{

	protected $trueTableName = "yx_admin";


	/*
	 * 登录
	 */
	Public function login($username,$pass){
		$user = M("Admin")->where( array('username'=>$username,'role'=>array('in',array(3,4,5))) )
				->field('id,username,realname,pass,role,status')
				->find();
		if( empty($user) ){
			return array('data'=>0,'info'=>'账户不存在','status'=>0);
		}elseif($user['status']==2){
			return array('data'=>0,'info'=>'账户被禁用','status'=>2);
		}elseif( md5($pass) != $user['pass']){
			return array('data'=>0,'info'=>'密码不正确','status'=>0);
		}elseif($user['status']==1){
			unset($user['pass']);
			cookie("Agent", $user);
			self::loginLogs($user['id']);
			return array('data'=>0,'info'=>'登录成功','status'=>1);
		}
	}



	/*
	 * 注册
	 */
	public function register($data){

	}







	/*
	 * 登录后保存cookie
	 */
	public function init($user){
		cookie("Agent", $user);
	}

	/*
	 * 退出登录
	 */
	public function exitLogin(){
		cookie("Agent", null);
	}

	/*
	 * 登录日记
	 */
	public function loginLogs($uid){
		M("AdminLoginLogs")->add(array(
			'uid' => $uid,
			'ctime' => time(),
			'ip' => self::getRealIP()
		));
	}

	/*
	 * 获取真实IP
	 */
	public function getRealIP(){
		global $ip;
		if (getenv("HTTP_CLIENT_IP"))
			$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
			$ip = getenv("REMOTE_ADDR");
		else $ip = "Unknow";
		return $ip;
	}

}