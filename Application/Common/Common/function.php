<?php

//获取用户ip地址
function get_ip()
{
    $onlineip = '';
    if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
        $onlineip = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
        $onlineip = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
        $onlineip = getenv('REMOTE_ADDR');
    } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
        $onlineip = $_SERVER['REMOTE_ADDR'];
    }
    return $onlineip;
}


/*
 * 套餐
 */
function chooseWay($way){
    $choose = array( '体验', '季度', '半年', '一年','一个月','一天' );
    if( isset($way) ){
        return $choose[$way];
    }else{
        return $choose;
    }
}



/*
 * 支付方式
 */
function payWay($way){
    $pay =  array( '--', '支付宝', '微信' );
    if( isset($way) ){
        return $pay[$way];
    }else{
        return $pay;
    }
}

/*
 * 性别
 */
function toSex($value){
    $sex = array('','男','女');
    return $sex[$value];
}


/*
 * 职位
 */
function toRole($value){
    $sex = array('','男','女');
    return $sex[$value];
}

/*
 * 计算时间差
 * time：指定时间
 */
function timediff($time){
    $diff = floatval( (time() - $time) / 3600 );
    if($diff>0){
        $diff = ceil($diff);
    }else{
        $diff = -ceil( abs($diff) );
    }
    $date['day'] = intval($diff / 24);
    $date['hour'] = ceil($diff) % 24;
    //dump($date);
    return $date;
}

/*
 * 生成短链接
 */
function shortURL($url){
    $code = sprintf('%u', crc32($url));
    $surl = '';
    while($code){
        $mod = $code % 62;
        if($mod>9 && $mod<=35){
            $mod = chr($mod + 55);
        }elseif($mod>35){
            $mod = chr($mod + 61);
        }
        $surl .= $mod;
        $code = floor($code/62);
    }
    return $surl;

}


?>