<?php
namespace Home\Controller;
use Think\BaseController;
use Home\Controller\UserBaseController;



class AccountController extends UserBaseController {

    function _initialize(){
        parent::_initialize();
    }

    public function index(){
        $user = M("Member")->where(array('uid'=>$this->uid))->find();
        if( empty($user['face']) && $user['open_id'] && strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false){
            import('Common.Lib.Wechatpay.lib.WxPay', '', '.JsApiPay.php');
            $pay = new \JsApiPay();
            $openId = $pay->GetOpenid();
            $jssdk = new \Home\ORG\WxJssdk();
            $info = $jssdk->getWechatFace($openId);
            $user['face'] = $info['headimgurl'];
            M("Member")->where(array('uid'=>$this->uid))->save( array('open_id'=>$openId,'face'=>$info['headimgurl']) );
        }

//        import('Home.ORG.WxJssdk');
//        $jssdk = new \Home\ORG\WxJssdk();
//        $info = $jssdk->getWechatFace($user['open_id']);

        $user['date'] = timediff($user['outtime']);

        $Notics = new \Admin\Model\NoticsModel();
        $types = $Notics->check($this->uid);
        $this->assign('notic_articles',in_array(1,$types));
        $this->assign('notic_images',in_array(2,$types));

        $this->assign("user",$user);
        $this->display();
    }


    /*
     * 我的名片
     */
    public function card(){
        $user = M("Member")->where(array('uid'=>$this->uid))->find();
        if( is_numeric($user['role']) ){
            unset($user['role']);
        }
        //微信分享设置
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();

        $this->assign("user",$user);
        $this->assign('signPackage',$signPackage);
        $this->display();
    }

    /*
     * 保存我的微信
     */
    public function saveWeixin()
    {
        $card = $_POST['card'];
        if (empty($card)) {
            $this->ajaxReturn(array('data' => 0, 'info' => '请上传微信名片', 'status' => 0));
        }
        $result = M("Member")->where(array('uid' => $this->uid))->save(array('card' => $card));
        if ($result !== false) {
            $this->ajaxReturn(array('data' => 0, 'info' => '保存成功', 'status' => 1));
        } else {
            $this->ajaxReturn(array('data' => 0, 'info' => '保存失败', 'status' => 0));
        }
    }

    /*
     * 更新名片信息
     */
    public function saveInfo(){
        $_POST['info_finish'] = 1;
        foreach($_POST as $vo){
            if( empty($vo) ){
                $_POST['info_finish'] = 0;
            }
        }
        $result = M("Member")->where(array('uid'=>$this->uid))->save($_POST);
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'保存成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'保存失败','status'=>0) );
        }
    }

    /*
     * 修改密码
     * old_pass：旧密码
     * new_pass：新密码
     */
    public function pass(){
        if(IS_POST){
            $user = M("Member")->where(array('uid'=>$this->uid))->field('pass')->find();
            if($user['pass'] == md5($_POST['old_pass']) ){
                $result = M("Member")->where(array('uid'=>$this->uid))
                            ->save( array('pass'=>md5($_POST['new_pass'])) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'旧密码不正确','status'=>0) );
            }
            if($result !== false){
                cookie("User", null);
                $this->ajaxReturn( array('data'=>0,'info'=>'修改密码成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'修改密码失败','status'=>0) );
            }
        }else{
            $this->display();
        }
    }

    /*
     * 我的微信
     */
    public function weixin(){
        if(IS_POST){

        }else{
            $pic = M("Member")->where(array('uid'=>$this->uid))->getField('card');
            //微信分享设置
            import('Home.ORG.WxJssdk');
            $jssdk = new \Home\ORG\WxJssdk();
            $signPackage = $jssdk->GetSignPackage();
            $this->assign('signPackage',$signPackage);

            $this->assign('pic',$pic);
            $this->display();
        }
    }

    /*
     * 上传图片插件
     * type：图片类型，1名片，2广告
     * base64_string：压缩图片
     */
    public function interface_upload(){
        $Image = new \Home\Model\ImageModel();
        $msg = $Image->upload(array(
            'type' => $_POST['type'],
            'base64_string' => $_POST['base64_string'],
            'uid' => $this->uid
        ));
        $this->ajaxReturn($msg);
    }


    public function wechat(){
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();
        dump($signPackage);
        $this->assign('signPackage',$signPackage);
        $this->display();
    }

    public function wechat2(){
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();
        dump($signPackage);
        $this->assign('signPackage',$signPackage);
        $this->display();
    }

}