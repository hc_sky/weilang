<?php
namespace Home\Controller;
use Think\BaseController;
use Home\ORG\Picture;

class AdController extends UserBaseController {

    function _initialize(){
        parent::_initialize();
    }


    public function index(){
        $map['status'] = 1;
        $map['uid'] = $this->uid;
        $count = M("Ad")->where($map)->count();
        $p = new \Think\Page($count,10);
        $list = M("Ad")->where($map)->limit($p->firstRow.','.$p->listRows)->order('first desc')->select();
        foreach($list as $key=>$vo){
            $list[$key]['cover'] = M("AdDetail")->where(array('aid'=>$vo['aid'],'image'=>array('neq','')))->getField('image');
            if( empty($list[$key]['cover']) ){
                unset($list[$key]);
            }
        }
        $page = $p->show();

        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }

    /*
     * 设为首选
     * aid：广告ID
     * operate：操作，1设为首选，0取消首选
     */
    public function toDefault(){
        $aid = intval($_POST['aid']);
        $operate = intval($_POST['operate']);
        if($aid){
            if($operate==1){
                $article = (int) M("Article")->where( array('uid'=>$this->uid,'status'=>1) )->count();
                if($article<1){
                    $this->ajaxReturn( array('data'=>0,'info'=>'没有发布文章，不能推广','status'=>0) );
                }
            }
            M("Ad")->where( array('uid'=>$this->uid) )->save( array('first'=>0) );
            $result = M("Ad")->where( array('aid'=>$aid,'uid'=>$this->uid) )->save( array('first'=>$operate) );
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'设置成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'设置失败','status'=>0) );
        }
    }


    /*
     * 删除广告
     * aid：广告ID
     */
    public function deleteAd(){
        $aid = intval($_POST['aid']);
        if($aid){
            $result = M("Ad")->where( array('aid'=>$aid,'uid'=>$this->uid) )->delete();
            M("AdDetail")->where( array('aid'=>$aid,'uid'=>$this->uid) )->delete();
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'删除成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'删除失败','status'=>0) );
        }
    }

    /*
     * 删除广告
     * aid：广告ID
     */
    public function deleteAdDetail(){
        $id = intval($_POST['id']);
        if($id){
            $result = M("AdDetail")->where( array('id'=>$id,'uid'=>$this->uid) )->save( array('image'=>'') );
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'删除成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'删除失败','status'=>0) );
        }
    }

    /*
     * 编辑添加广告
     * images：图片地址
     * type：广告类型
     * url：广告链接
     */
    public function edit(){
        if(IS_POST){
            $aid = intval($_POST['aid']);
            $ids = explode(',',$_POST['ids']);
            $images = explode(',',$_POST['images']);
            $source = explode(',',$_POST['source']);
            $type = explode(',',$_POST['type']);
            $text = explode(',',$_POST['text']);
            if($aid){
                foreach($ids as $key=>$vo){
                    if( $images[$key] == '/Public/images/mobile/ad_add.png' ){
                        $images[$key] = '';
                    }
                    if($vo && $type[$key] ){
                        $url = self::toURL($type[$key],$text[$key]);
                        $result = M("AdDetail")->where( array('id'=>$ids[$key],'aid'=>$aid,'uid'=>$this->uid) )
                                ->save(array(
                                    'aid' => $aid,
                                    'uid' => $this->uid,
                                    'type' => $type[$key],
                                    'image' => $images[$key],
                                    'source' => $source[$key],
                                    'text' => $text[$key],
                                    'url' => $url,
                                    'status' => 1,
                                    'ctime' => time()
                                ));
                    }
                    //echo M()->getLastSql().'<br>';
                }
            }else{
                $aid = M("Ad")->add( array('uid'=>$this->uid,'ctime'=>time()) );
                //无广告图时设为默认
//                $count = M("Ad")->where( array('uid'=>$this->uid) )->count();
//                if( empty($count) ){
//                    $aid = M("Ad")->add( array('uid'=>$this->uid,'ctime'=>time(),'first'=>1) );
//                }else{
//                    $aid = M("Ad")->add( array('uid'=>$this->uid,'ctime'=>time()) );
//                }
                foreach($images as $key=>$vo){
                    if( $images[$key] == '/Public/images/mobile/ad_add.png' ){
                        $images[$key] = '';
                    }
                    if($type[$key]){
                        $url = self::toURL($type[$key],$text[$key]);
                        $result = M("AdDetail")->add(array(
                            'aid' => $aid,
                            'uid' => $this->uid,
                            'type' => $type[$key],
                            'image' => $images[$key],
                            'source' => $source[$key],
                            'text' => $text[$key],
                            'url' => $url,
                            'status' => 1,
                            'ctime' => time()
                        ));
                    }
                    //echo M()->getLastSql().'<br>';
                }
            }

            if($result){
                $this->ajaxReturn( array('data'=>0,'info'=>'保存成功','status'=>1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'保存失败','status'=>0) );
            }
        }else{
            $aid = intval($_GET['aid']);
            $list = M("AdDetail")->where( array('aid'=>$aid,'uid'=>$this->uid) )->select();
            $this->assign('list',$list);
            $this->display();
        }
    }

    /*
     * 广告转换
     */
    public function toURL($type,$text){
        $pre = array( 1=>'http://', 2=>'tel:', 3=>'');
        if($type==3){
            $url = 'http://wpa.qq.com/msgrd?v=3&uin='.$text.'&site=qq&menu=yes';
        }else{
            if($type==1){
                $pos = strpos($text,'http');
                if($pos !== false){
                    $url = $text;
                }else{
                    $url = $pre[$type]. $text;
                }
            }else{
                $url = $pre[$type]. $text;
            }

        }
        return $url;
    }

    public function add(){
        $this->display();
    }

    /*
     * 添加图文广告
     */
    public function create(){

        $this->display();
    }

    /*
     * 自定义广告（图文广告）
     */
    public function createpicture(){
        if( empty($_POST['image']) ){
            $this->ajaxReturn( array('data'=>0,'info'=>'背景图片不能为空','status'=>0) );
        }elseif( empty($_POST['text']) ){
            $this->ajaxReturn( array('data'=>0,'info'=>'文字广告语不能为空','status'=>0) );
        }
        $model = new \Home\ORG\Picture();
        $msg = $model->addText(array(
            'uid' => $this->uid,
            'image' => $_POST['image'],
            'text' => $_POST['text'],
            'size' =>  $_POST['size'],
            'color' =>  $_POST['color'],
        ));
        $this->ajaxReturn($msg);
    }



    /*
     * ajax选择图片上传类型
     * id：点击窗口ID
     */
    public function ajax_choose(){
        $this->display();
    }

    /*
     * ajax选择图片已有模板
     * id：点击窗口ID
     */
    public function choose_tpl(){
        if($_REQUEST['type']){
            $map['type'] = $_REQUEST['type'];
        }else{
            $map['type'] = 1;
        }
        $map['status'] = 1;
        $count = M("AdTemplate")->where($map)->count();
        $p = new \Think\Page($count,100);
        $list = M("AdTemplate")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        $page = $p->show();
        $ad_types = M("AdTemplateType")->where( array('status'=>1) )->select();
        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->assign('ad_types',$ad_types);
        $this->display('choose');
    }


    public function ajax_choose_tpl(){
        if($_POST['type']){
            $map['type'] = $_POST['type'];
        }
        $map['status'] = 1;
        $count = M("AdTemplate")->where($map)->count();
        $p = new \Think\Page($count,100);
        $list = M("AdTemplate")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();

        if($list){
            $this->ajaxReturn( array('data'=>$list,'info'=>'返回模板数据','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'暂无数据','status'=>0) );
        }
    }




    /*
     * 上传图片插件
     * type：图片类型，1名片，2广告
     * base64_string：压缩图片
     */
    public function interface_upload(){
        $Image = new \Home\Model\ImageModel();
        $msg = $Image->upload(array(
            'type' => $_POST['type'],
            'base64_string' => $_POST['base64_string'],
            'uid' => $this->uid
        ));
        if($msg['status']==1){
            $model = new \Home\ORG\Picture();
            //将图片剪切成固定大小，多余部分不要
            $msg['url'] = $model->imagecropper(".".$msg['url'], 530, 130);
        }
        $this->ajaxReturn($msg);
    }

}