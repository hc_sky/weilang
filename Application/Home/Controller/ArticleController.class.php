<?php
namespace Home\Controller;
use Think\BaseController;


class ArticleController extends BaseController {

    public function _initialize(){
        parent::_initialize();
    }


    public function index(){
        $map['status'] = 1;
        $map['uid'] = cookie("User")['uid'];
        $count = M("Article")->where($map)->count();
        $p = new \Think\Page($count,5);
        $list = M("Article")->where($map)->limit($p->firstRow.','.$p->listRows)->order('id desc')->select();
        foreach($list as $key=>$vo){
            $list[$key]['num_ad_click'] = M("AdClick")->where( array('aid'=>$vo['id']) )->sum('click');
            $list[$key]['link'] = 'http://'.$_SERVER['SERVER_NAME'].'/Home/article/item?id='.$vo['uid'].'&aid='.$vo['id'];
        }
        $page = $p->show();

        $this->assign('list',$list);
        $this->assign('page',$page);
        $this->display();
    }

    /*
     * 加载指定网页内容
     */
    public function item(){
        $aid = $_GET['aid'];
        $uid = $_GET['id'];
        $article = M("Article")->where( array('id'=>$aid,'uid'=>$uid) )->find();
        $Image = new \Home\Model\ImageModel();
        $ads = $Image->defaultAD($uid);
        M("Article")->where( array('id'=>$aid,'uid'=>$uid) )->setInc("click");

        $this->assign('url',$article['url']);
        $this->assign('title',$article['title']);
        $this->assign('content',$article['content']);
        $this->assign('ads',$ads);
        $this->display();
    }


    /*
     * 删除文章
     * aid：文章ID
     */
    public function deleteArticle(){
        $map['id'] = $_POST['aid'];
        $map['uid'] = cookie("User")['uid'];
        $result = M("Article")->where($map)->save( array('status'=>0) );
        if($result !== false){
            $count = M("Article")->where( array('uid'=>$map['uid'],'status'=>1) )->count();
            if($count<1){
                M("Ad")->where( array('uid'=>$map['uid']) )->save( array('first'=>0) );
            }
            $this->ajaxReturn( array('data'=>0,'info'=>'删除成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'删除失败','status'=>0) );
        }
    }










}