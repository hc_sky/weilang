<?php
namespace Home\Controller;
use Think\BaseController;
use Home\Controller\UserBaseController;

class CarController extends UserBaseController {

    function _initialize(){
        parent::_initialize();

    }

    /*
     * 购物车列表
     */
    public function index(){
        $order['money_all'] = 0;
        $map['uid'] = $this->uid;
        $map['status'] = 0;
        $list = M("GoodsCar")->where($map)->select();
        foreach($list as $key=>$vo){
            $goods = M("goods")->where(array('id'=>$vo['gid']))->field('title,cover,price')->find();
            $list[$key]['title'] = $goods['title'];
            $list[$key]['cover'] = $goods['cover'];
            $list[$key]['price'] = $goods['price'];
            $order['money_all'] += $vo['money'];
        }
        $order['money_all'] = round($order['money_all'],2);


        $this->assign("list",$list);
        $this->assign("order",$order);
        if( empty($list) ){
            $this->display('car_no');
        }else{
            $this->display();
        }
    }


    /*
     * ajax加入购物车页面
     * id：商品ID
     */
    public function ajax_add(){
        if( empty($this->uid) ){
            $this->ajaxReturn( array('data'=>0,'info'=>'用户未登陆','status'=>0) );
        }
        $id = intval($_GET['id']);
        $goods = M("Goods")->where(array('id'=>$id))->field("id,title,cover,price")->find();
        $this->assign("goods",$goods);
        $this->display();
    }

    /*
     * 加入购物车动作
     * id：商品ID
     * num：数量
     */
    public function doAddToCar(){
        $id = intval($_POST['id']);
        $num = intval($_POST['num']);
        $map['uid'] = $this->uid;
        $map['gid'] = $id;
        $map['status'] = array('neq',9);
        $order = M("GoodsCar")->where($map)->find();
        $price = M("Goods")->where(array('id'=>$id))->getField('price');
        if($order){
            $result = M("GoodsCar")->where( array('id'=>$order['id']) )->save(array(
                'num' => $order['num'] + $num,
                'money' => $order['money'] + $num * $price,
                'utime' => time()
            ));
        }else{
            $result = M("GoodsCar")->add(array(
                'gid' => $id,
                'uid' => $this->uid,
                'num' => $num,
                'money' => $num * $price,
                'ctime' => time()
            ));
        }
        $nun_now = M("GoodsCar")->where(array('uid'=>$this->uid,'status'=>0))->sum("num");
        if($result){
            $this->ajaxReturn( array('data'=>$nun_now,'info'=>'加入购物车成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>$nun_now,'info'=>'加入购物车失败','status'=>0) );
        }
    }


    /*
     * 编辑购物车
     */
    public function edit(){
        $map['uid'] = $this->uid;
        $map['status'] = 0;
        $list = M("GoodsCar")->where($map)->select();
        foreach($list as $key=>$vo){
            $goods = M("goods")->where(array('id'=>$vo['gid']))->field('title,cover,price')->find();
            $list[$key]['title'] = $goods['title'];
            $list[$key]['cover'] = $goods['cover'];
            $list[$key]['price'] = $goods['price'];
        }

        $this->assign("list",$list);
        $this->display();
    }

    /*
     * 更新购物车订单信息
     * id：订单ID
     * num：商品数量
     * money：商品总价
     */
    public function updateGoodsOrder(){
        if($_POST['id']<1 || $_POST['num']<1 || $_POST['money']<1){
            $this->ajaxReturn( array('data'=>0,'info'=>'确实必要参数','status'=>0) );
        }
        $id = intval($_POST['id']);
        $result = M("GoodsCar")->where(array('id'=>$id))->save(array(
            'num' => intval($_POST['num']),
            'money' => intval($_POST['money']),
            'utime' => time()
        ));
        if($result){
            $this->ajaxReturn( array('data'=>0,'info'=>'更新成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'更新失败','status'=>0) );
        }
    }

    /*
     * 删除购物车订单
     */
    public function deleteGoodsOrder(){
        $ids = explode(",",$_POST['ids']);
        if( count($ids)>1 ){
            foreach($ids as $vo){
                $result = M("GoodsCar")->where(array('id'=>$vo))->save(array('status'=>9));
            }
        }else{
            $result = M("GoodsCar")->where(array('id'=>$ids[0]))->save(array('status'=>9));
        }
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'删除成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'删除失败','status'=>0) );
        }
    }


    /*
     * 订单显示页面
     */
    public function order(){
        $order['money_all'] = 0;
        $map['uid'] = $this->uid;
        $map['status'] = 0;
        $list = M("GoodsCar")->where($map)->select();
        foreach($list as $key=>$vo){
            $goods = M("goods")->where(array('id'=>$vo['gid']))->field('title,cover,price')->find();
            $list[$key]['title'] = $goods['title'];
            $list[$key]['cover'] = $goods['cover'];
            $list[$key]['price'] = $goods['price'];
            $order['money_all'] += $vo['money'];
        }
        $order['money_all'] = round($order['money_all'],2);
        $address = D("Member")->address($this->uid,1);

        $this->assign("list",$list);
        $this->assign("order",$order);
        $this->assign("address",$address);
        $this->display();
    }


    /*
     * 验证是否存在收货地址
     */
    public function checkHaveAddress(){

        $this->display();
    }

    /*
     * 选择支付方式
     * car_id：购物车ID集合
     * money：商品总价
     * freight：运费
     * address_id：收货地址ID
     */
    public function alipay(){
        if(IS_POST){
            $result = M("GoodsOrder")->add(array(
                'uid' => $this->uid,
                'car_id' => implode(",",$_REQUEST['ids']),
                'money' => $_REQUEST['money'],
                'freight' => $_REQUEST['freight'],
                'address_id' => $_REQUEST['address_id'],
                'ctime' => time()
            ));
            if($result){
                M("GoodsCar")->where( array('id'=>array('in',$_REQUEST['ids'])) )
                    ->save( array('status'=>1) );
                $this->ajaxReturn( array('data'=>0,'info'=>'支付成功',1) );
            }else{
                $this->ajaxReturn( array('data'=>0,'info'=>'支付失败',0) );
            }
        }
        $this->display();
    }

}