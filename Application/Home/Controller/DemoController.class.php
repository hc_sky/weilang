<?php
namespace Home\Controller;
use Think\BaseController;

class DemoController extends BaseController {

    public function getURL(){
        $url = "http://www.gxrect.com:8094/VoteApp/servletapply/S_TouPiao?mac=null&jiluid=147";
        $info = $this->httpGet($url);
        $info = str_replace("'",'"',$info);
        $info = json_decode($info,true);
        //dump($info);
//        header('Content-Type:application/json; charset=utf-8');
//        exit( json_encode( array('data'=>$info,'info'=>'返回数据') ,0) );
        $this->ajaxReturn( array('data'=>$info,'info'=>'返回信息','status'=>1),'JSONP' );
    }

    public function postURL(){
        $url = 'http://www.huifang.cn/Topics/Estate/orderCustom';
        $data = array('phone'=>'13725527772','name'=>'sky');
        $info = self::http_request($url,$data);
        $info = json_decode($info,true);
        dump($info);
    }

    /*
     * GET请求远程地址
     * url：需请求的地址
     */
    private function httpGet($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);

        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }

    /*
     * POST请求远程地址
     * url：需请求的地址
     * data：参数
     */
    private function http_request($url, $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }


}