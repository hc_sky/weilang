<?php
namespace Home\Controller;
use Think\BaseController;
use Home\ORG\Picture;


class IndexController extends UserBaseController {

    public function _initialize(){
        parent::_initialize();
    }


    public function index(){
        $Notics = new \Admin\Model\NoticsModel();
        $types = $Notics->check($this->uid);
        $this->assign('notic_articles',in_array(1,$types));
        $this->assign('notic_images',in_array(2,$types));
        $this->display();
    }

    public function createpicture(){
        $model = new \Home\ORG\Picture();
        $photo = $model->addText(array(
            'uid' => $this->uid,
        ));

    }

    /*
     * 将转换的文章入库
     */
    public function convertArticle(){
        $uid = $this->uid;
        $msg = D("Article")->add($_POST['url'],$uid);
        $log_ad = M("Ad")->where( array('uid'=>$uid) )->count();
        if($log_ad<1){
            $msg['info'] = '您还没设置广告，快去设置吧';
        }
        $this->ajaxReturn($msg);
    }

    /*
     * 将图片保存到本地（事后调用）
     */
    public function tolocal(){
        $aid = intval($_POST['aid']);
        D("Article")->picFromWechatToLocal2($aid);
    }


    public function info(){
        //$file = 'http://mmbiz.qpic.cn/mmbiz_jpg/1Pdxd6Rq4ia2baibkYPoCsFI9af92p5EoicgxOqn6pddJkPz0PMbR8D1ia6XSxcG2xGbalnRr7cl82QNHBM9AXt1jQ/0?wx_fmt=jpeg';
        $file = 'http://mmbiz.qpic.cn/mmbiz_gif/1Pdxd6Rq4ia2baibkYPoCsFI9af92p5EoiciclvJXah8sFr46AyNhBI5QreWFSn4N5ScpNBl40U1xZoLYO4HPqDL7g/0?';
        $file = 'http://mmbiz.qpic.cn/mmbiz_gif/Aq9C81iclpxdYeGVSYGDw6Od1KkKcMeqicEM1XPKukEbkQCibKicxKINxCDvu5GdkDvgv6HXy7USF6X624l8Rpc4FA/0';

        dump( getimagesize($file) );die;

        $model = new \Think\Image();
        $result = $model->mime($file);
        dump( $result );
        dump( pathinfo($file) );
    }

    public function demo(){
        $url = 'https://mp.weixin.qq.com/s/BC7WmqcsU15Z191QYojANw';
        $content = file_get_contents($url);
        $preg_title = '#<title>([\s\S]*)</title>#iUs';
        $preg_cover2 = "/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png\.GIF|\.JPG|\.PNG]))[\'|\"].*?[\/]?>/";
        $preg_cover = "/<img.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
        $preg_iframe = "/<iframe.*?>/";
        preg_match_all($preg_title,$content,$title);
        preg_match_all($preg_cover,$content,$images);
        preg_match_all($preg_iframe,$content,$iframe);
        //dump($images);
        foreach($iframe[0] as $key=>$vo){
            $content = str_replace($vo, '', $content);
        }
        echo $content;
        //dump($iframe);
    }

    /*
     * 通知设为已读
     * type：通知类型，1文章通知，2图片通知
     */
    public function read(){
        $type = intval($_POST['type']);
        $result = M("Notics")->where( array('uid'=>$this->uid,'type'=>$type,'read'=>0) )->save( array('read'=>1) );
        if($result !== false){
            $this->ajaxReturn( array('data'=>0,'info'=>'操作成功','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'操作失败','status'=>0) );
        }
    }








}