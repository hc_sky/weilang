<?php
namespace Home\Controller;
use Think\BaseController;

class PublicController extends BaseController {

    public function login(){
        if( cookie("User") ){
            $this->redirect("/home/index");
        }
        self::wehcat_js();
        $agent_num = (int) M("AgentsNum")->where( array('id'=>1) )->getField('value');
        $this->assign('agent_num',$agent_num + 30000);
        $this->display();
    }

    public function wehcat_js(){
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();
        $this->assign('signPackage',$signPackage);
    }


    /*
     * 登录
     */
    public function interface_login(){
        $phone = $_POST['phone'];
        $pass = $_POST['pass'];
        $Member = new \Home\Model\MemberModel();
        $msg = $Member->login($phone,$pass);
        $this->ajaxReturn($msg);
    }

    /*
     * 注册会员
     */
    public function register(){
        if(IS_POST){
            $Member = new \Home\Model\MemberModel();
            $msg = $Member->register($_POST);
            if($msg['status']==1){
                $user = M("Member")->where( array('phone'=>$_POST['phone']) )->find();
                $Member->init($user);
            }
            $this->ajaxReturn($msg);
        }else{
            self::wehcat_js();
            $code = M("AdminDetail")->where( array('uid'=>$_GET['uid']) )->getField('code');
            $this->assign('code',$code);
            $this->display();
        }
    }

    /*
     * 检查用户是否已注册
     */
    public function checkPhoneLogs(){
        $log = M("Member")->where( array('phone'=>$_POST['phone'],'pay'=>array('EGT',0)) )->find();
        if($log){
            $this->ajaxReturn( array('data'=>0,'info'=>'手机号码已注册','status'=>0) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'手机号码未注册','status'=>1) );
        }
    }



    /*
     * 注册记录
     */
    public function registerLogs(){
        $SMS = new \Home\Model\SmsModel();
        $msg = $SMS->checkPhoneCode($_POST['phone'],$_POST['code']);
        if($msg['status']==1){
            $Member = new \Home\Model\MemberModel();
            $msg = $Member->registerLosg($_POST);
        }
        $this->ajaxReturn($msg);
    }

    /*
     * 获取短信验证码
     * phone：手机号码
     */
    public function interface_code()
    {
        $phone = $_REQUEST["phone"];
        $SMS = new \Home\Model\SmsModel();
        if (empty($phone)) {
            $this->ajaxReturn( array('data'=>0,'info'=>'手机号码错误','status'=>0) );
        }
        for ($i = 0; $i < 4; $i++) {
            $number .= rand(0, 9);
        }
        $r = $SMS->checkAccess($phone);
        if ($r !== true){
            $this->ajaxReturn( array('data'=>0,'info'=>$r,'status'=>0) );
        }
        $sms = $SMS->where(array('phone' => $phone))->order('id DESC')->find();
        if ( (time() - $sms['ctime']) > 60 ) {
            $SMS->saveSms($phone, $number);
            $result = $SMS->sendTemplateSMS($phone, array($number, '10'), '165168');
        } else {
            $this->ajaxReturn( array('data'=>0,'info'=>'请稍后再试','status'=>0) );
        }
        if ($result) {
            $this->ajaxReturn( array('data'=>0,'info'=>'验证码已发送','status'=>1) );
        } else {
            $this->ajaxReturn( array('data'=>0,'info'=>'验证码发送失败','status'=>0) );
        }
    }


    /*
     * 忘记密码
     */
    public function forget(){
        if(IS_POST){
            $SMS = new \Home\Model\SmsModel();
            $msg = $SMS->checkPhoneCode($_POST['phone'],$_POST['code']);
            if($msg['status']==1){
                $Member = new \Home\Model\MemberModel();
                $msg = $Member->changePass($_POST['phone'],$_POST['pass']);
            }
            $this->ajaxReturn($msg);
        }else{
            $this->display();
        }
    }

    /*
     * 退出登录
     */
    public function layout(){
        $Member = new \Home\Model\MemberModel();
        $Member->exitLogin();
        $this->ajaxReturn( array('data'=>0,'info'=>'退出登录','status'=>1) );
    }

    /*
     * H5产品介绍页
     */
    public function product(){
        $map['name'] = array('in',array('cfg_company','cfg_free'));
        $config = M("SystemConfig")->where($map)->select();
        foreach($config as $vo){
            $web[$vo['name']] = $vo['value'];
        }
        self::wehcat_js();
        $this->assign('web',$web);
        $this->display();
    }

    /*
     * 我的名片
     */
    public function card(){
        $uid = intval($_GET['uid']);
        $uid_cookie = cookie("User")['uid'];
        $isshow = 1;
        $user = M("Member")->where(array('uid'=>$uid))->find();
        if( strpos($user['website'],'http') === false && $user['website'] ){
            $user['website'] = 'http://'. $user['website'];
        }
        if( is_numeric($user['role']) ){
            unset($user['role']);
        }
        if($uid == $uid_cookie){
            $isshow = 0;
        }
        $system['cfg_company'] = M("SystemConfig")->where( array('name'=>'cfg_company') )->getField("value");
        $system['cfg_web'] = M("SystemConfig")->where( array('name'=>'cfg_web') )->getField("value");
        if( strpos($system['cfg_web'],'http') == false ){
            $system['cfg_web'] = 'http://'. $system['cfg_web'];
        }
        //微信分享设置
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();
        $this->assign('signPackage',$signPackage);

        $this->assign("user",$user);
        $this->assign("isshow",$isshow);
        $this->assign("system",$system);
        $this->display();
    }

    /*
     * 冻结、解除冻结
     */
    public function lock(){
        $uid = intval($_GET['uid']);
        $status = M("Member")->where( array('uid'=>$uid) )->getField('status');
        if($status==1){
            $this->redirect("Public/login");
        }
        $this->display();
    }

    /*
     * 用户协议
     */
    public function agreement(){
        $content = M("SystemArticle")->where( array('id'=>1) )->getField('content');
        $this->assign("content",$content);
        $this->display();
    }

    /*
     * 微信加好友
     */
    public function wechat(){
        $uid = intval($_GET['uid']);
        $user = M("Member")->where( array('uid'=>$uid) )->field('name,card')->find();
        $this->assign("user",$user);
        $this->display();
    }

    /*
     * 广告阅读数统计
     * aid：文章ID
     * id：广告ID
     */
    public function clickNum(){
        $aid = intval($_POST['aid']);
        $ad = intval($_POST['id']);
        $result = M("AdDetail")->where(array('id'=>$ad))->setInc("click");
        if($result !== false){
            $log = M("AdClick")->where(array('aid'=>$aid,'ad'=>$ad))->find();
            if($log){
                M("AdClick")->where(array('aid'=>$aid,'ad'=>$ad))->setInc("click");
            }else{
                M("AdClick")->add(array('aid'=>$aid,'ad'=>$ad,'click'=>1));
            }
            //echo M()->getLastSql().'<br>';
            $this->ajaxReturn( array('data'=>0,'info'=>'点击数加','status'=>1) );
        }else{
            $this->ajaxReturn( array('data'=>0,'info'=>'点击数不变','status'=>0) );
        }
    }

    /*
     * 微信支付
     */
    public function wechatpay(){
        import('Seller.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $signPackage = $jssdk->GetSignPackage();
        dump($signPackage);
        $this->assign('signPackage',$signPackage);
        $this->display();
    }

    /*
     * 套餐选择
     * id：潜在客户ID
     * cancel：是否取消，1：取消支付（需改变用户状态）
     */
    public function buy(){
        $out_status = 1;
        $map['name'] = array("in",array('cfg_price1','cfg_price2','cfg_price3','cfg_price4','cfg_price5','cfg_free'));
        $config = M("SystemConfig")->where($map)->select();
        //dump($config);
        if($_GET['cancel']==1){
            self::cancelpay( intval($_GET['id']) );
        }
        if( cookie("User") ){
            $uid_out = cookie("User")['uid'];
            $member = M("Member")->where( array('uid'=>$uid_out) )->field('outtime,status')->find();
            if( time() > $member['outtime'] ){
                $out_status = 0;
            }elseif( $_GET['id']==0 && $_GET['cancel']==1 ){
                $out_status = 0;
            }
        }
        $on_free = M("SystemConfig")->where( array('name'=>'cfg_free') )->getField('value');
        if($on_free==1 && $out_status==1){
            $showfree = 1;
        }else{
            $showfree = 0;
        };

        $this->assign("config",$config);
        $this->assign("showfree",$showfree);
        $this->display();
    }

    /*
     * 套餐过期
     */
    public function tryout(){
        $map['name'] = array("in",array('cfg_price0','cfg_price1','cfg_price2','cfg_price3','cfg_free'));
        $config = M("SystemConfig")->where($map)->select();

        $this->assign("config",$config);
        $this->display();
    }


    /*
     * 微信支付
     * id：潜在客户ID
     * choose：套餐选择，0:体验套餐，1：季度套餐，2半年套餐，3一年套餐，4一月套餐，5一天套餐
     * uid：会员ID（套餐过期后重新购买时使用）
     */
    public function pay(){
        $id = (int) I('id', 0);
        $choose = (int) I('choose', 5);
        $uid = (int) I('uid', 0);
        //套餐转换
//        switch($choose){
//            case 1:	$choose_pay = 0;	break;
//            case 2:	$choose_pay = 1;	break;
//            case 3:	$choose_pay = 2;	break;
//            case 4:	$choose_pay = 3;	break;
//        }
        $price = M("SystemConfig")->where( array('name'=>'cfg_price'.$choose) )->getField('value');

        $Member = new \Home\Model\MemberModel();
        if($id){
            $msg2 = $Member->register( array('id'=>$id) );
            if($choose==0){
                $this->ajaxReturn($msg2);
            }
            $msg = $Member->payLogs($id,$choose,2,$msg2['data']);
        }elseif($uid){
            $msg = $Member->payLogs(0,$choose,2,$uid);
        }
        $paymentId = $msg['data'];
        $tags = array( 1=>'季度套餐',2=>'半年套餐', 3=>'一年套餐', 4=>'一月套餐', 5=>'一天套餐' );

        import('Common.Lib.Wechatpay.lib.WxPay', '', '.JsApiPay.php');
        $pay = new \JsApiPay();
        $WxPayApi = new \WxPayApi();
        $openId = $pay->GetOpenid();
        $input = new \WxPayUnifiedOrder();

        $tradeNo = $choose . str_pad($paymentId, 8, 0, STR_PAD_LEFT);
        $data = [
            'body' => '购买'. $tags[$choose],
            'attach' => $choose,
            'tradeNo' => $tradeNo,
            'totalFee' => $price * 100,
            //'totalFee' => $choose,
            'goodTag' => '微浪营销',
        ];
        $input->SetBody($data['body']);
        $input->SetAttach($data['attach']);
        $input->SetOut_trade_no($data['tradeNo']);
        $input->SetTotal_fee($data['totalFee']);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetGoods_tag($data['goodTag']);
        $input->SetNotify_url('http://www.weilangcn.com/home/public/callback');//
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $order = $WxPayApi::unifiedOrder($input);
        $jsApiParameters = $pay->GetJsApiParameters($order);

        //套餐显示
        $showfree = 1;
        $map['name'] = array("in",array('cfg_price1','cfg_price2','cfg_price3','cfg_price4','cfg_price5','cfg_free'));
        $config = M("SystemConfig")->where($map)->select();
        if( cookie("User") ){
            $uid_out = cookie("User")['uid'];
            $member = M("Member")->where( array('uid'=>$uid_out) )->field('outtime,status')->find();
            if( time() > $member['outtime'] ){
                $out_status = 0;
            }
        }
        if($config['5']['value']==1 && $out_status==1){
            $showfree = 1;
        }else{
            $showfree = 0;
        };

        $this->assign('set',json_decode($jsApiParameters, true) );
        $this->assign("config",$config);
        $this->assign('showfree',$showfree);
        $this->assign('id',$id);
        $this->display();
    }


    /*
     * 微信回调
     */
    public function callback(){
        date_default_timezone_set('PRC');
        import('Common.Lib.Wechatpay.WechatPay');
        $WXPay = new \WechatPay();

        //存储微信的回调
        $result = $WXPay->get_back_data();
        $filename = "WXPayLog.txt";
        $handle = fopen($filename,"w");
        $str = fwrite($handle, json_encode($result));
        fclose($handle);

        //验证签名，并回应微信。
        //对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
        //微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
        //尽可能提高通知的成功率，但微信不保证通知最终能成功。
        if(!$result){
            $WXPay->response_back('FAIL', '签名失败');
        }else{
            $WXPay->response_back('SUCCESS');
        }

        //==商户根据实际情况设置相应的处理流程，此处仅作举例=======
        if($result){
            if ($result["return_code"] == "FAIL") {
                //此处应该更新一下订单状态，商户自行增删操作
                //log_result($log_name,"【通信出错】:\n".$xml."\n");
            }elseif($result["result_code"] == "FAIL"){
                //此处应该更新一下订单状态，商户自行增删操作
                //log_result($log_name,"【业务出错】:\n".$xml."\
            }else{
                //此处应该更新一下订单状态，商户自行增删操作
                //log_result($log_name,"【支付成功】:\n".$xml."\n");
                //log_message('Error', __CLASS__ . ' ' . __FUNCTION__ . serialize($result));
                $tradeNo = (int) substr($result['out_trade_no'], -8);
                $result_log = M("PayLogs")->where(array('id' => $tradeNo))->save(array('status' => 1));
                if($result_log){
                    $log = M("PayLogs")->where(array('id' => $tradeNo))->field('id,uid,choose,price')->find();
                    $phone = M("Member")->where( array('uid'=>$log['uid']) )->getField('phone');
                    M("RegisterLogs")->where( array('phone'=>$phone) )->order('id desc')->limit(1)->save( array('status'=>1) );
                    $Model = new \Home\Model\MemberModel();
                    $Model->updateUserDate($log['uid'],$log['choose'],$result["openid"]);	//更新套餐使用日期
                    $Model->money(array('uid'=>$log['uid'],'money'=>$log['price'],'pid'=>$log['id']));	//提成

                    $tags = array( 1=>'季度套餐',2=>'半年套餐', 3=>'一年套餐', 4=>'一月套餐', 5=>'一天套餐' );
                    //微信公众号支付消息通知
                    self::sendsms(array(
                        'openid' => $result["openid"],
                        'transaction_id' => $result['transaction_id'],
                        'money' => $log['price'],
                        'pay' => $tags[$log['choose']],
                        'phone' => $phone,
                    ));
                }

                //可根据返回参数来改变不同数据表（支付需求）
                if ($result['attach'] == '1') {

                }

            }

            //商户自行增加处理流程,
            //例如：更新订单状态
            //例如：数据库操作
            //例如：推送支付完成信息
        }
    }


    /*
     * 取消支付
     * id：潜在客户ID
     */
    public function cancelpay($id){
        if($id){
            $phone = M("RegisterLogs")->where( array('id'=>$id) )->getField('phone');
            $pay = M("Member")->where( array('phone'=>$phone) )->order('uid desc')->getField('pay');
        }
        if($phone && $pay<1 ){
            M("RegisterLogs")->where( array('id'=>$id) )->save( array('status'=>0) );
            M("Member")->where( array('phone'=>$phone) )->order('uid desc')->limit(1)
                ->save( array('pay'=>-1,'isaudit'=>-1) );
        }
    }



    /*
     * 公众号支付成功消息通知
     */
    public function sendsms($field){
        //微信分享设置
        import('Home.ORG.WxJssdk');
        $jssdk = new \Home\ORG\WxJssdk();
        $info = $jssdk->sendSMS($field);

    }

    /*
     * 短信退订
     *
     */
    public function smscallback(){
        $result = M("SmsNocall")->add( array('phone'=>$_GET['phoneNum'],'ctime'=>time()) );
        $this->ajaxReturn( array('info'=>'退订成功','status'=>1) );
    }


}