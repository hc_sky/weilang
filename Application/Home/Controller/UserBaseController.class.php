<?php
namespace Home\Controller;
use Think\BaseController;

class UserBaseController extends BaseController {

    function _initialize(){
        parent::_initialize();
        $this->check_member();
    }

    public function check_member(){
        if( cookie("User") ){
            $this->uid = cookie("User")['uid'];
            $uid = cookie("User")['uid'];
            $this->assign("User", cookie("User"));
            self::outtime($uid);
        }else{
            $this->redirect("Public/login");
        }
    }

    /*
     * 用户过期
     */
    public function outtime($uid){
        $member = M("Member")->where( array('uid'=>$uid) )->field('outtime,status')->find();
        $outtime = $member['outtime'];

        if( empty($member) ){
            D("Member")->exitLogin();
            $this->redirect("Public/login");
        }elseif( time() > $outtime){
            $user = M("Member")->where( array('uid'=>$this->uid) )->field('phone,pay')->find();
            $id = M("RegisterLogs")->where( array('phone'=>$user['phone']) )->order('id desc')->getField('id');
            if( empty($id) ){
                $this->redirect("Public/tryout",array('uid'=>$this->uid));
            }elseif($user['pay']==0){
                $this->redirect("Public/tryout",array('id'=>$id));
            }else{
                $this->redirect("Public/tryout",array('id'=>$id,'type'=>'2'));
            }
        }elseif($member['status']==2){
            cookie('User',null);
            $this->redirect("Public/lock",array('uid'=>$this->uid));
        }
    }

}