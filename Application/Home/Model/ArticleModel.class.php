<?php
namespace Home\Model;
use Think\Model;

class ArticleModel extends Model{

	//protected $trueTableName = '';

	/*
	 * 更新商品信息
	 */
	public function add($url,$uid){
		if(empty($url)){
			return false;
		}
		$content = file_get_contents($url);
		$preg_title = '#<title>([\s\S]*)</title>#iUs';
		$preg_cover2 = "/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png\.GIF|\.JPG|\.PNG]))[\'|\"].*?[\/]?>/";
		$preg_cover3 = "/<[img|IMG].*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
		$preg_cover = "/<img.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
		$preg_iframe = "/<iframe.*?>/";
		preg_match_all($preg_title,$content,$title);
		//preg_match_all($preg_cover,$content,$images);
		preg_match_all($preg_iframe,$content,$iframe);
		foreach($iframe[0] as $key=>$vo){
			$content = str_replace($vo, '', $content);
		}
//		foreach($images[1] as $key=>$vo){
//			$topic = self::picFromWechatToLocal($vo);
//			$images[1][$key] = $topic;
//			$content = str_replace($vo, $topic, $content);
//		}
//		if($images[1][1]){
//			$cover = $images[1][1];
//		}else{
//			$cover = $images[1][0];
//		}
		$result = M("Article")->add(array(
				'uid' => $uid,
				'title' => $title[1][0],
				//'cover' => $cover,
				'url' => $url,
				'content' => $content,
				'ctime' => time()
		));
		if($result){
			return array('id'=>$uid,'data'=>$result,'info'=>'文章入库成功','status'=>1);
		}else{
			return array('id'=>$uid,'data'=>$result,'info'=>'文章入库失败','status'=>0) ;
		}
	}

	/*
     * 微信图片转成本地图片
	 * 文章入库前保存图片
	 * remote：远程图片
	 * local：本地图片
     */
	public function picFromWechatToLocal($remote){
		if( strpos($remote,'mmbiz.qpic.cn') != false ){
			$info = getimagesize($remote);
			switch($info['2']){
				case 1:	$suffix = '.gif';	break;
				case 2:	$suffix = '.jpg';	break;
				case 3:	$suffix = '.png';	break;
			}

			if($suffix){
				$local = './Upload/article/'.time().rand(1,999999).$suffix;
				//echo '原图：'.$remote.'----转换成-----'.$local.'----后缀是：'.$suffix;
				$Http = new \Org\Net\Http();
				$Http::curlDownload($remote,$local);
				return str_replace('./Upload/','/Upload/',$local);
			}
		}
		return $remote;
	}

	/*
     * 微信图片转成本地图片
	 * 文章入库后再保存图片
	 * remote：远程图片
	 * local：本地图片
     */
	public function picFromWechatToLocal2($aid){
		$content = M("Article")->where( array('id'=>$aid) )->getField('content');
		$preg_cover = "/<img.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
		preg_match_all($preg_cover,$content,$images);
		foreach($images[1] as $key=>$vo){
			$topic = self::picFromWechatToLocal($vo);
			$images[1][$key] = $topic;
			$content = str_replace($vo, $topic, $content);
		}
		if($images[1][1]){
			$cover = $images[1][1];
		}else{
			$cover = $images[1][0];
		}
		$result = M("Article")->where( array('id'=>$aid) )->save(array(
				'cover' => $cover,
				'content' => $content,
		));
		return true;
	}

}