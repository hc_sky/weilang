<?php
namespace Home\Model;
use Think\Model;

class ImageModel extends Model{

	protected $trueTableName = 'yx_images_upload';

	/*
	 * 图片上传
	 * type：图片类型，1名片，2广告
     * base64_string：压缩图片
	 */
	public function upload($field){
		$type = $field['type'];
		$base64_string = $field['base64_string'];
		$savepath = self::ToImagesFolder($type);
		$image = self::base64_to_img( $base64_string, $savepath );
		if($image){
			if( !in_array($field['type'],array(3)) ){
				self::logs($type,$image);	//上传记录
				self::update( array('type'=>$type,'image'=>$image,'uid'=>$field['uid'],'text'=>$field['text']) );
			}
			$info = getimagesize( "http://{$_SERVER['HTTP_HOST']}".$image);
			return array('url'=>$image,'width'=>$info[0],'height'=>$info[1],'info'=>'上传成功','status'=>1);
		}else{
			return array('url'=>$image,'width'=>$info[0],'height'=>$info[1],'info'=>'上传失败','status'=>0);
		}
	}

	function base64_to_img( $base64_string, $output_file ) {
		$ifp = fopen( $output_file, "wb" );
		fwrite( $ifp, base64_decode( $base64_string) );
		fclose( $ifp );
		$output_file = str_replace('./Upload/','/Upload/',$output_file);
		return( $output_file );
	}

	/*
	 * 图片文件夹
	 * type：图片地址类型，1名片，2广告,3管理后台
	 *
	 */
	function ToImagesFolder($type){
		$savename = uniqid().'.jpeg';   //localResizeIMG压缩后的图片都是jpeg格式
		if($type==1){
			$file = './Upload/card/'.$savename;
		}elseif($type==3){
			$file = './Upload/admin/'.$savename;
		}else{
			$file = './Upload/ad/'.$savename;
		}
		return $file;
	}

	/*
	 * 图片数据入库
	 * type：入库类型
	 * image：图片地址
	 * uid：用户ID
	 */
	public function update($field){
		if($field['type']==1){
			$result = M("Member")->where(array('uid'=>$field['uid']))->save( array('card'=>$field['image']) );
		}
		if($result !== false){
			return array('data'=>$result,'info'=>'入库成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'入库失败','status'=>0);
		}
	}

	/*
	 * 图片上传记录
	 */
	public function logs($type,$image){
		M('ImagesUpload')->add( array('type'=>$type,'file'=>$image, 'ctime'=>time()) );
	}


	/*
	 * 显示默认广告
	 * uid：用户ID
	 */
	public function defaultAD($uid){
		$map['a.uid'] = $uid;
		$map['a.first'] = 1;
		$map['d.status'] = 1;
		$map['d.image'] = array('neq','');
		$ads = M("Ad")->where($map)->join("as a left join yx_ad_detail as d on a.aid = d.aid")
				->field('d.id, d.aid, d.image, d.text, d.type, d.url')->select();
		return $ads;
	}

}