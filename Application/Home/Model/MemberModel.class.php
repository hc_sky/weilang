<?php
namespace Home\Model;
use Think\Model;

class MemberModel extends Model{

	//protected $trueTableName = "yx_member";


	/*
	 * 登录
	 * phone：手机号码
	 * role：账号角色，1用户，2分销商
	 * 登录用户角色时，使用短信验证码登录，无需密码，账户不存在时，自动注册账号
	 * 分销商登录验证登录密码
	 */
	public function login($phone,$pass,$role=1){
		$map['m.phone'] = $phone;
		$map['r.role'] = $role;
		$user = M("Member")->where($map)->join('as m left join yx_member_role as r on m.uid=r.uid')
				->field('m.uid, m.name, m.status, m.loginnum, m.pass, m.pay')
				->find();
		if( empty($user) || $user['pay']== -1 ){
			return array('data'=>0,'info'=>'账号不存在','status'=>0);
		}elseif($user['status']==0){
			return array('data'=>0,'info'=>'账号已过期','status'=>0);
		}elseif($user['pass'] != md5($pass) ){
			return array('data'=>0,'info'=>'密码错误','status'=>0);
		}elseif( in_array($user['status'],array(1,2)) ){
			M("Member")->where(array('phone'=>$phone))->save(array(
					'lasttime' => time(),
					'loginnum' => $user['loginnum'] + 1
			));
			self::init($user);
			return array('data'=>0,'info'=>'登录成功','status'=>1);
		}
	}



	/*
	 * 注册
	 */
	public function register($data){
		if($data['phone']){
			$uid = M("Member")->where(array('phone'=>$data['phone']))->getField('uid');
		}elseif($data['id']){
			$logs = M("RegisterLogs")->where(array('id'=>$data['id']))->field('phone,pass,parent_uid,from')->find();
			$uid = M("Member")->where(array('phone'=>$logs['phone']))->getField('uid');
			$data['phone'] = $logs['phone'];
			$data['pass'] = $logs['pass'];
			$data['parent_uid'] = $logs['parent_uid'];
			$data['from'] = intval($logs['from']);
		}
		if(empty($data['role'])){
			$data['role'] = 1;
		}
		//$data['outtime'] = time() + (3600 * 5);
		$data['outtime'] = time();
		if(empty($uid)){
			$data['regtime'] = time();
			$data['startime'] = time();
			if($data['pass']){
				$data['pass'] = md5($data['pass']);
			}
			$data['pay'] = -1;
			$data['isaudit'] = -1;
			$data['status'] = 1;
			$result = $uid = M("Member")->add($data);
		}
		$msg = self::updateMemberRole($uid,$data['role']);	//查询角色是否赋予权限

		if($result || $msg['status']==1){
			M("RegisterLogs")->where( array('phone'=>$data['phone']) )->save(array('status'=>1));
			//self::payLogs($uid,$_POST['choose'],$_POST['pay_way']);
			return array('data'=>$result,'info'=>'账户注册成功','status'=>1);
		}elseif($msg['status']<1){
			return $msg;
		}else{
			return array('data'=>0,'info'=>'账户注册失败','status'=>0);
		}
	}

	/*
	 * 注册记录
	 */
	public function registerLosg($field){
		$user = M("Member")->where( array('phone'=>$field['phone']) )->find();
		if( $user['isaudit'] > -1 ){
			return array('data'=>0,'info'=>'手机号已注册','status'=>0);
		}
		$logs = M("RegisterLogs")->where( array('phone'=>$field['phone'],'status'=>0) )->find();
		if( empty($logs) ){
			$result = M("RegisterLogs")->add(array(
				'phone' => $field['phone'],
				'pass' => $field['pass'],
				'parent_uid' => intval($field['parent_uid']),
				'status' => 0,
				'from' => $field['from'],
				'ctime' => time()
			));
		}elseif($field['pass'] != $logs['pass'] && $logs){
			M("RegisterLogs")->where( array('phone'=>$field['phone']) )->save( array('pass'=>$field['pass']) );
			$result = $logs['id'];
		}else{
			$result = $logs['id'];
		}
		if($result){
			return array('data'=>$result,'info'=>'记录成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'记录失败','status'=>0);
		}
	}

	/*
	 * 账号角色权限开通
	 */
	public function updateMemberRole($uid,$role=1){
		$log = M("MemberRole")->where( array('uid'=>$uid,'role'=>$role) )->find();
		if( empty($log) ){
			$result = M("MemberRole")->add(array(
					'uid' => $uid,
					'role' => $role,
					'ctime' => time()
				));
			if($result){
				return array('data'=>$uid,'info'=>'权限赋予成功','status'=>1);
			}
		}else{
			//修改bug，放弃支付（账户已注册），又回来选择体验套餐的情况
			$member = M("Member")->where( array('uid'=>$uid) )->field('pay,isaudit')->find();
			if( $member['pay']== -1 && $member['isaudit']== -1 ){
				M("Member")->where( array('uid'=>$uid) )->save(array(
						'pay' => -1,
						'isaudit' => -1,
						'regtime' => time(),
						'startime' => time(),
						'outtime' => time(),
				));
			}
			return array('data'=>$uid,'info'=>'更新账户信息','status'=>1);
			//return array('data'=>$uid,'info'=>'账户已存在','status'=>0);
		}
	}

	/*
	 * 修改密码
	 * phone：手机号码
	 * pass：新密码
	 */
	public function changePass($phone,$pass){
		if( empty($pass) ){
			return array('data'=>0,'info'=>'密码不能为空','status'=>0);
		}
		$result = M("Member")->where( array('phone'=>$phone) )->save( array('pass'=> md5($pass)) );
		if($result !== false){
			cookie("User", null);
			return array('data'=>0,'info'=>'修改密码成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'修改失败','status'=>0);
		}
	}

	/*
	 * 更新个人信息
	 */
	public function updateBaseInfo($uid,$data){
		unset($data['id']);
		unset($data['out_time']);
		$result = M("Member")->where( array('id'=>$uid) )->save( $data );
		if($result !== false){
			return array('data'=>0,'info'=>'保存成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'保存失败','status'=>0);
		}
	}

	/*
	 * 更新收款账号
	 * uid：用户ID
	 * weixin：微信账户
	 * alipay：支付宝账户
	 */
	public function updateAlipay($data){
		$pay = M("MemberAlipay")->where( array('id'=>$data['uid']) )->find();
		if($pay){
			$result = M("MemberAlipay")->where( array('id'=>$data['uid']) )
						->save( array('weixin'=>$data['weixin'],'alipay'=>$data['alipay']) );
		}else{
			$result = M("MemberAlipay")->add( array(
					'uid' => $data['uid'],
					'weixin' => $data['weixin'],
					'alipay' => $data['alipay'],
			));
		}
		if($result !== false){
			return array('data'=>0,'info'=>'保存成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'保存失败','status'=>0);
		}
	}

	/*
	 * 更换手机号
	 * phone：新手机号
	 */
	public function updatePhone($uid,$phone){
		$result = M("Member")->where( array('id'=>$uid) )->save( array('phone'=>$phone) );
		if($result !== false){
			return array('data'=>0,'info'=>'更换成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'更换失败','status'=>0);
		}
	}


	/*
	 * 购买套餐记录
	 * id：注册时，记录ID
	 * choose：购买套餐
	 * pay_way：支付方式
	 * uid：会员ID
	 * 当有注册记录时，优先从记录中调去uid，没有时直接使用uid_pay
	 */
	public function payLogs($id,$choose,$pay_way,$uid_pay=0){
		if( in_array($choose,array(1,2,3,4,5)) ){
			$price = M("SystemConfig")->where( array('name'=>'cfg_price'.$choose) )->getField('value');
		}else{
			return false;
		}

		if($id){
			$log = M("RegisterLogs")->join("as l left join yx_member as m on l.phone = m.phone")
					->where( array('l.id'=>$id) )->field('m.uid,m.phone,m.parent_uid')->find();
			$uid = $log['uid'];
			$phone = $log['phone'];
			$sale_id = intval($log['parent_uid']);
		}elseif($uid_pay){
			$uid = $uid_pay;
			$member = M("Member")->where( array('uid'=>$uid_pay) )->field('phone,parent_uid')->find();
			$sale_id = $member['parent_uid'];
			$phone = $member['phone'];
		}
		$manage_id = (int) M("Admin")->where( array('id'=>$sale_id) )->getField('parent_uid');
		$leader_id = (int) M("Admin")->where( array('id'=>$manage_id) )->getField('parent_uid');
		if( empty($uid) && $uid_pay){
			$uid = $uid_pay;
		}
		//echo M()->getLastSql();
		$result = M("PayLogs")->add(array(
						'uid' => intval($uid),
						'phone' => $phone,
						'choose' => $choose,
						'price' => $price,
						'pay_way' => intval($pay_way),
						'ctime' => time(),
						'sale_id' => $sale_id,
						'manage_id' => $manage_id,
						'leader_id' => $leader_id,
					));
		if($result){
			return array('data'=>$result,'info'=>'更换成功','status'=>1);
		}else{
			return array('data'=>0,'info'=>'更换失败','status'=>0);
		}
	}

	/*
	 * 更新套餐使用日期
	 */
	public function updateUserDate($uid,$choose,$open_id=''){
		if($choose==1){
			$data['outtime'] = strtotime('+3 month');
		}elseif($choose==2){
			$data['outtime'] = strtotime('+6 month');
		}elseif($choose==3){
			$data['outtime'] = strtotime('+1 year');
		}elseif($choose==4){
			$data['outtime'] = strtotime('+1 month');
		}elseif($choose==5){
			$data['outtime'] = strtotime('+1 day');
		}
		$info = M("Member")->where( array('uid'=>$uid) )->field('regtime,startime,outtime')->find();
		if( $info['outtime']>time() ){
			$data['outtime'] = $data['outtime'] + $info['outtime'] - time();
		}else{
			$data['startime'] = time();
		}
		$data['regtime'] = time();
		$data['pay'] = $choose;
		$data['isaudit'] = 1;
		$data['open_id'] = $open_id;
		if($data['outtime']){
			$result = M("Member")->where( array('uid'=>$uid) )->save($data);
		}
	}


	/*
	 * 提成
	 * uid：用户ID
	 * money：用户支付金额
	 * pid：支付记录ID
	 */
	public function money($field){
		$parent_uid = M("Member")->where( array('uid'=>$field['uid']) )->getField('parent_uid');
		$sale_ratio = M("SystemConfig")->where( array('name'=>'cfg_ratio_sale') )->getField('value');
		$manage_ratio = M("SystemConfig")->where( array('name'=>'cfg_ratio_manager') )->getField('value');
		$leader_ratio = M("SystemConfig")->where( array('name'=>'cfg_ratio_director') )->getField('value');
		if($parent_uid){
			$sale = M("Admin")->where( array('id'=>$parent_uid) )->field('id,parent_uid,role')->find();
			$manage = M("Admin")->where( array('id'=>$sale['parent_uid']) )->field('id,parent_uid,role')->find();
			if($sale['role']==4){
				$money = $field['money'] * $sale_ratio / 100;
				//总监提成
				$leader = M("Admin")->where( array('id'=>$manage['parent_uid']) )->field('id,role')->find();
				$money2 = $field['money'] * $leader_ratio / 100;
				self::moneyLog( array('uid'=>$leader['id'],'role'=>$leader['role'],'ratio'=>$leader_ratio,'money'=>$money2,'pid'=>$field['pid']) );
				//经理提成
				$money2 = $field['money'] * $manage_ratio / 100;
				self::moneyLog( array('uid'=>$manage['id'],'role'=>$manage['role'],'ratio'=>$manage_ratio,'money'=>$money2,'pid'=>$field['pid']) );
			}elseif($sale['role']==3){
				$money = $field['money'] * $manage_ratio / 100;
			}
			self::moneyLog( array('uid'=>$sale['id'],'role'=>$sale['role'],'ratio'=>$sale_ratio,'money'=>$money,'pid'=>$field['pid']) );
		}
	}

	/*
	 * 提成记录
	 * uid：销售员、业主ID
	 * role：角色类型
	 * money：提成金额
	 * pid：支付记录ID
	 */
	public function moneyLog($field){
		M("MoneyLogs")->add(array(
				'uid' => intval($field['uid']),
				'role' => intval($field['role']),
				'ratio' => $field['ratio'],
				'money' => $field['money'],
				'pid' => intval($field['pid']),
				'ctime' => time(),
				'status' => 1
		));
	}


	/*
	 * 提成统计
	 * uid：业务员/主管ID
	 * date：时间限制，0不限，1昨天，2今天
	 */
	public function computeMoney($uid,$date=null){
		$yesterday = date('Y-m-d',strtotime('-1 day'));
		$today = date('Y-m-d',time());
		$tomorrow = date('Y-m-d',strtotime('+1 day'));
		$yesterday = strtotime($yesterday);
		$today= strtotime($today);
		$tomorrow= strtotime($tomorrow);;
		$data1 = array($yesterday,$today);
		$data2 = array($today,$tomorrow);

		$map['uid'] = $uid;
		if($date==1){
			$map['ctime'] = array('between',$data1);
		}elseif($date==2){
			$map['ctime'] = array('between',$data2);
		}
		$money = (float) M("MoneyLogs")->where($map)->sum('money');
		return $money;
	}

	/*
	 * 业绩统计
	 * uid：业务员/主管ID
	 * date：限制时间，1昨天，2今天，0不限
	 */
	public function workMoney($uid, $date=null){
		$yesterday = date('Y-m-d',strtotime('-1 day'));
		$today = date('Y-m-d',time());
		$tomorrow = date('Y-m-d',strtotime('+1 day'));
		$yesterday = strtotime($yesterday);
		$today= strtotime($today);
		$tomorrow= strtotime($tomorrow);;
		$data1 = array($yesterday,$today);
		$data2 = array($today,$tomorrow);

		$role = M("Admin")->where( array('id'=>$uid) )->getField('role');
		if($role==5){
			$map['leader_id'] = $uid;
		}elseif($role==4){
			$map['sale_id'] = $uid;
		}elseif($role==3){
			$map['manage_id'] = $uid;
		}

		if($date==1){
			$map['ctime'] = array('between',$data1);
		}elseif($date==2){
			$map['ctime'] = array('between',$data2);
		}
		$map['status'] = 1;
		$money = (float) M("PayLogs")->where($map)->sum('price');
		return $money;
	}

	/*
	 * 用户数统计
	 * uid：角色ID
	 * role：用户类型，0体验用户，1季度用户，2半年用户，3一年用户，4合计用户，10天度客户
	 * date：时间限制，0不限，1昨天，2今天
	 *
	 */
	public function compute($uid,$role=null,$date=null){
		$yesterday = date('Y-m-d',strtotime('-1 day'));
		$today = date('Y-m-d',time());
		$tomorrow = date('Y-m-d',strtotime('+1 day'));
		$yesterday = strtotime($yesterday);
		$today= strtotime($today);
		$tomorrow= strtotime($tomorrow);;
		$data1 = array($yesterday,$today);
		$data2 = array($today,$tomorrow);

		if($role!=10){
			$map['pay'] = $role;
		}else{
			$map['pay'] = array("in",array(1,2,3,4,5));
		}
		if($date==1){
			$map['regtime'] = array('between',$data1);
		}elseif($date==2){
			$map['regtime'] = array('between',$data2);
		}
		if($uid){
			$role = M("Admin")->where( array('id'=>$uid) )->getField('role');
			if($role==5){
				$mids = M("Admin")->where( array('parent_uid'=>$uid) )->getField('id',true);
				if( empty($mids) ){ $mids[] = -1; }
				$ids = M("Admin")->where( array('parent_uid'=>array('in',$mids)) )->getField('id',true);
				if( empty($ids) ){ $ids[] = -1; }
				$map['parent_uid'] = array('in',$ids);
			}elseif($role==3){
				$ids = M("Admin")->where( array('parent_uid'=>$uid) )->getField('id',true);
				if( empty($ids) ){ $ids[] = -1; }
				$map['parent_uid'] = array('in',$ids);
			}else{
				$map['parent_uid'] = $uid;
			}

		}
		$num = (int) M("Member")->where($map)->count();
		//echo M()->getLastSql().'<br>';
		return $num;
	}


	/*
	 * 登录后保存cookie
	 */
	public function init($user){
		cookie("User", $user);
	}

	/*
	 * 退出登录
	 */
	public function exitLogin(){
		cookie("User", null);
	}

	/*
	* 用户来源
	* parent_uid：用户邀请人
	*/
	public function source($parent_uid){
		if($parent_uid){
			//$parent_uid = M("Member")->where( array('uid'=>$uid) )->getField('parent_uid');
			//$Admin = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid');
			$sale = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$parent_uid) )->field('d.realname,m.parent_uid')->find();
			$manage = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$sale['parent_uid']) )->field('d.realname,m.parent_uid')->find();
			$leader = M("Admin")->join('as m left join yx_admin_detail as d on m.id = d.uid')->where( array('m.id'=>$manage['parent_uid']) )->field('d.realname,m.parent_uid')->find();
			$parents[] = $sale['realname'].'<font class="gray">（业务员）</font>';
			$parents[] = $manage['realname'].'<font class="gray">（经理）</font>';
			$parents[] = $leader['realname'].'<font class="gray">（总监）</font>';
			return $parents;
		}else{
			return false;
		}

	}


}