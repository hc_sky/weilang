<?php
namespace Home\Model;
use Think\Model;
use Home\ORG\QRcode;

class QRcodeModel extends Model{

	protected $trueTableName = 'yx_admin';


	/*
	 * 生成二维码
	 */
	public function baseAccount($field){
		if($field['id']){
			$id = $field['id'];
			unset($field['id']);
			if(empty($field['pass'])){
				unset($field['pass']);
			}else{
				$field['pass_code'] = $field['pass'];
				$field['pass'] = md5($field['pass']);
			}
			unset($field['role']);
			M("Admin")->where( array('id'=>$id) )->save($field);
			$result = M("AdminDetail")->where( array('uid'=>$id) )->save($field);
		}else{
			$field['ctime'] = time();
			$field['pass'] = md5($field['pass']);
			$field['uid'] = $uid = M("Admin")->add($field);
			if($field['role']==4){
				$field['code'] = self::createCode();
			}
			$result = M("AdminDetail")->add($field);
		}
		if($result !== false){
			return array('data'=>$result,'info'=>'保存成功','status'=>1);
		}else{
			return array('data'=>$result,'info'=>'保存失败','status'=>0) ;
		}
	}

	public function createCode(){
		$x32 = "ABCDEFGHJKMNPQRSTVWXYZ";
		$code = null;
		$max = strlen($x32) - 1;
		for($i=0;$i<6;$i++){
			$code .= $x32[rand(0,$max)];		//rand($min,$max)生成介于min和max两个数之间的一个随机整数
		}
		//dump($code);
		return $code;
	}


	/*
	 * uid：业务员ID
	 * type：二维码类型，1邀请码，2产品推荐码
	 */
	function createQRCode($uid,$type=1){
		$weburl = M("SystemConfig")->where( array('name'=>'cfg_web') )->getField('value');
		$weburl = "http://m.weilangcn.com/";
		//$url = "http://". $weburl ."/home/public/register.html?uid=". $uid;
		if($type==2){
			$url = $weburl."/home/public/product.html?uid=". $uid;
		}elseif($type==1){
			$url = $weburl."/home/public/register.html?uid=". $uid;
		}else{
			return false;
		}

		$errorCorrectionLevel = 'H';
		$matrixPointSize = 12;
		$filename = './Upload/qrcode/' . $uid.'_'.$type.'.png';
		QRcode::png($url, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
		$logo = './Public/images/mobile/logo_qr.png';     //需要显示在二维码中的Logo图像
		$QR = $filename;
		if ($logo !== FALSE) {
			$QR = imagecreatefromstring ( file_get_contents ( $QR ) );
			$logo = imagecreatefromstring ( file_get_contents ( $logo ) );
			$QR_width = imagesx ( $QR );
			$QR_height = imagesy ( $QR );
			$logo_width = imagesx ( $logo );
			$logo_height = imagesy ( $logo );
			$logo_qr_width = $QR_width / 5;
			$scale = $logo_width / $logo_qr_width;
			$logo_qr_height = $logo_height / $scale;
			$from_width = ($QR_width - $logo_qr_width) / 2;
			imagecopyresampled ( $QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height );
		}
		$result = imagepng ( $QR, $filename );        //带Logo二维码的文件名
		//QRtools::timeBenchmark();     //显示耗时
		if($result){
			self::updateQrcode($uid,$filename,$type);
			return str_replace('./Upload/','/Upload/',$filename);
		}
	}

	/*
	 * 更新二维码
	 * uid：业务员ID
	 */
	public function updateQrcode($uid,$file,$type){
		$output_file = str_replace('./Upload/','/Upload/',$file);
		if($type==2){
			$data['code_product'] = $output_file;
		}elseif($type==1){
			$data['code_invite'] = $output_file;
		}
		$result = M("AdminDetail")->where( array('uid'=>$uid) )->save( $data );
	}

}