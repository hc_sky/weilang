<?php
namespace Home\Model;
use Think\Model;
use Home\ORG\REST;

class SmsModel extends Model{

	protected $tableName = 'sms';



	/**
	  * 发送模板短信
	  * @param to 手机号码集合,用英文逗号分开
	  * @param datas 内容数据 格式为数组 例如：array('Marry','Alon')，如不需替换请填 null
	  * @param $tempId 模板Id,测试应用和未上线应用使用测试模板请填写1，正式应用上线后填写已申请审核通过的模板ID
	  */       
	function sendTemplateSMS($to,$datas,$tempId)
	{
	  	// 初始化REST SDK
		global $accountSid,$accountToken,$appId,$serverIP,$serverPort,$softVersion;
		
		//主帐号,对应开官网发者主账号下的 ACCOUNT SID
		$accountSid= '8a216da85b22910c015b39456d8b0551';
		//主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
		$accountToken= '8a3cbc64982b4285827053423d9cf399';
		//应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
		//在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
		$appId='8a216da85b22910c015b3947b32f0557';
		//请求地址
		//沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com
		//生产环境（用户应用上线使用）：app.cloopen.com
		$serverIP='app.cloopen.com';
		//请求端口，生产环境和沙盒环境一致
		$serverPort='8883';
		//REST版本号，在官网文档REST介绍中获得。
		$softVersion='2013-12-26';


		$rest = new REST($serverIP,$serverPort,$softVersion);
		$rest->setAccount($accountSid,$accountToken);
		$rest->setAppId($appId);
	    
	    // 发送模板短信
	     //echo "Sending TemplateSMS to $to <br/>";
	    $result = $rest->sendTemplateSMS($to,$datas,$tempId);
		//dump($result);
	    if($result == NULL ) {
	         //echo "result error!";
	         return false;
	    }
	    if($result->statusCode!=0) {
	    	return false;
	         //echo "error code :" . $result->statusCode . "<br>";
	         //echo "error msg :" . $result->statusMsg . "<br>";
	    }else{
	         //echo "Sendind TemplateSMS success!<br/>";
	         // 获取返回信息
	         $smsmessage = $result->TemplateSMS;
			 return true;
	         //echo "dateCreated:".$smsmessage->dateCreated."<br/>";
	         //echo "smsMessageSid:".$smsmessage->smsMessageSid."<br/>";
	    }
	}
	
	
	/*
	 * 保存发送的短信信息
	 */
	public function saveSms($phone,$code){
		if (empty($phone) || empty($code)) return false;
		$data['phone'] = $phone;
		$data['code'] = $code;
		$data['ctime'] = time();
		$data['ip'] = get_ip();
		$result = D("Sms")->add($data);
		return $result;
	}

	/**
	 * 删除几个月前的短信记录,
	 * $num_ago：默认是3个月前的
	 */
	public function deleteSms($num_ago = 3){
		if(!is_int($num_ago)){
			return false;
		}
		$now = time();
		$now_ago = $num_ago*30*24*3600;
		$ago = $now - $now_ago;
		$map['ctime'] = array('lt',$ago);
		return $this->where($map)->delete();
	}
	
	
	/*
	 * 短信验证码
	 * phone：手机号码
	 * code：验证码
	 */
	function checkPhoneCode($phone,$code){
		if(empty($phone) || empty($code)){
			$msg["data"] = 0;
			$msg["info"] = "缺少参数";
			$msg["status"] = 0;
		}else{
			$sms = D("Sms")->where(array("phone"=>$phone))->order("id desc")->find();
			$area = time() - $sms["ctime"];
			if($area>300){
				$msg["data"] = 0;
				$msg["info"] = "验证码已失效";
				$msg["status"] = -1;
			}elseif($sms["code"] == $code){
				$msg["data"] = 0;
				$msg["info"] = "通过短信验证";
				$msg["status"] = 1;
			}else{
				$msg["data"] = 0;
				$msg["info"] = "验证码错误";
				$msg["status"] = 0;
			}
		}
		return $msg;
	}

	public function checkAccess($phone= '')
	{return true;
		$ip = get_ip();
		$date = date('Y-m-d');
		$model = M('SmsLogs');
		$return = true;

		$ipLog = $model->where(array('ip_or_phone' => $ip, 'date' => $date, 'type' => 1))->find();
		$phoneLog = $model->where(array('ip_or_phone' => $phone, 'date' => $date, 'type' => 2))->find();
		if ($ipLog) {
			$model->where(array('ip_or_phone' => $ip, 'date' => $date, 'type' => 1))->setInc('hits');
		} else {
			$model->add(array('ip_or_phone' => $ip, 'date' => $date, 'type' => 1));
		}
		if ($phoneLog) {
			$model->where(array('ip_or_phone' => $phone, 'date' => $date, 'type' => 2))->setInc('hits');
		} else {
			$model->add(array('ip_or_phone' => $phone, 'date' => $date, 'type' => 2));
		}

		$ipResult = $this->whiteBlackList($ip);
		if ($ipResult === 0) {
			$return = '您所在的IP由于在短时间发送短信过多而被封禁, 请联系客服解封';
		} elseif ($ipResult === 1) {

		} else {
			if ($ipLog['hits'] >= 19) {
				$this->addToBlackList($ip);
				$return = '您所在的IP由于在短时间发送短信过多而被封禁, 请联系客服解封!';
			}
		}

		$ipResult = $this->whiteBlackList($phone);
		if ($ipResult === 10) {
			$return = '该手机号码由于在短时间发送短信过多而被封禁, 请联系客服解封';
		} elseif ($ipResult === 11) {

		} else {
			if ($phoneLog['hits'] >= 10) {
				$this->addToBlackList($phone, 10);
				$return = '该手机号码由于在短时间发送短信过多而被封禁, 请联系客服解封!';
			}
		}
		return $return;
	}

	private function whiteBlackList($ip)
	{
		$ip = M('IpList')->where(array('ip' => $ip))->find();
		if (! empty($ip) ) {
			return (int) $ip['type'];
		} else {
			return false;
		}
	}

	private function addToBlackList($ip, $type = 0) {
		M('IpList')->add(array('ip' => $ip, 'add_time' => time(), 'type' => $type));
	}



}