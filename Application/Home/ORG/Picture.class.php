<?php
/*
 *  Copyright (c) 2014 The CCP project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a Beijing Speedtong Information Technology Co.,Ltd license
 *  that can be found in the LICENSE file in the root of the web site.
 *
 *   http://www.yuntongxun.com
 *
 *  An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

namespace Home\ORG;
use Home\ORG\String;

class Picture
{


    function __construct($ServerIP, $ServerPort, $SoftVersion)
    {

    }


    /*
     * a.合成图片信息 复制一张图片的矩形区域到另外一张图片的矩形区域
     * image：图片
     * color：字体颜色
     * size：字体大小
     * circle：旋转角度
     * text：文字内容
     * uid：用户ID
     */
    public function addText($field)
    {
        //$bigImgPath = '1.jpg';
        //$field['image'] = 'http://www.huifang.cn/Public/images/version1/banner.jpg';
        $field['image'] = '.' . $field['image'];


        $img = imagecreatefromstring(file_get_contents($field['image']));

        $font = 'font/msyh.ttc';    //微软雅黑
        //字体颜色 RGB
        switch ($field['color']) {
            case 'ff0000':
                $black = imagecolorallocate($img, 255, 0, 0);
                break;
            case '0068b7':
                $black = imagecolorallocate($img, 0, 104, 183);
                break;
            case 'fff100':
                $black = imagecolorallocate($img, 255, 255, 0);
                break;
            case 'f39800':
                $black = imagecolorallocate($img, 255, 152, 0);
                break;
            case '009944':
                $black = imagecolorallocate($img, 0, 152, 68);
                break;
            case '333333':
                $black = imagecolorallocate($img, 50, 50, 50);
                break;
            case 'aaaaaa':
                $black = imagecolorallocate($img, 170, 170, 170);
                break;
            case 'ffffff':
                $black = imagecolorallocate($img, 255, 255, 255);
                break;
            default:
                $black = imagecolorallocate($img, 255, 255, 255);
                break;
        }
        list($bgWidth, $bgHight, $bgType) = getimagesize($field['image']);

        $field['size'] = ($field['size'] > 14) ? $field['size'] : 14;    //文字大小
        $field['circle'] = isset($field['circle']) ? $field['circle'] : 0;     //旋转角度

        $text_info = ImageTTFBBox($field['size'], $field['circle'], $font, $field['text']);
        $width_text = abs($text_info[4] - $text_info[6]);
        $height_text = abs($text_info[7] - $text_info[1]);
        $left = abs(($bgWidth - $width_text) / 2);
        $top = abs($bgHight / 2) + ($height_text / 2);
        if ($width_text > $bgWidth) {
            $line = ceil($width_text / $bgWidth);
            $num_line = floor($bgWidth * String::mstrlen($field['text']) / $width_text);
            for ($i = 0; $i < $line; $i++) {
                $star = $i * $num_line;
                $end = ($i + 1) * $num_line;
                $arr[] = String::msubstr($field['text'], $star, $end);
            }
            $left = 0;
            $top = abs($bgHight / 2);

            foreach ($arr as $vo) {
                imagefttext($img, $field['size'], $field['circle'], $left, $top, $black, $font, $vo);
                $top = $top + $height_text;
            }
        } else {
            imagefttext($img, $field['size'], $field['circle'], $left, $top, $black, $font, $field['text']);
        }


        $file = "Upload/text_to_ad/{$field['uid']}_" . time();
        switch ($bgType) {
            case 1: //gif
                header('Content-Type:image/gif');
                //imagegif($img);
                $result = imagegif($img, $file . ".gif");
                $suffix = ".gif";
                break;
            case 2: //jpg
                header('Content-Type:image/jpg');
                //imagejpeg($img);
                $result = imagejpeg($img, $file . ".jpg");
                $suffix = ".jpg";
                break;
            case 3: //jpg
                header('Content-Type:image/png');
                //imagepng($img);
                $result = imagejpeg($img, $file . ".png");
                $suffix = ".png";
                break;
            default:
                break;
        }
        // dump($img);
        imagedestroy($img);
        if ($result) {
            return array('data' => "/{$file}{$suffix}", 'info' => '合成图片成功', 'status' => 1);
        } else {
            return array('data' => '', 'info' => '合成图片成功', 'status' => 1);
        }
    }


    /*
     * 裁剪图片为固定大小
     * source_path：原图
     * target_width：宽度
     * target_height：高度
     */
    function imagecropper($source_path, $target_width, $target_height){
        $source_info = getimagesize($source_path);
        $source_width = $source_info[0];
        $source_height = $source_info[1];
        $source_mime = $source_info['mime'];
        $source_ratio = $source_height / $source_width;
        $target_ratio = $target_height / $target_width;

        // 源图过高
        if ($source_ratio > $target_ratio){
            $cropped_width = $source_width;
            $cropped_height = $source_width * $target_ratio;
            $source_x = 0;
            $source_y = ($source_height - $cropped_height) / 2;
        }elseif ($source_ratio < $target_ratio){ // 源图过宽
            $cropped_width = $source_height / $target_ratio;
            $cropped_height = $source_height;
            $source_x = ($source_width - $cropped_width) / 2;
            $source_y = 0;
        }else{ // 源图适中
            $cropped_width = $source_width;
            $cropped_height = $source_height;
            $source_x = 0;
            $source_y = 0;
        }

        switch ($source_mime){
            case 'image/gif':
                $source_image = imagecreatefromgif($source_path);
                break;
            case 'image/jpeg':
                $source_image = imagecreatefromjpeg($source_path);
                break;
            case 'image/png':
                $source_image = imagecreatefrompng($source_path);
                break;
            default:
                return false;
                break;
        }

        $target_image = imagecreatetruecolor($target_width, $target_height);
        $cropped_image = imagecreatetruecolor($cropped_width, $cropped_height);

        // 裁剪
        imagecopy($cropped_image, $source_image, 0, 0, $source_x, $source_y, $cropped_width, $cropped_height);
        // 缩放
        imagecopyresampled($target_image, $cropped_image, 0, 0, 0, 0, $target_width, $target_height, $cropped_width, $cropped_height);
        $dotpos = strrpos($source_path, '.');
        $imgName = substr($source_path, 0, $dotpos);
        $suffix = substr($source_path, $dotpos);
        $imgNew = $imgName . '_small' . $suffix;
        //$imgNew = $imgName . $suffix;
        imagejpeg($target_image, $imgNew, 100);
        imagedestroy($source_image);
        imagedestroy($target_image);
        imagedestroy($cropped_image);
        return str_replace('./Upload/','/Upload/',$imgNew);
    }



}
?>
