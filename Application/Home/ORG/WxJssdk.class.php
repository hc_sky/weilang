<?php

namespace Home\ORG;

class WxJssdk
{
  private $appId;
  private $appSecret;


//  public function __construct($appId, $appSecret) {
//    $this->appId = $appId;
//    $this->appSecret = $appSecret;
//  }

  public function __construct() {
    $this->appId = 'wxf3142c017f288e56';
    $this->appSecret = 'a37f9b33ee629879edffb86690f8fd9d';
  }

  public function getSignPackage() {
    $jsapiTicket = $this->getJsApiTicket();
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $timestamp = time();
    $nonceStr = $this->createNonceStr();

    // 这里参数的顺序要按照 key 值 ASCII 码升序排序
    $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

    $signature = sha1($string);

    $signPackage = array(
      "appId"     => $this->appId,
      "nonceStr"  => $nonceStr,
      "timestamp" => $timestamp,
      "url"       => $url,
      "signature" => $signature,
      "rawString" => $string
    );
    return $signPackage; 
  }

  /*
   * 获取用户头像
   */
  public function getWechatFace($openId){
    if( empty($openId) ){
      return false;
      //$openId = 'otP-OvwI406n91lDcrmZjhKrU7i8';
    }
    $accessToken = $this->getAccessToken();
    //dump($accessToken);
    $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$accessToken&openid={$openId}&lang=zh_CN";
    $info = json_decode($this->httpGet($url),true);
    return $info;
  }

  /*
   * 尝试消息通知
   */
  public function sendSMS($field){
    $accessToken = $this->getAccessToken();
//    $data = '
//        {
//            "touser": "otP-OvwI406n91lDcrmZjhKrU7i8",
//            "msgtype": "text",
//            "text": {
//                "content": "地方斯蒂芬"
//            }
//        }
//    ';
    $tousers = array('otP-Ov6X8YnjAInP3SRsNSb6uCy8','otP-OvwI406n91lDcrmZjhKrU7i8');
    // 账号一接收消息通知
    foreach($tousers as $vo){
      $data = '{
                "touser": "'.$vo.'",
                "template_id": "0S000bdRSx83J2JijTkJbDRh3f_qXSrkKCtkJ05HQSI",
                "data": {
                    "first" :   {
                        "value": "交易单号：'.$field['transaction_id'].'",
                        "color": "#999999"
                    },
                    "orderMoneySum" :   {
                        "value": "'.$field['money'].'元",
                        "color": "#000000"
                    },
                    "orderProductName" :   {
                        "value": "购买'.$field['pay'].'",
                        "color": "#000000"
                    },
                    "Remark" :   {
                        "value": "备注：用户已完成支付，手机号码:'.$field['phone'].'",
                        "color": "#888888"
                    }
                }
        }
      ';
      $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$accessToken}";
      $info = json_decode($this->http_request($url,$data),true);
    }
    return $info;
  }

  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }

  private function getJsApiTicket() {
    // jsapi_ticket 应该全局存储与更新
    $keyName = 'jsapi_ticket';
    $ticket = false;

    if ($ticket===false) {
      $accessToken = $this->getAccessToken();
      $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
      $res = json_decode($this->httpGet($url));
      $ticket = $res->ticket;
    }
    return $ticket;
  }

  private function getAccessToken() {
    // access_token 应该全局存储与更新
    $keyName = 'jsapi_accessToken';
    $access_token = false;
    
    if ($access_token===false) {
      $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
      $res = json_decode($this->httpGet($url));
      $access_token = $res->access_token;
    }

    return $access_token;
  }

  private function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }

  /*
   * post方法
   * url：需请求的地址
   * data：参数
   */
  private function http_request($url, $data = null)
  {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if (!empty($data)){
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
  }

}

