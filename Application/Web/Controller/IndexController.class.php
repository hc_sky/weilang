<?php
namespace Web\Controller;
use Think\BaseController;
use Home\ORG\ISmobile;

class IndexController extends BaseController {

    public function _initialize(){
        parent::_initialize();
        $browsers = new ISmobile();
        $browsers->is_mobile;
        $browsers->is_browser;
        if( $browsers->is_mobile() && empty($_GET['token']) ){
            redirect('http://m.weilangcn.com/home');
        }
        //dump($browsers);
        //dump($browsers->is_mobile());
        //dump($browsers->is_browser());
    }


    public function index(){
        //域名指向跳转
        if($_SERVER['HTTP_HOST'] == 'm.weilangcn.com'){
            redirect('http://m.weilangcn.com/home');
        }
        self::smsStatus();
        $map['name'] = array('in',array('cfg_web','cfg_company','cfg_free'));
        $config = M("SystemConfig")->where($map)->select();
        foreach($config as $vo){
            $web[$vo['name']] = $vo['value'];
        }
        $agent_num = (int) M("AgentsNum")->where( array('id'=>1) )->getField('value');

        $this->assign('web',$web);
        $this->assign('agent_num',$agent_num + 30000);
        $this->display();
    }


    /*
     * 修改短信阅读状态
     */
    public function smsStatus(){
        if($_GET['token']){
            M("SmsUserLogs")->where( array('code'=>$_GET['token']) )->setInc('click');
            $this->redirect("/Home/Public/product",array('uid'=>23));
        }
    }









}