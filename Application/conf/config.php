<?php
return array(

    //'MODULE_ALLOW_LIST' => array('Home','Seller','Admin'),
    //'DEFAULT_MODULE' => 'Web',  // 默认模块

    'SESSION_AUTO_START' => true,
    'SESSION_NAME'=>'ThinkID',                // 默认Session_name
    'SESSION_PATH'=>'',                        // 采用默认的Session save path
    'SESSION_TYPE'=>'File',                        // 默认Session类型 支持 DB 和 File
    'SESSION_EXPIRE'=>'300000',                // 默认Session有效期
    'SESSION_TABLE'=>'think_session',        // 数据库Session方式表名
    'SESSION_CALLBACK'=>'',                        // 反序列化对象的回调方法

);