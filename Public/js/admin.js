$(function(){
	/*
	 * 限制手机号码只能输入11位的数字
	 */
	$(document).on("input keyup","input[name='phone']",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
		this.maxLength = 11;
	});

	/*
	 * 限制属性data-rel="number"的输入框只能输入数字
	 */
	$("input[data-rel='number']").on("input keyup",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
	});

	$("input[name='all']").on("click",function(){
		var check = $(this).prop("checked");
		if(check){
			$("input[type='checkbox']").prop("checked", "checked");
		}else{
			$("input[type='checkbox']").prop("checked", false);
		}
	});

	// 跳转到指定页
	$(".box_go_page button").on("click",function(){
		var page = $("input[name='pages']").val();
		var url = window.location.href;
		if( page<1 ){
			return false;
		}
		if( url.indexOf('/p/') > 0 ){
			url = url.replace(/p\/(\d+)/g, "p/"+ page);
		}else if( url.indexOf('/p/') == -1 ){

			if( url.indexOf('.html') > 0 ){
				url = url.replace(/.html/g, "/p/"+ page +'.html');
			}else{
				url = url + '/p/'+ page + '.html';
			}
		}
		//console.log( url.indexOf('/p/') );
		//console.log(page);
		//console.log(url);
		if( page>1 ){
			window.location.href = url;
		}
	});

	// select选择自动跳转
	$(".auto_select").on("change",function(){
		var url = window.location.href;
		var name = $(this).attr("name");
		if( url.indexOf('/'+ name +'/') > 0 ){
			if(this.name=='status'){
				var reg = new RegExp(/status\/(\d+)/g);
			}else if(this.name=='order'){
				var reg = new RegExp(/order\/(\d+)/g);
			}else if(this.name=='money'){
				var reg = new RegExp(/money\/(\d+)/g);
			}else if(this.name=='price'){
				var reg = new RegExp(/price\/(\d+)/g);
			}else if(this.name=='type'){
				var reg = new RegExp(/type\/(\d+)/g);
			}else if(this.name=='date'){
				var reg = new RegExp(/date\/(\d+)/g);
			}else if(this.name=='role'){
				var reg = new RegExp(/role\/(\d+)/g);
			}else if(this.name=='province2'){
				var reg = new RegExp(/province2\/(\d+)/g);
			}else if(this.name=='city2'){
				var reg = new RegExp(/c\/(\d+)/g);
			}else if(this.name=='area2'){
				var reg = new RegExp(/area2\/(\d+)/g);
			}else if(this.name=='status_read'){
				var reg = new RegExp(/status_read\/(\d+)/g);
			}else if(this.name=='status_register'){
				var reg = new RegExp(/status_register\/(\d+)/g);
			}
			url = url.replace(reg, name +"/"+ this.value);
		}else if( url.indexOf('/'+ name +'/') == -1 ){
			if( url.indexOf('.html') > 0 ){
				url = url.replace(/.html/g, "/"+ name +"/"+ this.value +'.html');
			}else{
				url = url + '/'+ name +'/'+ this.value + '.html';
			}
		}
		//console.log( url.indexOf('/'+ name +'/') +"/"+ this.value +"/"+ this.name);
		//console.log(url);
		window.location.href = url;
	});

	$(".title_choose label").on("click",function(){
		var pos = $(this).index();
		$(this).addClass("current").siblings("label").removeClass("current");
		$(".box_choose_big").css("display","none").eq(pos).css("display","block");
	});



	$(document).on("click",".box_choose label",function(){
		var pos = $(this).index();
		$(this).addClass("current").siblings("label").removeClass("current");
		$(this).siblings("input[type='hidden']").val(pos);
	});

	$("select[name='province'], select[name='city']").on("input keyup",function(){
		//console.log(this.name);return false;
		var id,level,name,obj;
		name = $(this).attr('name');
		if(name=='province'){
			level = 2;
			id = $("select[name='province'] option:selected").attr("data-id");
			obj = $("select[name='city']");
			$("select[name='region']").empty().append('<option value="">请选择</option>');
		}else if(name=='city'){
			level = 3;
			id = $("select[name='city'] option:selected").attr("data-id");
			obj = $("select[name='region']");
		}
		console.log(id+'/'+level+'/'+name);
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/choose_city",
			data: "id="+ id + "&level=" + level,
			dataType: "json",
			success: function(msg){
				if(msg.status==1){
					var data = msg.data;
					obj.empty().append('<option value="">请选择</option>');
					for(var i = 0; i<data.length; i++){
						obj.append("<option value='"+ data[i]['name'] +"' data-id='"+ data[i]['id'] +"'>"+ data[i]['name'] +"</option>");
					}
				}else{

				}
			}
		});
	});

	//获取短信验证码
	$("#code").on("click",function(){
		var phone,obj;
		var telReg = /^(1[3-9][0-9])\d{8}$/;
		var token = $('input[name="token"]').val();
		phone = $('input[name="phone"]').val();
		if(phone==""){
			dialog('请输入手机号码');
		}else if(!telReg.test(phone) ){
			dialog('手机号码不正确');
		}else{
			$.ajax({
				type: "POST",
				url:  APP + "/home/public/interface_code",
				data: "phone="+ phone + "&token=" + token,
				dataType: "json",
				success: function(msg){
					if(msg.status==1){
						var countdown = 60;
						$("#code").attr("disabled","disabled");
						setTimeout(function(){ setTime(countdown); },1000);
					}
				}
			});
		}
	})

});


//60秒倒计时
function setTime(val){
	if(val == 1) {
		$("#code").removeAttr("disabled").text("发送验证码");
	}else{
		$("#code").text(val + "秒后重发");
	}
	val--;
	if(val>0){
		setTimeout(function(){ setTime(val); },1000)
	}
}

/*
 * jquery.form.js公共提交入口
 * beforeSubmit：提交检查函数可选参数
 * success：提交结果函数
 */
$.fn.publicAjaxForm=function(beforeSubmit,success){
	if(arguments.length==1){
		$(this).ajaxForm({
			success:  success,
			dataType: 'json'
		});
	}else{
		$(this).ajaxForm({
			beforeSubmit:  beforeSubmit,
			success:  success,
			dataType: 'json'
		});
	}
};

function complete(data){
	dialog(data.info);
	if(data.status==1 && data.url){
		setTimeout(function(){ window.location.href = data.url; },1000);
	}else if(data.status==1){
		setTimeout(function(){ window.location.reload(); },1000);
	}
}

// 弹出窗口样式
function dialog(msg){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
			msg
		},
		autoClose : 2000,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
};

/*
 * 用户登陆
 */
function login(){
	var username = $("input[name='username']").val();
	var pass = $("input[name='pass']").val();
	if(username==''){
		dialog('请输入用户名');
	}else if(pass==''){
		dialog('请输入密码');
	}else{
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/login",
			data: "username="+ username + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}if(msg.status==2){
					window.location.href = MODULE +"/public/lock.html";
				}else{
					dialog(msg.info);
				}
				return false;
			}
		});
	}
	return false;
}

/*
 * 忘记密码
 */
function forget(){
	var phone = $("input[name='phone']").val();
	var pass = $("input[name='pass']").val();
	var code = $("input[name='code']").val();
	if(phone==''){
		dialog('请输入手机号');
	}else if(code==''){
		dialog('请输入验证码');
	}else if(pass==''){
		dialog('请输入密码');
	}else{
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/forget",
			data: "phone="+ phone + "&pass=" + pass + "&code=" + code,
			dataType: "json",
			success: function(msg){
				dialog(msg.info);
				if(msg.status==1){
					setTimeout(function(){ window.location.href = URL +"/login"; },1000);
				}
			}
		});
	}
	return false;
}

/*
 * 退出登录
 */
function layout(){
	$.ajax({
		type: "POST",
		url:  MODULE + "/public/layout",
		dataType: "json",
		success: function(msg){
			if(msg.status==1){
				console.log(msg.url);
				setTimeout(function(){ window.location.href = msg.url; },1000);
				//setTimeout(function(){ window.location.reload(); },1000);
			}
		}
	});
}

/*
 * 全选
 */
function chooseAll(){
	$("input[name='all']").trigger("click");
}






/*
 * 广告图操作
 * operate：0删除
 * ids：广告图片ID
 */
function operateAds(operate,id){
	var model = $("input[name='model']").val();
	if(arguments.length==1 && id == undefined){
		var ids = new Array;
		$("input[name='ids']:checked").each(function () {
			ids.push($(this).val());
		})
	}else{
		var ids = id;
	}
	if(ids.length==0){
		dialog('请选择操作项');
	}else{
		$.ajax({
			type: "POST",
			url: URL + "/operateAds",
			data: "ids=" + ids +"&type="+ model +"&operate=" + operate,
			dataType: "json",
			success: function (msg) {
				dialog(msg.info);
				if (msg.status == 1) {
					setTimeout(function () { window.location.reload(); }, 1000);
				}
			}
		});
		console.log(ids);
	}
}


/*
 * 删除广告
 */
function deleteAd(id){
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '确认删除广告？',
			yesFn: function (e) {
				operateAds(0,id);
			},
			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: false
	});
}


/*
 * 删除模板
 */
function deleteTemplate(id){
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '确认删除模板？',
			yesFn: function (e) {
				operateAds(0,id);
			},
			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: false
	});
}

/*
 * 删除商品类别
 */
//function deleteGoodsType(id){
//	easyDialog.open({
//		container: {
//			header: "系统提示",
//			content: '确认删除该类别？',
//			yesFn: function (e) {
//				$.ajax({
//					type: "POST",
//					url:  URL + "/deleteGoodsType",
//					data: "id="+ id,
//					dataType: "json",
//					success: function(msg){
//						if(msg.status==1){
//							setTimeout(function () { window.location.reload(); }, 1000);
//						}else{
//							dialog(msg.info);
//						}
//					}
//				});
//			},
//			noFn: true
//		},
//		autoClose: false,
//		fixed: false,
//		overlay: true,
//		drag: false,
//		lock: false
//	});
//}


/*
 * 文章操作
 * ids：广告图片ID
 * operate：0删除，3已阅
 */
function operateArticle(operate,id){
	if(arguments.length==1 && id == undefined){
		var ids = new Array;
		$("input[name='ids']:checked").each(function () {
			ids.push($(this).val());
		})
	}else{
		var ids = id;
	}
	if(ids.length==0){
		dialog('请选择操作项');
	}else{
		$.ajax({
			type: "POST",
			url: APP + "/admin/check/operateArtice",
			data: "ids=" + ids +"&operate=" + operate,
			dataType: "json",
			success: function (msg) {
				dialog(msg.info);
				if (msg.status == 1) {
					setTimeout(function () { window.location.reload(); }, 1000);
				}
			}
		});
		console.log(ids);
	}
}

/*
 * 删除用户
 */
function operateUser(operate,id){
	if(arguments.length==2 && id){
		var ids = id;
	}else{
		var ids = new Array;
		$("input[name='ids']:checked").each(function () {
			ids.push($(this).val());
		});
	}
	console.log(ids);
	if(ids.length==0){
		dialog('请选择操作项');
	}else{
		easyDialog.open({
			container: {
				header: "系统提示",
				content: '确认执行操作？',
				yesFn: function (e) {
					$.ajax({
						type: "POST",
						url: URL + "/operateUser",
						data: "ids=" + ids + "&operate=" + operate,
						dataType: "json",
						success: function (msg) {
							dialog(msg.info);
							if (msg.status == 1) {
								setTimeout(function () { window.location.reload(); }, 1000);
							}
						}
					});
				},
				noFn: true
			},
			autoClose: false,
			fixed: false,
			overlay: true,
			drag: false,
			lock: false
		});
	}
}

/*
 * 删除用户
 */
function operateOutUser(operate,id){
	if(arguments.length==2 && id){
		var ids = id;
	}else{
		var ids = new Array;
		$("input[name='ids']:checked").each(function () {
			ids.push($(this).val());
		});
	}
	console.log(ids);
	if(ids.length==0){
		dialog('请选择操作项');
	}else{
		easyDialog.open({
			container: {
				header: "系统提示",
				content: '确认执行操作？',
				yesFn: function (e) {
					$.ajax({
						type: "POST",
						url: URL + "/deleteOutUser",
						data: "ids=" + ids + "&operate=" + operate,
						dataType: "json",
						success: function (msg) {
							dialog(msg.info);
							if (msg.status == 1) {
								setTimeout(function () { window.location.reload(); }, 1000);
							}
						}
					});
				},
				noFn: true
			},
			autoClose: false,
			fixed: false,
			overlay: true,
			drag: false,
			lock: false
		});
	}
}


/*
 * 确认收货
 */
//function sureToTake(id){
//	easyDialog.open({
//		container: {
//			header: "系统提示",
//			content: '确认收到商品？',
//			yesFn: function (e) {
//				$.ajax({
//					type: "POST",
//					url: URL + "/sureToTake",
//					data: "id=" + id,
//					dataType: "json",
//					success: function (msg) {
//						dialog(msg.info);
//						if (msg.status == 1) {
//							setTimeout(function () {
//								window.location.reload();
//							}, 1000);
//						}
//					}
//				})
//			},
//			noFn: true
//		},
//		autoClose: false,
//		fixed: false,
//		overlay: true,
//		drag: false,
//		lock: false
//	});
//}

/*
 * 立即发货
 */
//function sendOutGoods(id){
//	easyDialog.open({
//		container: {
//			header: "系统提示",
//			content: '<style>.easyDialog_wrapper{ width:350px; }  </style>' +
//			'<iframe width="100%" height="190"  frameborder="0" src="' + URL + '/sendout.html?id=' + id +'" />'
//		},
//		autoClose: false,
//		fixed: false,
//		overlay: true,
//		drag: false,
//		lock: false
//	});
//}

/*
 * 订单商品
 */
//function goodsInfo(id){
//	easyDialog.open({
//		container: {
//			header: "订单商品",
//			content: '<style>.easyDialog_wrapper{ width:450px; }  .easyDialog_text{padding: 10px 15px !important; }</style>' +
//			'<iframe width="100%" height="300"  frameborder="0" src="' + URL + '/goods_info.html?id=' + id +'" />'
//		},
//		autoClose: false,
//		fixed: false,
//		overlay: true,
//		drag: false,
//		lock: false
//	});
//}


/*
 * 分销商注册信息
 */
//function showSellerInfo(id){
//	easyDialog.open({
//		container: {
//			header: "注册登录信息",
//			content: '<style>.easyDialog_text{padding: 10px 15px !important; }</style>' +
//			'<iframe width="100%" height="200"  frameborder="0" src="' + URL + '/seller_info.html?id=' + id +'" />'
//		},
//		autoClose: false,
//		fixed: false,
//		overlay: true,
//		drag: false,
//		lock: false
//	});
//}


/*
 * 确认结佣
 * id：商品ID
 */
//function sureToPay(){
//	$(".box_fixed").css("display","block");
//	$("#pay").load("pay").before('<div class="box_opacity"></div>');
//}

/*
 * 搜索主管
 */
function openSearch(role){
	easyDialog.open({
		container: {
			header: "选择所属上级",
			content: '<style>.easyDialog_wrapper{width:450px !important;} .easyDialog_text{padding: 10px 15px !important; }</style>' +
			'<iframe width="100%" height="280"  frameborder="0" src="' + URL + '/search.html?role=' + role +'" />'
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: false
	});
	return false;
}


/*
 * 查看销售、主管账号信息
 */
function viewSale(id){
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '<style>.easyDialog_wrapper{width:500px !important;} .easyDialog_text{padding: 10px 15px !important; }</style>' +
			'<iframe width="100%" height="220"  frameborder="0" src="' + URL + '/viewSale.html?id=' + id +'" />'
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: false
	});
	return false;
}


/*
 * 结算提成
 */
function sureToPay(uid){
	easyDialog.open({
		container: {
			header: "确认结佣",
			content: '<p style="padding-bottom:15px;">结算金额：<input type="text" name="money" value="" /></p>' +
					 '<div class="box_choose">' +
					 '<span>结佣方式：</span>' +
					 '<input type="hidden" name="pay_way" value="2" />' +
					 '<label class="current">微信</label>' +
				 	 '<label>支付宝</label>' +
					 '</div>' +
					 '<p style="padding:15px 0 7px 0;">上传凭证：<input type="file" id="file1" name="file" value="" style="width:165px;" /></p>' +
					 '<p class="prompt"></p>',
			yesFn: function (e) {
				var money = $("input[name='money']").val();
				var pay_way = $("input[name='pay_way']").val();
				var file = document.getElementById("file1").files[0];

				if(money < 1){
					$(".prompt").fadeIn(300).text('请输入结算金额');
					setTimeout(function(){ $(".prompt").fadeOut(300) },4000);
					return false;
				}else if(file==undefined){
					$(".prompt").fadeIn(300).text('请上传凭证');
					setTimeout(function(){ $(".prompt").fadeOut(300) },4000);
					return false;
				}

				var formData = new FormData();
				formData.append("uid",uid);
				formData.append("money",money);
				formData.append("pay_way",pay_way);
				formData.append("file",file);
				$.ajax({
					type: "POST",
					url: URL + "/tocase",
					//data: "uid=" + uid +"&money="+ money +"&pay_way="+ pay_way +"&file="+ file,
					data: formData,
					processData : false,
					contentType : false,
					cache: false,
					dataType: "json",
					success: function (msg) {
						dialog(msg.info);
						if (msg.status == 1) {
							setTimeout(function () {
								window.location.reload();
							}, 1000);
						}
					}
				})
			},
			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
}

/*
 * 查看凭证
 */
function viewLincense(file){
	easyDialog.open({
		container : {
			header : '查看结算凭证',
			content : '<style>.easyDialog_wrapper{width:800px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
					  '<img src="'+ file +'"  height="400" style="max-width:95%;" />'
		},
		autoClose : false,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
}


/*
 * 添加管理员检查表单
 */
function checkAdminFrom(){
	var id,username,realname,password,role,parent_uid;
	id = $("input[name='id']").val();
	username = $("input[name='username']").val();
	realname = $("input[name='realname']").val();
	password = $("input[name='pass']").val();
	role = $("input[name='role']").val();
	parent_uid = $("input[name='parent_uid']").val();
	console.log(parent_uid);
	console.log(role);
	console.log( $.inArray(role,[3,4]));
	if(realname==""){
		$("input[name='realname']").after('<label class="msg">请输入使用者</label>');
	}else if(username==""){
		$("input[name='username']").after('<label class="msg">账号不能为空</label>');
	}else if(password=="" && id<1){
		$("input[name='pass']").after('<label class="msg">请输入密码</label>');
	}else if(parent_uid<1 && $.inArray(role,[3,4]) != -1 && 0){
		$("a.bnt_green").after('<label class="msg">请选择所属上级</label>');
	}else{
		return true;
	}
	setTimeout(function(){ $(".msg").fadeOut(300) },4000);
	return false;
}

/*
 * 管理操作
 * operate：1恢复，2禁用，3删除
 * ids：账号ID
 */
function operateManage(operate,id){
	if(arguments.length==1 && id == undefined){
		var ids = new Array;
		$("input[name='ids']:checked").each(function () {
			ids.push($(this).val());
		});
	}else{
		var ids = id;
	}
	if(ids.length==0){
		dialog('请选择操作项');
	}else{
		$.ajax({
			type: "POST",
			url: URL + "/operateManage",
			data: "ids=" + ids +"&operate=" + operate,
			dataType: "json",
			success: function (msg) {
				dialog(msg.info);
				if (msg.status == 1) {
					setTimeout(function () {
						window.location.reload();
					}, 1000);
				}
			}
		});
	}
}

/*
 * 添加广告模板分类
 */
function addSort(){
	var name = $("input[name='name']").val();
	if(name==''){
		dialog('请输入分类名称');
	}else{
		$.ajax({
			type: "POST",
			url: URL + "/addAdType",
			data: "name=" + name,
			dataType: "json",
			success: function (msg) {
				dialog(msg.info);
				if (msg.status == 1) {
					setTimeout(function () { window.location.reload(); }, 1000);
				}
			}
		});
	}
}

/*
 * 删除广告类别
 */
function deleteAdType(id){
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '确认删除该分类？',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url: URL + "/deleteAdType",
					data: "id=" + id,
					dataType: "json",
					success: function (msg) {
						if (msg.status == 1) {
							setTimeout(function () { window.location.reload(); }, 1000);
						}
					}
				});
			},
			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: false
	});
}


/*
 * 修改广告类别
 */
function updateAdType(id){
	var name = $("#name_"+ id).val();
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '确认修改该分类？',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url: URL + "/updateAdType",
					data: "id=" + id +"&name="+ name,
					dataType: "json",
					success: function (msg) {
						if (msg.status == 1) {
							setTimeout(function () { window.location.reload(); }, 1000);
						}
					}
				});
			},
			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: false
	});
}


/*
 * 短信操作
 */
function operateSMS(operate,id){
	if(arguments.length==2 && id){
		var ids = id;
	}else{
		var ids = new Array;
		$("input[name='ids']:checked").each(function () {
			ids.push($(this).val());
		});
	}
	console.log(ids);
	if(ids.length==0 && operate!=2 ){
		dialog('请选择操作项');
	}else{
		easyDialog.open({
			container: {
				header: "系统提示",
				content: '确认执行操作？',
				yesFn: function (e) {
					$.ajax({
						type: "POST",
						url: URL + "/operateSMS",
						data: "ids=" + ids + "&operate=" + operate,
						dataType: "json",
						success: function (msg) {
							dialog(msg.info);
							if (msg.status == 1) {
								setTimeout(function () { window.location.reload(); }, 1000);
							}
						}
					});
				},
				noFn: true
			},
			autoClose: false,
			fixed: false,
			overlay: true,
			drag: false,
			lock: false
		});
	}
}