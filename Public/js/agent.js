$(function(){

	$(document).on("click",".box_opacity",function(){
		closeUpload();
	});

	$("input[name='all']").on("click",function(){
		var check = $(this).prop("checked");
		if(check){
			$("input[type='checkbox']").prop("checked", "checked");
		}else{
			$("input[type='checkbox']").prop("checked", false);
		}
	});

	/*
	 * 限制手机号码只能输入11位的数字
	 */
	$(document).on("input keyup","input[name='phone']",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
		this.maxLength = 11;
	});


	/*
	 * 限制属性data-rel="number"的输入框只能输入数字
	 */
	$("input[data-rel='number']").on("input keyup",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
	});


	//获取短信验证码
	$("#code").on("click",function(){
		var phone,obj;
		var telReg = /^(1[3-9][0-9])\d{8}$/;
		var token = $('input[name="token"]').val();
		phone = $('input[name="phone"]').val();
		if(phone==""){
			dialog('请输入手机号码');
		}else if(!telReg.test(phone) ){
			dialog('手机号码不正确');
		}else{
			$.ajax({
				type: "POST",
				url:  APP + "/home/public/interface_code",
				data: "phone="+ phone + "&token=" + token,
				dataType: "json",
				success: function(msg){
					if(msg.status==1){
						var countdown = 60;
						$("#code").attr("disabled","disabled");
						setTimeout(function(){ setTime(countdown); },1000);
					}
				}
			});
		}
	})
});

//60秒倒计时
function setTime(val){
	if(val == 1) {
		$("#code").removeAttr("disabled").text("发送验证码");
	}else{
		$("#code").text(val + "秒后重发");
	}
	val--;
	if(val>0){
		setTimeout(function(){ setTime(val); },1000)
	}
}

/*
 * 关闭底部弹出上传窗口
 */
function closeUpload(){
	$("body").css('overflow-y','auto');
	$(".box_opacity").remove();
	$("#choose").empty();
}

/*
 * jquery.form.js公共提交入口
 * beforeSubmit：提交检查函数可选参数
 * success：提交结果函数
 */
$.fn.publicAjaxForm=function(beforeSubmit,success){
	if(arguments.length==1){
		$(this).ajaxForm({
			success:  success,
			dataType: 'json'
		});
	}else{
		$(this).ajaxForm({
			beforeSubmit:  beforeSubmit,
			success:  success,
			dataType: 'json'
		});
	}
};

function complete(data){
	dialog(data.info);
	if(data.status==1){
		setTimeout(function(){ window.location.reload(); },1000);
	}
};

/*
 * 复制粘贴
 */
$.fn.pasteEvents = function( delay ) {
	if (delay == undefined) delay = 20;
	return $(this).each(function() {
		var $el = $(this);
		$el.on("paste", function() {
			$el.trigger("prepaste");
			setTimeout(function() { $el.trigger("postpaste"); }, delay);
		});
	});
};



// 弹出窗口样式
function dialog(msg){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
			msg
		},
		autoClose : 2000,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
}

// 弹出窗口样式
function dialogNoClose(msg){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
			msg
		},
		autoClose : false,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
}

/*
 * 用户登陆
 */
function login(){
	var phone = $("input[name='phone']").val();
	var pass = $("input[name='pass']").val();
	if(phone==''){
		dialog('请输入手机号码');
	}else if(pass==''){
		dialog('请输入密码');
	}else{
		$.ajax({
			type: "POST",
			url:  "interface_login",
			data: "phone="+ phone + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				console.log(msg);
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}else{
					dialog(msg.info);
				}
			}
		});
	}
	return false;
}


/*
 * ajax验证登录状态
 */
function ajaxCheckLogin(){
	$.ajax({
		type: "POST",
		url: MODULE + "/public/ajaxCheckLogin",
		//data: "ids=" + ids,
		dataType: "json",
		async: false,
		success: function (msg) {
			if (msg.status == 0) {
				dialog(msg.info);
				setTimeout(function () { window.location.href= MODULE + '/public/login.html' }, 1000);
				return false;
			}
		}
	})
}


/*
 * 注册
 */
function register(){
	var phone = $("input[name='phone']").val();
	var pass = $("input[name='pass']").val();
	var parent_uid = $("input[name='parent_uid']").val();
	var code = $("input[name='code']").val();
	var from = $("input[name='from']").val();

	var telReg = /^(1[3-9][0-9])\d{8}$/;
	if(phone=="" || !telReg.test(phone) ){
		dialog('手机号码格式不正确');
		return false;
	}else if(pass==''){
		dialog('请输入登录密码');
		return false;
	}else if(pass.length<6 || pass.length>16){
		dialog('请输入6-16位密码');
		return false;
	}else{
		$(".bnt").attr('disable','disable');
		$.ajax({
			type: "POST",
			url: MODULE + "/public/registerLogs",
			data: "phone=" + phone +"&pass="+ pass +"&parent_uid="+ parent_uid +"&code="+ code +"&from="+ from,
			dataType: "json",
			async: false,
			success: function (msg) {
				if (msg.status == 1) {
					setTimeout(function () { window.location.href= URL + '/step2?id='+ msg.data }, 1000);
				}else{
					dialog(msg.info);
				}
			}
		});
		return true;
	}
}

/*
 * 注册
 */
function doRegister(){
	var id = $("input[name='id']").val();
	var realname = $("input[name='realname']").val();
	var weixin = $("input[name='weixin']").val();
	var sex = $("input[name='sex']:checked").val();
	var province = $("select[name='province'] option:selected").val();
	var city = $("select[name='city'] option:selected").val();

	if(id<1){
		dialog('参数错误');
	}else if(realname==""){
		dialog('请输入真实姓名');
	}else if(weixin==''){
		dialog('请输入微信');
	}else if(province==''){
		dialog('请选择归属省份');
	}else if(city==''){
		dialog('请选择归属城市');
	}else{
		$(".bnt").attr('disable','disable');
		$.ajax({
			type: "POST",
			url: URL + "/register",
			data: "id="+ id +"&realname="+ realname +"&weixin="+ weixin +"&sex="+ sex +"&province="+ province +"&city="+ city,
			dataType: "json",
			async: false,
			success: function (msg) {
				dialog(msg.info);
				if (msg.status == 1) {
					setTimeout(function () { window.location.href= URL + '/login' }, 1000);
				}
			}
		});
		return true;
	}
	return false;
}


/*
 * 忘记密码
 */
function forget(){
	var phone = $("input[name='phone']").val();
	var code = $("input[name='code']").val();
	var pass = $("input[name='pass']").val();
	var repass = $("input[name='repass']").val();
	if(phone==''){
		dialog('请输入手机号码');
	}else if(code==''){
		dialog('请输入短信验证码');
	}else if(pass==''){
		dialog('请输入密码');
	}else if(pass.length<6 || pass.length>16){
		dialog('请输入6-16位密码');
	}else if(pass!= repass){
		dialog('两次输入的密码不相同');
	}else{
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/forget",
			data: "phone="+ phone + "&code=" + code + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				console.log(msg);
				if(msg.status==1){
					dialog(msg.info);
					setTimeout(function () { window.location.href = URL +"/login"; }, 1000);
				}else{
					dialog(msg.info);
				}
			}
		});
	}
	return false;
}

/*
 * 退出登录
 */
function layout(){
	$.ajax({
		type: "POST",
		url:  MODULE + "/public/layout",
		dataType: "json",
		success: function(msg){
			if(msg.status==1){
				setTimeout(function(){ window.location.reload(); },1000);
			}
		}
	});
}



/*
 * 登陆超时
 */
function loginOutTime(){
	console.log('1111');
	easyDialog.open({
		container: {
			header: "登陆超时，请重输入密码",
			content:  '<style>.easyDialog_title { display:block; font-weight:100; }</style>' +
					  '<input type="password" name="pass" value="" class="pass" placeholder="输入登录密码" />',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url:  MODULE + "/public/forget",
					data: "phone="+ phone + "&code=" + code + "&pass=" + pass,
					dataType: "json",
					success: function(msg){
						console.log(msg);
						if(msg.status==1){
							window.location.href = URL +"/login";
						}else{
							dialog_warn(msg.info);
						}
					}
				});
			},
			noFn: true,
			yesText : '确认',
			noText: '取消'
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
}



/*
 * 修改密码
 */
function changePass(){
	var old_pass = $("input[name='old_pass']").val();
	var new_pass = $("input[name='new_pass']").val();
	//var repass = $("input[name='repass']").val();
	if(old_pass==''){
		dialog('请输入旧密码');
	}else if(new_pass==''){
		dialog('请输入新密码');
	}else{
		$.ajax({
			type: "POST",
			url:  URL + "/pass",
			data: "old_pass="+ old_pass + "&new_pass=" + new_pass,
			dataType: "json",
			success: function(msg){
				if(msg.status==1){
					setTimeout(function(){ window.location.reload(); },1000);
				}else{
					dialog(msg.info);
				}
			}
		});
	}
	return false;
}


