$(function(){

	$(document).on("click",".box_opacity",function(){
		closeUpload();
	});

	$("input[name='all']").on("click",function(){
		var check = $(this).prop("checked");
		if(check){
			$("input[type='checkbox']").prop("checked", "checked");
		}else{
			$("input[type='checkbox']").prop("checked", false);
		}
	});

	/*
	 * 限制手机号码只能输入11位的数字
	 */
	$(document).on("input keyup","input[name='phone']",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
		this.maxLength = 11;
	});


	/*
	 * 限制属性data-rel="number"的输入框只能输入数字
	 */
	$("input[data-rel='number']").on("input keyup",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
	});


	//获取短信验证码
	$("#code").on("click",function(){
		var phone,obj;
		var telReg = /^(1[3-9][0-9])\d{8}$/;
		var token = $('input[name="token"]').val();
		phone = $('input[name="phone"]').val();
		if(phone==""){
			dialog('请输入手机号码');
		}else if(!telReg.test(phone) ){
			dialog('手机号码不正确');
		}else{
			$.ajax({
				type: "POST",
				url:  URL + "/interface_code",
				data: "phone="+ phone + "&token=" + token,
				dataType: "json",
				success: function(msg){
					if(msg.status==1){
						var countdown = 60;
						$("#code").attr("disabled","disabled");
						setTimeout(function(){ setTime(countdown); },1000);
					}
				}
			});
		}
	})
});

//60秒倒计时
function setTime(val){
	if(val == 1) {
		$("#code").removeAttr("disabled").text("发送验证码");
	}else{
		$("#code").text(val + "秒后重发");
	}
	val--;
	if(val>0){
		setTimeout(function(){ setTime(val); },1000)
	}
}

/*
 * 关闭底部弹出上传窗口
 */
function closeUpload(){
	$("body").css('overflow-y','auto');
	$(".box_opacity").remove();
	$("#choose").empty();
}

/*
 * jquery.form.js公共提交入口
 * beforeSubmit：提交检查函数可选参数
 * success：提交结果函数
 */
$.fn.publicAjaxForm=function(beforeSubmit,success){
	if(arguments.length==1){
		$(this).ajaxForm({
			success:  success,
			dataType: 'json'
		});
	}else{
		$(this).ajaxForm({
			beforeSubmit:  beforeSubmit,
			success:  success,
			dataType: 'json'
		});
	}
};

function complete(data){
	dialog(data.info);
	if(data.status==1){
		setTimeout(function(){ window.location.reload(); },1000);
	}
};

/*
 * 复制粘贴
 */
$.fn.pasteEvents = function( delay ) {
	if (delay == undefined) delay = 20;
	return $(this).each(function() {
		var $el = $(this);
		$el.on("paste", function() {
			$el.trigger("prepaste");
			setTimeout(function() { $el.trigger("postpaste"); }, delay);
		});
	});
};




//function copyLink(){
//	console.log('1111');
//	var text = window.clipboardData.getData("text");
//	console.log(text);
//	//$("textarea[name='link']").on("postpaste", function() {
//	//	var el = $(this);
//	//	setTimeout(function() {
//	//		var text = $(el).val();
//	//		alert(text);
//	//	}, 100);
//	//}).pasteEvents();
//}



// 弹出窗口样式
function dialog(msg){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
			msg
		},
		autoClose : 2000,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
}

// 弹出窗口样式
function dialogNoClose(msg){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
			msg
		},
		autoClose : false,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
}

/*
 * 用户登陆
 */
function login(){
	var phone = $("input[name='phone']").val();
	var pass = $("input[name='pass']").val();
	if(phone==''){
		dialog('请输入手机号码');
	}else if(pass==''){
		dialog('请输入密码');
	}else{
		$.ajax({
			type: "POST",
			url:  "interface_login",
			data: "phone="+ phone + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				console.log(msg);
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}else{
					dialog(msg.info);
				}
			}
		});
	}
	return false;
}


/*
 * ajax验证登录状态
 */
function ajaxCheckLogin(){
	$.ajax({
		type: "POST",
		url: MODULE + "/public/ajaxCheckLogin",
		//data: "ids=" + ids,
		dataType: "json",
		async: false,
		success: function (msg) {
			if (msg.status == 0) {
				dialog(msg.info);
				setTimeout(function () { window.location.href= MODULE + '/public/login.html' }, 1000);
				return false;
			}
		}
	})
}

/*
 * 注册
 */
function register(){
	//var step = arguments[1];
	var phone = $("input[name='phone']").val();
	var pass = $("input[name='pass']").val();
	var parent_uid = $("input[name='parent_uid']").val();
	var code = $("input[name='code']").val();
	var from = $("input[name='from']").val();

	var telReg = /^(1[3-9][0-9])\d{8}$/;
	if(phone=="" || !telReg.test(phone) ){
		dialog('手机号码格式不正确');
		return false;
	}else if(pass==''){
		dialog('请输入登录密码');
		return false;
	}else if(pass.length<6 || pass.length>16){
		dialog('请输入6-16位密码');
		return false;
	}else{
		$(".bnt").attr('disable','disable');
		$.ajax({
			type: "POST",
			url: MODULE + "/public/registerLogs",
			data: "phone=" + phone +"&pass="+ pass +"&parent_uid="+ parent_uid +"&code="+ code +"&from="+ from,
			dataType: "json",
			async: false,
			success: function (msg) {
				if (msg.status == 1) {
					setTimeout(function () { window.location.href= APP + '/home/public/buy/?id='+ msg.data }, 1000);
				}else{
					dialog(msg.info);
				}
			}
		});
		return true;
	}
}


function buyFree(){
	var id = $("input[name='id']").val();
	var choose = $("input[name='choose']").val();
	var pay_way = $("input[name='pay_way']").val();
	if(choose>0 && pay_way==0){
		dialog("请选择支付方式");
	}else{
		$.ajax({
			type: "POST",
			url: MODULE + "/public/pay/",
			data: "id=" + id +"&choose="+ choose,
			dataType: "json",
			async: false,
			success: function (msg) {
				dialog(msg.info);
				if (msg.status == 1 && choose==0) {
					setTimeout(function () { window.location.href= MODULE + '/index.html' }, 1000);
				}
			}
		});
		return false;
	}
}

/*
 * 购买套餐
 */
function goToPay(){
	var uid = $("input[name='uid']").val();
	var id = $("input[name='id']").val();
	var choose = $("input[name='choose']").val();
	var pay_way = $("input[name='pay_way']").val();
	if(pay_way==0 && choose != 0){
		dialog('请选择支付方式');
		return false;
	}
	easyDialog.open({
		container: {
			header: "微信支付确认",
			content: '如您已使用微信支付完成付款，请点击“我已支付成功”查看订单；如付款遇到问题，请尝试使用其他方式付款。',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url:  MODULE + "/public/paymoney",
					data: "id="+ id + "&uid="+ uid + "&choose="+ choose + "&pay_way=" + pay_way,
					dataType: "json",
					success: function(msg){
						if(msg.status==1){
							buyOk();
							//window.location.href = MODULE + '/index.html';
						}
					}
				});
			},
			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
}

/*
 * 购买成功提示
 */
function buyOk(){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
						'<div class="box_buy_ok"><div class="box_1"><i class="ico_ok"></i><div class="box_1_1">恭喜您，<br>购买成功</div></div> </div> ',
		},
		autoClose : false,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
	setTimeout(function () { window.location.href = URL +"/login"; }, 2000);
}

/*
 * 忘记密码
 */
function forget(){
	var phone = $("input[name='phone']").val();
	var code = $("input[name='code']").val();
	var pass = $("input[name='pass']").val();
	var repass = $("input[name='repass']").val();
	if(phone==''){
		dialog('请输入手机号码');
	}else if(code==''){
		dialog('请输入短信验证码');
	}else if(pass==''){
		dialog('请输入密码');
	}else if(pass.length<6 || pass.length>16){
		dialog('请输入6-16位密码');
	}else if(pass!= repass){
		dialog('两次输入的密码不相同');
	}else{
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/forget",
			data: "phone="+ phone + "&code=" + code + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				console.log(msg);
				if(msg.status==1){
					dialog(msg.info);
					setTimeout(function () { window.location.href = URL +"/login"; }, 1000);
				}else{
					dialog(msg.info);
				}
			}
		});
	}
	return false;
}

/*
 * 退出登录
 */
function layout(){
	$.ajax({
		type: "POST",
		url:  MODULE + "/public/layout",
		dataType: "json",
		success: function(msg){
			if(msg.status==1){
				setTimeout(function(){ window.location.reload(); },1000);
			}
		}
	});
}



/*
 * 登陆超时
 */
function loginOutTime(){
	console.log('1111');
	easyDialog.open({
		container: {
			header: "登陆超时，请重输入密码",
			content:  '<style>.easyDialog_title { display:block; font-weight:100; }</style>' +
					  '<input type="password" name="pass" value="" class="pass" placeholder="输入登录密码" />',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url:  MODULE + "/public/forget",
					data: "phone="+ phone + "&code=" + code + "&pass=" + pass,
					dataType: "json",
					success: function(msg){
						console.log(msg);
						if(msg.status==1){
							window.location.href = URL +"/login";
						}else{
							dialog_warn(msg.info);
						}
					}
				});
			},
			noFn: true,
			yesText : '确认',
			noText: '取消'
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
}


/*
 * 转换文章
 */
function convertArticle(){
	var url = $("textarea[name='url']").val();
	if(url==''){
		dialog('请粘贴将文章链接');
		return false;
	}else if( url.indexOf('http://')== -1 ){
		dialog('请输入有效连接');
		return false;
	}
	$.ajax({
		type: "POST",
		url:  URL + "/convertArticle",
		data: "url="+ url,
		dataType: "json",
		success: function(msg){
			dialog(msg.info);
			if(msg.status==1){
				$.post( URL + "/tolocal", { aid: msg.data } );
				console.log(msg.data);
				setTimeout(function(){
					//window.location.href = MODULE +"/article/item?id="+ msg.id +"&aid=" + msg.data;
					window.location.href = MODULE +"/article/index";
				},2000);
			}
		},
		beforeSend: function(){
			dialogNoClose('发布中...');
		}
	});
	return false;
}

/*
 * 删除文章
 */

function deleteArticle(aid){
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '<p style="font-size:18px;">是否要删除该文章？</p>',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url:  URL + "/deleteArticle",
					data: "aid="+ aid,
					dataType: "json",
					success: function(msg){
						if(msg.status==1){
							setTimeout(function(){ window.location.reload(); },1000);
						}
					}
				});
			},
			noText : '取消',
			yesText : '确认',

			noFn: true
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
	return false;
}

/*
 * 设为首选
 */
function adToDefault(aid,operate){
	$.ajax({
		type: "POST",
		url:  URL + "/toDefault",
		data: "aid="+ aid + "&operate=" + operate,
		dataType: "json",
		success: function(msg){
			if(msg.status==1){
				setTimeout(function(){ window.location.reload(); },1000);
			}else{
				dialog(msg.info);
			}
		}
	});
}


/*
 * 保存广告
 */
function saveAd(){
	var aid = $("input[name='aid']").val();
	var ids = new Array;
	var images = new Array;
	var type = new Array;
	var text = new Array;
	var source = new Array;
	$("input[name='ids']").each(function(){
		ids.push($(this).val());
	});
	$(".cover").each(function(){
		images.push($(this).attr('src'));
		source.push($(this).attr('data-source'));
	});
	$("select[name='type'] option:selected").each(function(){
		type.push($(this).val());
	});
	$("input[name='text']").each(function(){
		text.push($(this).val());
	});
	console.log(ids);
	console.log(images);
	console.log(type);
	console.log(text);
	if(images[0]=='/Public/images/mobile/ad_add.png' && images[1]=='/Public/images/mobile/ad_add.png' && images[2]=='/Public/images/mobile/ad_add.png'){
		dialog('请选择广告图片');
		return false;
	}
	$.ajax({
		type: "POST",
		url:  MODULE + "/ad/edit",
		data: "aid="+ aid + "&ids="+ ids +"&images="+ images +"&source="+ source + "&type=" + type + "&text=" + text,
		dataType: "json",
		success: function(msg){
			dialog('上传中，请等待');
			if(msg.status==1){
				setTimeout(function(){ window.location.href = MODULE + '/ad/index.html'; },1000);
			}else{
				dialog(msg.info);
			}
		}
	});
	return false;
}

/*
 * 删除广告
 */
function deleteAd(aid){
	easyDialog.open({
		container: {
			header: "系统提示",
			content:  '<p style="font-size:18px;">确认删除此条信息？</p>',
			yesFn: function (e) {
				$.ajax({
					type: "POST",
					url:  URL + "/deleteAd",
					data: "aid="+ aid,
					dataType: "json",
					success: function(msg){
						if(msg.status==1){
							setTimeout(function(){ window.location.reload(); },1000);
						}else{
							dialog(msg.info);
						}
					}
				});
			},
			noFn: true,
			yesText: '确定',
			noText: '取消'
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
}

/*
 * 删除单张广告图
 */
function deleteDetailAd(id){
	$.ajax({
		type: "POST",
		url:  URL + "/deleteAdDetail",
		data: "id="+ id,
		dataType: "json",
		success: function(msg){
			if(msg.status==1){

			}
		}
	});
}


/*
 * 合成文字广告
 */
function createAd(id){
	var image,text,size,color;
	image = $(".picture").attr('src');
	text = $("input[name='text_ad']").val();
	size = $("select[name='size'] option:selected").val();
	color = $("select[name='color'] option:selected").val();
	if(image==''){
		dialog('请上传背景图片');
		return false;
	}else if(text==''){
		dialog('请输入广告文字');
		return false;
	}
	console.log(text);
	$.ajax({
		type: "POST",
		url:  MODULE + "/ad/createpicture",
		data: "image="+ image +"&text="+ text +"&size="+ size +"&color="+ color,
		dataType: "json",
		success: function(msg){
			console.log("#uploadphoto"+ id + '/'+ msg.data);
			if(msg.status==1){
				//$("#picture").attr('value',msg.data);
				//$("#picture").siblings('.picture').attr('src',msg.data);
				$("#uploadphoto"+ id).attr('value',msg.data);
				$("#uploadphoto"+ id).siblings('.cover').attr('src',msg.data);
				closeUpload();
			}
		}
	});

}

/*
 * 广告图定时切换
 */



/*
 * 保存基本信息
 */
function saveinfo() {
	//console.log(arguments);
	var name = $("input[name='name']").val();
	var role = $("input[name='role']").val();
	var phone = $("input[name='phone']").val();
	var cellphone = $("input[name='cellphone']").val();
	var qq = $("input[name='qq']").val();
	var weixin = $("input[name='weixin']").val();
	var email = $("input[name='email']").val();
	var website = $("input[name='website']").val();
	var company = $("input[name='company']").val();
	var address = $("input[name='address']").val();
	var business = $("textarea[name='business']").val();
	$.ajax({
		type: "POST",
		url:  MODULE + "/account/saveInfo",
		data: "name="+ name +"&role="+ role +"&phone="+ phone +"&cellphone="+ cellphone +"&qq="+ qq +
				"&weixin="+ weixin +"&email="+ email +"&website="+ website +"&company="+ company +"&address="+ address +"&business="+ business,
		dataType: "json",
		success: function(msg){
			if(msg.status==1){
				setTimeout(function(){ window.location.reload(); },1000);
			}else{
				dialog(msg.info);
			}
		}
	});
	return false;
}

/*
 * 修改密码
 */
function changePass(){
	var old_pass = $("input[name='old_pass']").val();
	var new_pass = $("input[name='new_pass']").val();
	//var repass = $("input[name='repass']").val();
	if(old_pass==''){
		dialog('请输入旧密码');
	}else if(new_pass==''){
		dialog('请输入新密码');
	}else{
		$.ajax({
			type: "POST",
			url:  URL + "/pass",
			data: "old_pass="+ old_pass + "&new_pass=" + new_pass,
			dataType: "json",
			success: function(msg){
				if(msg.status==1){
					setTimeout(function(){ window.location.reload(); },1000);
				}else{
					dialog(msg.info);
				}
			}
		});
	}
	return false;
}

function saveWeixin(){
	var card = $(".cover").attr("data-src");
	console.log(card);
	if(card==''){
		dialog('请上传微信名片');
		return false;
	}
	$.ajax({
		type: "POST",
		url:   "saveWeixin",
		data: "card="+ card,
		dataType: "json",
		success: function(msg){
			dialog(msg.info);
			if(msg.status==1){
				//setTimeout(function(){ window.location.reload(); },1000);
			}
		}
	});
}



