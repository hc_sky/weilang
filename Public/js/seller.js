$(function(){



	/*
	 * 限制手机号码只能输入11位的数字
	 */
	$(document).on("input keyup","input[name='phone']",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
		this.maxLength = 11;
	});


	/*
	 * 限制属性data-rel="number"的输入框只能输入数字
	 */
	$("input[data-rel='number']").on("input keyup",function(){
		this.value = this.value.replace(/[^0-9-]+/,'');
	});


	//获取短信验证码
	$("#code").on("click",function(){
		var phone,obj;
		var telReg = /^(1[3-9][0-9])\d{8}$/;
		var token = $('input[name="token"]').val();
		phone = $('input[name="phone"]').val();
		if(phone=="" || !telReg.test(phone) ){
			console.log('手机号码不正确');
		}else{
			$.ajax({
				type: "POST",
				url:  "/home/public/interface_code",
				data: "phone="+ phone + "&token=" + token,
				dataType: "json",
				success: function(msg){
					if(msg.status==1){
						var countdown = 60;
						$("#code").attr("disabled","disabled");
						setTimeout(function(){ setTime(countdown); },1000);
					}
				}
			});
		}
	})
})

//60秒倒计时
function setTime(val){
	if(val == 1) {
		$("#code").removeAttr("disabled").text("发送验证码");
	}else{
		$("#code").text(val + "秒后重发");
	}
	val--;
	if(val>0){
		setTimeout(function(){ setTime(val); },1000)
	}
}

/*
 * jquery.form.js公共提交入口
 * beforeSubmit：提交检查函数可选参数
 * success：提交结果函数
 */
$.fn.publicAjaxForm=function(beforeSubmit,success){
	if(arguments.length==1){
		$(this).ajaxForm({
			success:  success,
			dataType: 'json'
		});
	}else{
		$(this).ajaxForm({
			beforeSubmit:  beforeSubmit,
			success:  success,
			dataType: 'json'
		});
	}
}

function complete(data){
	dialog(data.info);
	if(data.status==1){
		setTimeout(function(){ window.location.reload(); },1000);
	}
}

// 弹出窗口样式
function dialog(msg){
	easyDialog.open({
		container : {
			header : '系统提示',
			content : '<style>.easyDialog_wrapper{width:250px;} .easyDialog_text{text-align:center; line-height:20px !important; color:#555; padding:15px !important; }</style>'+
			msg,
		},
		autoClose : 2000,      //定义自动关闭窗口时间
		fixed : false,          //弹出窗口是否跟随页面的滚动而滚动
		overlay : true,        //是否有遮罩层
		drag : false,           //是否能被拖动
		lock : true             //lock为true时，按esc键不能关闭窗口
	});
};

/*
 * 警告 弹出窗口样式
 */
function dialog_warn(msg){
	easyDialog.open({
		container: {
			header: "系统提示",
			content: '<style>.easyDialog_footer button{ width:100%; }</style>' +
			'<i class="ico_warn h20_block"></i>' + msg,
			yesFn: function (e) {

			},
			noFn: false
		},
		autoClose: false,
		fixed: false,
		overlay: true,
		drag: false,
		lock: true
	});
}

/*
 * 用户登陆
 */
function login(){
	var phone = $("input[name='phone']").val();
	var pass = $("input[name='pass']").val();
	if(phone=='' && 0){
		dialog('请输入手机号码');
	}else if(pass=='' && 0){
		dialog('请输入密码');
	}else{
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/interface_login",
			data: "phone="+ phone + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				console.log(msg);
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}else{
					dialog_warn(msg.info);
				}
			}
		});
	}
	return false;
}

/*
 * 注册
 */
function register(step){
	var phone,code,password,realname,province,city,district,estate,agree;
	phone = $("input[name='phone']").val();
	code = $("input[name='code']").val();
	password = $("input[name='pass']").val();
	realname = $("input[name='realname']").val();
	province = $("input[name='province']").val();
	city = $("input[name='city']").val();
	district = $("input[name='district']").val();
	estate = $("input[name='estate']").val();
	agree = $("input[name='agree']:checked").val();

	if(agree!=1){
		dialog("请浏览注册协议");
	}
	if(step==1){
		//$(".box_step_1").css("display","none");
		//$(".box_step_2").css("display","block");
		if(phone!='' && code!='' && password!=''){
			$.ajax({
				type: "POST",
				url:  URL + "/checkCode",
				data: "phone="+ phone +"&code="+ code,
				dataType: "json",
				success: function(msg){
					if(msg.status==1){
						$(".box_step_1").css("display","none");
						$(".box_step_2").css("display","block");
					}else{
						dialog(msg.info);
					}
				}
			});
		}else{
			return false;
		}
	}else if(step==2){
		if(realname!='' && province!='' && district!='' && estate!=''){
			$.ajax({
				type: "POST",
				url:  URL + "/register",
				data: "phone="+ phone +"&code="+ code +"&password="+ password +"&realname="+ realname +"&province="+ province +"&city="+ city +"&district="+ district +"&estate="+ estate,
				dataType: "json",
				success: function(msg){
					if(msg.status==1){
						dialog(msg.info);
						setTimeout(function(){ window.location.href = URL + '/login.html'; },1000);
					}
				}
			});
		}
	}
}


/*
 * 忘记密码
 */
function forget(){
	var phone = $("input[name='phone']").val();
	var code = $("input[name='code']").val();
	var pass = $("input[name='pass']").val();
	var repass = $("input[name='repass']").val();
	if(phone==''){
		dialog('请输入手机号码');
	}else if(code==''){
		dialog('请输入短信验证码');
	}else if(pass==''){
		dialog('请输入密码');
	}else if(pass!= repass){
		dialog('两次输入的密码不相同');
	}else{
		$.ajax({
			type: "POST",
			url:  MODULE + "/public/forget",
			data: "phone="+ phone + "&code=" + code + "&pass=" + pass,
			dataType: "json",
			success: function(msg){
				console.log(msg);
				if(msg.status==1){
					window.location.href = URL +"/login";
				}else{
					dialog_warn(msg.info);
				}
			}
		});
	}
	return false;
}

/*
 * 修改密码
 */
function changePass(){
	var old_pass = $("input[name='old_pass']").val();
	var new_pass = $("input[name='new_pass']").val();
	var repass = $("input[name='repass']").val();
	if(old_pass==''){
		dialog('请输入旧密码');
	}else if(new_pass==''){
		dialog('请输入新密码');
	}else if(new_pass!= repass){
		dialog('两次输入的密码不相同');
	}else{
		$.ajax({
			type: "POST",
			url:  URL + "/changePass",
			data: "old_pass="+ old_pass + "&new_pass=" + new_pass,
			dataType: "json",
			success: function(msg){
				dialog(msg.info);
				if(msg.status==1){
					window.location.href = MODULE +"/public/login";
				}
			}
		});
	}
}

/*
 * 更改个人信息
 */
function updateBase(){
	var realname = $("input[name='realname']").val();
	var province = $("input[name='province']").val();
	var city = $("input[name='city']").val();
	var district = $("input[name='district']").val();
	var estate = $("input[name='estate']").val();
	if(realname==''){
		dialog('请输入真实姓名');
	}else if(province==''){
		dialog('请选择所在省份');
	}else if(city== ''){
		dialog('请选择所在城市');
	}else if(district== ''){
		dialog('请选择所在区域');
	}else{
		$.ajax({
			type: "POST",
			url:  URL + "/updateBaseInfo",
			data: "realname="+ realname +"&province="+ province +"&city="+ city +"&district="+ district +"&estate="+ estate,
			dataType: "json",
			success: function(msg){
				dialog(msg.info);
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}
			}
		});
	}
}


/*
 * 设置支付账号
 */
function setPayAccount(){
	var weixin = $("input[name='weixin']").val();
	var alipay = $("input[name='alipay']").val();
	var phone = $("input[name='phone']").val();
	var code = $("input[name='code']").val();
	if(weixin==''){
		dialog('请设置微信账户');
	}else if(alipay==''){
		dialog('请设置支付宝账户');
	}else if(phone== ''){
		dialog('请输入手机号码');
	}else if(code== ''){
		dialog('请填写短信验证码');
	}else{
		$.ajax({
			type: "POST",
			url:  URL + "/updatePayAccount",
			data: "weixin="+ weixin +"&alipay="+ alipay +"&phone="+ phone +"&code="+ code,
			dataType: "json",
			success: function(msg){
				dialog(msg.info);
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}
			}
		});
	}
}

/*
 * 变更手机号码
 * phone：手机号
 * code：验证码
 */
function changePhone(){
	var phone = $("input[name='phone']").val();
	var code = $("input[name='code']").val();
	if(phone== ''){
		dialog('请输入手机号码');
	}else if(code== ''){
		dialog('请填写短信验证码');
	}else{
		$.ajax({
			type: "POST",
			url:  URL + "/changePhone",
			data: "phone="+ phone +"&code="+ code,
			dataType: "json",
			success: function(msg){
				dialog(msg.info);
				if(msg.status==1){
					window.location.href = MODULE +"/index";
				}
			}
		});
	}
}


/*
 * 底部弹出加入购物车
 * id：商品ID
 */
function chooseCity(type){
	//$("body").append().load("getGoodsToCar.html");
	$("body").css('overflow-y','hidden');
	$("#box_city").load("choose_city").before('<div class="box_opacity"></div>');
}