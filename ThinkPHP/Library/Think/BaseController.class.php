<?php
namespace Think;

class BaseController extends Controller {

    public static $Cache = array();

    public function _initialize(){
        self::init_site();
        self::site_jump();
        self::compute();
        self::agents_random();
    }

    protected function init_site(){

    }

    /*
     * 域名跳转
     */
    protected function site_jump(){
//        dump( $_SERVER['HTTP_HOST'] );
//        dump( $_SERVER['REQUEST_URI'] );

    }

    /*
     * 页面统计数
     * MODULE_NAME：模块名
     * CONTROLLER_NAME：控制器名
     * ACTION_NAME：方法名
     */
    protected function compute(){
        if( MODULE_NAME == 'Home' && CONTROLLER_NAME == 'Public' && ACTION_NAME =='login' ){
            self::autoClick(2);
        }
        if(MODULE_NAME == 'Home'){
            self::autoClick(1);
        }
    }



    /*
     * 浏览数加一
     * page：页面，1移动端所有页面，2登录页
     */
    function autoClick($page){
        $logs = M("WebClick")->where( array('page'=>$page,'date'=>date('Y-m-d',time())) )->find();
        if($logs){
            M("WebClick")->where( array('page'=>$page,'date'=>date('Y-m-d',time())) )->setInc("pv");
        }else{
            M("WebClick")->add(array(
                'page' => $page,
                'ip' => 1,
                'pv' => 1,
                'date' => date('Y-m-d',time())
            ));
        }
        self::autoIP($page);

    }

    /*
     * 统计PV
     * page：页面，1所有页面，2登录页
     */
    public function autoIP($page)
    {
        $ip = self::getRealIP();

        $logs = M("WebClickIp")->where( array('page'=>$page,'date'=>date('Y-m-d',time()),'ip'=>$ip ) )->find();
        if( empty($logs) ){
            $result = M("WebClickIp")->add(array(
                'page' => $page,
                'date' => date('Y-m-d',time()),
                'ip' => $ip
            ));
            if($result){
                M("WebClick")->where( array('page'=>$page,'date'=>date('Y-m-d',time())) )->setInc("ip");
            }
        }
        return $ip;
    }

    /*
     * 获取真实IP
     */
    public function getRealIP(){
        global $ip;
        if (getenv("HTTP_CLIENT_IP"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if(getenv("HTTP_X_FORWARDED_FOR"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if(getenv("REMOTE_ADDR"))
            $ip = getenv("REMOTE_ADDR");
        else $ip = "Unknow";
        return $ip;
    }

    public function agents_random(){
        $log = M("AgentsNum")->where( array('id'=>1) )->find();
        $diff = time() - $log['update_time'];
        $num = intval( $diff / 1800 );
        if( $diff > 1800 ){
            for($i=0; $i<$num; $i++){
                $log['value'] = $log['value'] + rand(1,3);
                M("AgentsNum")->where( array('id'=>1) )->save( array('value'=>$log['value'],'update_time'=>time() ) );
                //echo M()->getLastSql().'<br>';
            }
        }
    }

}